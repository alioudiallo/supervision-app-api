# docker run

```bash
sudo docker run -it -p 8080:8080 -e "SPRING_PROFILES_ACTIVE=dev" -d snecommerce/kaylene-api:latest
```

# Start client side

# Start Server Side

sudo ./mvnw yarn start

localhost:8010/swagger-ui/index.html

# running jar

./mvnw spring-boot:run java -jar target/\*.jar

# regenerate & clean directory and update model

bash shopt -s extglob sudo rm -rf node_modules src sudo rm -rf
!(.yo-rc.json|.git|kaylene|README.md|Jenkinsfile|deploy.sh|)

sudo jhipster git add . git commit -m "JHipster regenerate"

sudo jhipster import-jdl kaylene/demand.jdl --force

update application-prod.yml update securityconfig.yml

# clean h2 database

sudo ./mvnw clean

# Building and running a Docker image of application

To create a Docker image your application, and push it into your Docker
registry:

cd project_dir/ sudo ./mvnw package -Pprod dockerfile:build

* run To run this image docker-compose -f src/main/docker/app.yml up

# push to Docker Hub

https://docs.docker.com/docker-cloud/builds/push-images/

$ docker login

(snecommerce / aqwzsx123)

> sudo ./mvnw clean package -Pprod -DskipTests=true dockerfile:build && docker
> image rm snecommerce/supervision-api && docker tag supervisionapi:latest
> snecommerce/supervision-api && docker push snecommerce/supervision-api

# Database Model

![Domain Model](domain-model.png)
