package com.kaylene.supervision.service.mapper;

import com.kaylene.supervision.domain.Offer;

import com.kaylene.supervision.service.dto.OfferDTO;

import java.util.ArrayList;

import java.util.List;

import javax.annotation.Generated;

import org.springframework.stereotype.Component;

@Generated(

    value = "org.mapstruct.ap.MappingProcessor",

    date = "2017-11-29T20:33:28+0000",

    comments = "version: 1.1.0.Final, compiler: Eclipse JDT (IDE) 3.13.50.v20171007-0855, environment: Java 1.8.0_131 (Oracle Corporation)"

)

@Component

public class OfferMapperImpl implements OfferMapper {

    @Override

    public Offer toEntity(OfferDTO dto) {

        if ( dto == null ) {

            return null;
        }

        Offer offer = new Offer();

        offer.setId( dto.getId() );

        offer.setLabel( dto.getLabel() );

        offer.setMaxHost( dto.getMaxHost() );

        //offer.setType( dto.getType() );

        return offer;
    }

    @Override

    public OfferDTO toDto(Offer entity) {

        if ( entity == null ) {

            return null;
        }

        OfferDTO offerDTO = new OfferDTO();

        offerDTO.setId( entity.getId() );

        offerDTO.setLabel( entity.getLabel() );

        offerDTO.setMaxHost( entity.getMaxHost() );

        //offerDTO.setType( entity.getType() );

        return offerDTO;
    }

    @Override

    public List<Offer> toEntity(List<OfferDTO> dtoList) {

        if ( dtoList == null ) {

            return null;
        }

        List<Offer> list = new ArrayList<Offer>();

        for ( OfferDTO offerDTO : dtoList ) {

            list.add( toEntity( offerDTO ) );
        }

        return list;
    }

    @Override

    public List<OfferDTO> toDto(List<Offer> entityList) {

        if ( entityList == null ) {

            return null;
        }

        List<OfferDTO> list = new ArrayList<OfferDTO>();

        for ( Offer offer : entityList ) {

            list.add( toDto( offer ) );
        }

        return list;
    }
}

