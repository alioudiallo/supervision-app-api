package com.kaylene.supervision.service.mapper;

import com.kaylene.supervision.domain.Company;

import com.kaylene.supervision.domain.Contract;

import com.kaylene.supervision.domain.Demand;

import com.kaylene.supervision.domain.HostDemand;

import com.kaylene.supervision.service.dto.DemandDTO;

import com.kaylene.supervision.service.dto.HostDemandDTO;

import java.util.ArrayList;

import java.util.HashSet;

import java.util.List;

import java.util.Set;

import javax.annotation.Generated;

import org.springframework.beans.factory.annotation.Autowired;

import org.springframework.stereotype.Component;

@Generated(

    value = "org.mapstruct.ap.MappingProcessor",

    date = "2017-11-29T20:33:29+0000",

    comments = "version: 1.1.0.Final, compiler: Eclipse JDT (IDE) 3.13.50.v20171007-0855, environment: Java 1.8.0_131 (Oracle Corporation)"

)

@Component

public class DemandMapperImpl implements DemandMapper {

    @Autowired

    private ContractMapper contractMapper;

    @Autowired

    private CompanyMapper companyMapper;

    @Autowired

    private HostDemandMapper hostDemandMapper;

    @Override

    public List<Demand> toEntity(List<DemandDTO> dtoList) {

        if ( dtoList == null ) {

            return null;
        }

        List<Demand> list = new ArrayList<Demand>();

        for ( DemandDTO demandDTO : dtoList ) {

            list.add( toEntity( demandDTO ) );
        }

        return list;
    }

    @Override

    public List<DemandDTO> toDto(List<Demand> entityList) {

        if ( entityList == null ) {

            return null;
        }

        List<DemandDTO> list = new ArrayList<DemandDTO>();

        for ( Demand demand : entityList ) {

            list.add( toDto( demand ) );
        }

        return list;
    }

    @Override

    public DemandDTO toDto(Demand demand) {

        if ( demand == null ) {

            return null;
        }

        DemandDTO demandDTO_ = new DemandDTO();

        demandDTO_.setContractId( demandContractId( demand ) );

        demandDTO_.setCompanyId( demandCompanyId( demand ) );

        Set<HostDemandDTO> set = hostDemandSetToHostDemandDTOSet( demand.getHostsToAdds() );

        if ( set != null ) {

            demandDTO_.setHostsToAdds( set );
        }

        demandDTO_.setId( demand.getId() );

        demandDTO_.setType( demand.getType() );

        demandDTO_.setCurrentStatus( demand.getCurrentStatus() );

        demandDTO_.setDescription( demand.getDescription() );

        demandDTO_.setCreatedDate( demand.getCreatedDate() );

        demandDTO_.setLastModifiedDate( demand.getLastModifiedDate() );
        
        demandDTO_.setUserId( demand.getUserId());
        
        demandDTO_.setUserName( demand.getUserName());
        
        demandDTO_.setAdminId( demand.getAdminId());
        
        demandDTO_.setAdminName( demand.getAdminName());

        return demandDTO_;
    }

    @Override

    public Demand toEntity(DemandDTO demandDTO) {

        if ( demandDTO == null ) {

            return null;
        }

        Demand demand_ = new Demand();

        demand_.setCompany( companyMapper.fromId( demandDTO.getCompanyId() ) );

        demand_.setContract( contractMapper.fromId( demandDTO.getContractId() ) );

        demand_.setId( demandDTO.getId() );

        demand_.setType( demandDTO.getType() );

        demand_.setCurrentStatus( demandDTO.getCurrentStatus() );

        demand_.setDescription( demandDTO.getDescription() );

        demand_.setCreatedDate( demandDTO.getCreatedDate() );

        demand_.setLastModifiedDate( demandDTO.getLastModifiedDate());

        demand_.setHostsToAdds(toHostDemandEntities(demandDTO.getHostsToAdds()) );
        
        demand_.setUserId( demandDTO.getUserId());
        
        demand_.setUserName( demandDTO.getUserName());

        return demand_;
    }

    @Override

    public HostDemand toHostDemandEntity(HostDemandDTO hostDemandDTO) {

        if ( hostDemandDTO == null ) {

            return null;
        }

        HostDemand hostDemand = new HostDemand();

        hostDemand.setDemand( fromId( hostDemandDTO.getDemandId() ) );

        hostDemand.setId( hostDemandDTO.getId() );
        
        hostDemand.setZabbixId( hostDemandDTO.getZabbixId() );

        hostDemand.setLabel( hostDemandDTO.getLabel());

        hostDemand.setIpAddress( hostDemandDTO.getIpAddress());

        hostDemand.setSiteId( hostDemandDTO.getSiteId());

        hostDemand.setType( hostDemandDTO.getType());

        hostDemand.setCodeSnmp( hostDemandDTO.getCodeSnmp());

        hostDemand.setDateIntegration( hostDemandDTO.getDateIntegration() );

        return hostDemand;
    }

    @Override

    public Set<HostDemand> toHostDemandEntities(Set<HostDemandDTO> hostDemandDTO) {

        if ( hostDemandDTO == null ) {

            return null;
        }

        Set<HostDemand> set = new HashSet<HostDemand>();

        for ( HostDemandDTO hostDemandDTO_ : hostDemandDTO ) {

            set.add( toHostDemandEntity( hostDemandDTO_ ) );
        }

        return set;
    }

    private Long demandContractId(Demand demand) {

        if ( demand == null ) {

            return null;
        }

        Contract contract = demand.getContract();

        if ( contract == null ) {

            return null;
        }

        Long id = contract.getId();

        if ( id == null ) {

            return null;
        }

        return id;
    }

    private Long demandCompanyId(Demand demand) {

        if ( demand == null ) {

            return null;
        }

        Company company = demand.getCompany();

        if ( company == null ) {

            return null;
        }

        Long id = company.getId();

        if ( id == null ) {

            return null;
        }

        return id;
    }

    protected Set<HostDemandDTO> hostDemandSetToHostDemandDTOSet(Set<HostDemand> set) {

        if ( set == null ) {

            return null;
        }

        Set<HostDemandDTO> set_ = new HashSet<HostDemandDTO>();

        for ( HostDemand hostDemand : set ) {

            set_.add( hostDemandMapper.toDto( hostDemand ) );
        }

        return set_;
    }
}

