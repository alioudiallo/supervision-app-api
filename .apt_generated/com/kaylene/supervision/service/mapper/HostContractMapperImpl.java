package com.kaylene.supervision.service.mapper;

import com.kaylene.supervision.domain.Contract;

import com.kaylene.supervision.domain.HostContract;

import com.kaylene.supervision.service.dto.HostContractDTO;

import java.util.ArrayList;

import java.util.List;

import javax.annotation.Generated;

import org.springframework.beans.factory.annotation.Autowired;

import org.springframework.stereotype.Component;

@Generated(

    value = "org.mapstruct.ap.MappingProcessor",

    date = "2017-11-29T20:33:28+0000",

    comments = "version: 1.1.0.Final, compiler: Eclipse JDT (IDE) 3.13.50.v20171007-0855, environment: Java 1.8.0_131 (Oracle Corporation)"

)

@Component

public class HostContractMapperImpl implements HostContractMapper {

    @Autowired

    private ContractMapper contractMapper;

    @Override

    public List<HostContract> toEntity(List<HostContractDTO> dtoList) {

        if ( dtoList == null ) {

            return null;
        }

        List<HostContract> list = new ArrayList<HostContract>();

        for ( HostContractDTO hostContractDTO : dtoList ) {

            list.add( toEntity( hostContractDTO ) );
        }

        return list;
    }

    @Override

    public List<HostContractDTO> toDto(List<HostContract> entityList) {

        if ( entityList == null ) {

            return null;
        }

        List<HostContractDTO> list = new ArrayList<HostContractDTO>();

        for ( HostContract hostContract : entityList ) {

            list.add( toDto( hostContract ) );
        }

        return list;
    }

    @Override

    public HostContractDTO toDto(HostContract hostContract) {

        if ( hostContract == null ) {

            return null;
        }

        HostContractDTO hostContractDTO_ = new HostContractDTO();

        hostContractDTO_.setContractId( hostContractContractId( hostContract ) );

        hostContractDTO_.setId( hostContract.getId() );

        hostContractDTO_.setZabbixHostId( hostContract.getZabbixHostId() );

        return hostContractDTO_;
    }

    @Override

    public HostContract toEntity(HostContractDTO hostContractDTO) {

        if ( hostContractDTO == null ) {

            return null;
        }

        HostContract hostContract_ = new HostContract();

        hostContract_.setContract( contractMapper.fromId( hostContractDTO.getContractId() ) );

        hostContract_.setId( hostContractDTO.getId() );

        hostContract_.setZabbixHostId( hostContractDTO.getZabbixHostId() );

        return hostContract_;
    }

    private Long hostContractContractId(HostContract hostContract) {

        if ( hostContract == null ) {

            return null;
        }

        Contract contract = hostContract.getContract();

        if ( contract == null ) {

            return null;
        }

        Long id = contract.getId();

        if ( id == null ) {

            return null;
        }

        return id;
    }
}

