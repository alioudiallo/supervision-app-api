package com.kaylene.supervision.service.mapper;

import com.kaylene.supervision.domain.Company;

import com.kaylene.supervision.domain.Site;

import com.kaylene.supervision.service.dto.SiteDTO;

import java.util.ArrayList;

import java.util.List;

import javax.annotation.Generated;

import org.springframework.beans.factory.annotation.Autowired;

import org.springframework.stereotype.Component;

@Generated(

    value = "org.mapstruct.ap.MappingProcessor",

    date = "2017-11-29T20:33:29+0000",

    comments = "version: 1.1.0.Final, compiler: Eclipse JDT (IDE) 3.13.50.v20171007-0855, environment: Java 1.8.0_131 (Oracle Corporation)"

)

@Component

public class SiteMapperImpl implements SiteMapper {

    @Autowired

    private CompanyMapper companyMapper;

    @Override

    public List<Site> toEntity(List<SiteDTO> dtoList) {

        if ( dtoList == null ) {

            return null;
        }

        List<Site> list = new ArrayList<Site>();

        for ( SiteDTO siteDTO : dtoList ) {

            list.add( toEntity( siteDTO ) );
        }

        return list;
    }

    @Override

    public List<SiteDTO> toDto(List<Site> entityList) {

        if ( entityList == null ) {

            return null;
        }

        List<SiteDTO> list = new ArrayList<SiteDTO>();

        for ( Site site : entityList ) {

            list.add( toDto( site ) );
        }

        return list;
    }

    @Override

    public SiteDTO toDto(Site site) {

        if ( site == null ) {

            return null;
        }

        SiteDTO siteDTO_ = new SiteDTO();

        siteDTO_.setCompanyId( siteCompanyId( site ) );

        siteDTO_.setId( site.getId() );

        siteDTO_.setZabbixProxyId( site.getZabbixProxyId() );

        siteDTO_.setLabel( site.getLabel() );

        siteDTO_.setAddress( site.getAddress() );

        siteDTO_.setLatitude( site.getLatitude() );

        siteDTO_.setLongitude( site.getLongitude() );

        return siteDTO_;
    }

    @Override

    public Site toEntity(SiteDTO siteDTO) {

        if ( siteDTO == null ) {

            return null;
        }

        Site site_ = new Site();

        site_.setCompany( companyMapper.fromId( siteDTO.getCompanyId() ) );

        site_.setId( siteDTO.getId() );

        site_.setZabbixProxyId( siteDTO.getZabbixProxyId() );

        site_.setLabel( siteDTO.getLabel() );

        site_.setAddress( siteDTO.getAddress() );

        site_.setLatitude( siteDTO.getLatitude() );

        site_.setLongitude( siteDTO.getLongitude() );

        return site_;
    }

    private Long siteCompanyId(Site site) {

        if ( site == null ) {

            return null;
        }

        Company company = site.getCompany();

        if ( company == null ) {

            return null;
        }

        Long id = company.getId();

        if ( id == null ) {

            return null;
        }

        return id;
    }
}

