import { browser, element, by, $ } from 'protractor';
import { NavBarPage } from './../page-objects/jhi-page-objects';
const path = require('path');

describe('HostType e2e test', () => {

    let navBarPage: NavBarPage;
    let hostTypeDialogPage: HostTypeDialogPage;
    let hostTypeComponentsPage: HostTypeComponentsPage;
    const fileToUpload = '../../../../main/webapp/content/images/logo-jhipster.png';
    const absolutePath = path.resolve(__dirname, fileToUpload);
    

    beforeAll(() => {
        browser.get('/');
        browser.waitForAngular();
        navBarPage = new NavBarPage();
        navBarPage.getSignInPage().autoSignInUsing('admin', 'admin');
        browser.waitForAngular();
    });

    it('should load HostTypes', () => {
        navBarPage.goToEntity('host-type');
        hostTypeComponentsPage = new HostTypeComponentsPage();
        expect(hostTypeComponentsPage.getTitle()).toMatch(/Host Types/);

    });

    it('should load create HostType dialog', () => {
        hostTypeComponentsPage.clickOnCreateButton();
        hostTypeDialogPage = new HostTypeDialogPage();
        expect(hostTypeDialogPage.getModalTitle()).toMatch(/Create or edit a Host Type/);
        hostTypeDialogPage.close();
    });

    it('should create and save HostTypes', () => {
        hostTypeComponentsPage.clickOnCreateButton();
        hostTypeDialogPage.setNameInput('name');
        expect(hostTypeDialogPage.getNameInput()).toMatch('name');
        hostTypeDialogPage.setLabelInput('label');
        expect(hostTypeDialogPage.getLabelInput()).toMatch('label');
        hostTypeDialogPage.setImgInput(absolutePath);
        hostTypeDialogPage.save();
        expect(hostTypeDialogPage.getSaveButton().isPresent()).toBeFalsy();
    }); 

    afterAll(() => {
        navBarPage.autoSignOut();
    });
});

export class HostTypeComponentsPage {
    createButton = element(by.css('.jh-create-entity'));
    title = element.all(by.css('custom-host-type div h2 span')).first();

    clickOnCreateButton() {
        return this.createButton.click();
    }

    getTitle() {
        return this.title.getText();
    }
}

export class HostTypeDialogPage {
    modalTitle = element(by.css('h4#myHostTypeLabel'));
    saveButton = element(by.css('.modal-footer .btn.btn-primary'));
    closeButton = element(by.css('button.close'));
    nameInput = element(by.css('input#field_name'));
    labelInput = element(by.css('input#field_label'));
    imgInput = element(by.css('input#file_img'));

    getModalTitle() {
        return this.modalTitle.getText();
    }

    setNameInput = function (name) {
        this.nameInput.sendKeys(name);
    }

    getNameInput = function () {
        return this.nameInput.getAttribute('value');
    }

    setLabelInput = function (label) {
        this.labelInput.sendKeys(label);
    }

    getLabelInput = function () {
        return this.labelInput.getAttribute('value');
    }

    setImgInput = function (img) {
        this.imgInput.sendKeys(img);
    }

    getImgInput = function () {
        return this.imgInput.getAttribute('value');
    }

    save() {
        this.saveButton.click();
    }

    close() {
        this.closeButton.click();
    }

    getSaveButton() {
        return this.saveButton;
    }
}
