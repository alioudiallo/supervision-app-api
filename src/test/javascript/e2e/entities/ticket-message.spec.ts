import { browser, element, by, $ } from 'protractor';
import { NavBarPage } from './../page-objects/jhi-page-objects';
const path = require('path');

describe('TicketMessage e2e test', () => {

    let navBarPage: NavBarPage;
    let ticketMessageDialogPage: TicketMessageDialogPage;
    let ticketMessageComponentsPage: TicketMessageComponentsPage;
    const fileToUpload = '../../../../main/webapp/content/images/logo-jhipster.png';
    const absolutePath = path.resolve(__dirname, fileToUpload);
    

    beforeAll(() => {
        browser.get('/');
        browser.waitForAngular();
        navBarPage = new NavBarPage();
        navBarPage.getSignInPage().autoSignInUsing('admin', 'admin');
        browser.waitForAngular();
    });

    it('should load TicketMessages', () => {
        navBarPage.goToEntity('ticket-message');
        ticketMessageComponentsPage = new TicketMessageComponentsPage();
        expect(ticketMessageComponentsPage.getTitle()).toMatch(/Ticket Messages/);

    });

    it('should load create TicketMessage dialog', () => {
        ticketMessageComponentsPage.clickOnCreateButton();
        ticketMessageDialogPage = new TicketMessageDialogPage();
        expect(ticketMessageDialogPage.getModalTitle()).toMatch(/Create or edit a Ticket Message/);
        ticketMessageDialogPage.close();
    });

    it('should create and save TicketMessages', () => {
        ticketMessageComponentsPage.clickOnCreateButton();
        ticketMessageDialogPage.setTicketIdInput('5');
        expect(ticketMessageDialogPage.getTicketIdInput()).toMatch('5');
        ticketMessageDialogPage.setTextInput('text');
        expect(ticketMessageDialogPage.getTextInput()).toMatch('text');
        ticketMessageDialogPage.setUserIdInput('5');
        expect(ticketMessageDialogPage.getUserIdInput()).toMatch('5');
        ticketMessageDialogPage.setUserNameInput('userName');
        expect(ticketMessageDialogPage.getUserNameInput()).toMatch('userName');
        ticketMessageDialogPage.setCreatedDateInput(12310020012301);
        expect(ticketMessageDialogPage.getCreatedDateInput()).toMatch('2001-12-31T02:30');
        ticketMessageDialogPage.save();
        expect(ticketMessageDialogPage.getSaveButton().isPresent()).toBeFalsy();
    }); 

    afterAll(() => {
        navBarPage.autoSignOut();
    });
});

export class TicketMessageComponentsPage {
    createButton = element(by.css('.jh-create-entity'));
    title = element.all(by.css('custom-ticket-message div h2 span')).first();

    clickOnCreateButton() {
        return this.createButton.click();
    }

    getTitle() {
        return this.title.getText();
    }
}

export class TicketMessageDialogPage {
    modalTitle = element(by.css('h4#myTicketMessageLabel'));
    saveButton = element(by.css('.modal-footer .btn.btn-primary'));
    closeButton = element(by.css('button.close'));
    ticketIdInput = element(by.css('input#field_ticketId'));
    textInput = element(by.css('input#field_text'));
    userIdInput = element(by.css('input#field_userId'));
    userNameInput = element(by.css('input#field_userName'));
    createdDateInput = element(by.css('input#field_createdDate'));

    getModalTitle() {
        return this.modalTitle.getText();
    }

    setTicketIdInput = function (ticketId) {
        this.ticketIdInput.sendKeys(ticketId);
    }

    getTicketIdInput = function () {
        return this.ticketIdInput.getAttribute('value');
    }

    setTextInput = function (text) {
        this.textInput.sendKeys(text);
    }

    getTextInput = function () {
        return this.textInput.getAttribute('value');
    }

    setUserIdInput = function (userId) {
        this.userIdInput.sendKeys(userId);
    }

    getUserIdInput = function () {
        return this.userIdInput.getAttribute('value');
    }

    setUserNameInput = function (userName) {
        this.userNameInput.sendKeys(userName);
    }

    getUserNameInput = function () {
        return this.userNameInput.getAttribute('value');
    }

    setCreatedDateInput = function (createdDate) {
        this.createdDateInput.sendKeys(createdDate);
    }

    getCreatedDateInput = function () {
        return this.createdDateInput.getAttribute('value');
    }

    save() {
        this.saveButton.click();
    }

    close() {
        this.closeButton.click();
    }

    getSaveButton() {
        return this.saveButton;
    }
}
