import { browser, element, by, $ } from 'protractor';
import { NavBarPage } from './../page-objects/jhi-page-objects';
const path = require('path');

describe('DemandStory e2e test', () => {

    let navBarPage: NavBarPage;
    let demandStoryDialogPage: DemandStoryDialogPage;
    let demandStoryComponentsPage: DemandStoryComponentsPage;
    const fileToUpload = '../../../../main/webapp/content/images/logo-jhipster.png';
    const absolutePath = path.resolve(__dirname, fileToUpload);
    

    beforeAll(() => {
        browser.get('/');
        browser.waitForAngular();
        navBarPage = new NavBarPage();
        navBarPage.getSignInPage().autoSignInUsing('admin', 'admin');
        browser.waitForAngular();
    });

    it('should load DemandStories', () => {
        navBarPage.goToEntity('demand-story');
        demandStoryComponentsPage = new DemandStoryComponentsPage();
        expect(demandStoryComponentsPage.getTitle()).toMatch(/Demand Stories/);

    });

    it('should load create DemandStory dialog', () => {
        demandStoryComponentsPage.clickOnCreateButton();
        demandStoryDialogPage = new DemandStoryDialogPage();
        expect(demandStoryDialogPage.getModalTitle()).toMatch(/Create or edit a Demand Story/);
        demandStoryDialogPage.close();
    });

    it('should create and save DemandStories', () => {
        demandStoryComponentsPage.clickOnCreateButton();
        demandStoryDialogPage.setCreatedDateInput(12310020012301);
        expect(demandStoryDialogPage.getCreatedDateInput()).toMatch('2001-12-31T02:30');
        demandStoryDialogPage.statusSelectLastOption();
        demandStoryDialogPage.adminSelectLastOption();
        demandStoryDialogPage.demandSelectLastOption();
        demandStoryDialogPage.save();
        expect(demandStoryDialogPage.getSaveButton().isPresent()).toBeFalsy();
    }); 

    afterAll(() => {
        navBarPage.autoSignOut();
    });
});

export class DemandStoryComponentsPage {
    createButton = element(by.css('.jh-create-entity'));
    title = element.all(by.css('custom-demand-story div h2 span')).first();

    clickOnCreateButton() {
        return this.createButton.click();
    }

    getTitle() {
        return this.title.getText();
    }
}

export class DemandStoryDialogPage {
    modalTitle = element(by.css('h4#myDemandStoryLabel'));
    saveButton = element(by.css('.modal-footer .btn.btn-primary'));
    closeButton = element(by.css('button.close'));
    createdDateInput = element(by.css('input#field_createdDate'));
    statusSelect = element(by.css('select#field_status'));
    adminSelect = element(by.css('select#field_admin'));
    demandSelect = element(by.css('select#field_demand'));

    getModalTitle() {
        return this.modalTitle.getText();
    }

    setCreatedDateInput = function (createdDate) {
        this.createdDateInput.sendKeys(createdDate);
    }

    getCreatedDateInput = function () {
        return this.createdDateInput.getAttribute('value');
    }

    setStatusSelect = function (status) {
        this.statusSelect.sendKeys(status);
    }

    getStatusSelect = function () {
        return this.statusSelect.element(by.css('option:checked')).getText();
    }

    statusSelectLastOption = function () {
        this.statusSelect.all(by.tagName('option')).last().click();
    }
    adminSelectLastOption = function () {
        this.adminSelect.all(by.tagName('option')).last().click();
    }

    adminSelectOption = function (option) {
        this.adminSelect.sendKeys(option);
    }

    getAdminSelect = function () {
        return this.adminSelect;
    }

    getAdminSelectedOption = function () {
        return this.adminSelect.element(by.css('option:checked')).getText();
    }

    demandSelectLastOption = function () {
        this.demandSelect.all(by.tagName('option')).last().click();
    }

    demandSelectOption = function (option) {
        this.demandSelect.sendKeys(option);
    }

    getDemandSelect = function () {
        return this.demandSelect;
    }

    getDemandSelectedOption = function () {
        return this.demandSelect.element(by.css('option:checked')).getText();
    }

    save() {
        this.saveButton.click();
    }

    close() {
        this.closeButton.click();
    }

    getSaveButton() {
        return this.saveButton;
    }
}
