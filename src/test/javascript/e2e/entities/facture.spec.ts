import { browser, element, by, $ } from 'protractor';
import { NavBarPage } from './../page-objects/jhi-page-objects';
const path = require('path');

describe('Facture e2e test', () => {

    let navBarPage: NavBarPage;
    let factureDialogPage: FactureDialogPage;
    let factureComponentsPage: FactureComponentsPage;
    const fileToUpload = '../../../../main/webapp/content/images/logo-jhipster.png';
    const absolutePath = path.resolve(__dirname, fileToUpload);
    

    beforeAll(() => {
        browser.get('/');
        browser.waitForAngular();
        navBarPage = new NavBarPage();
        navBarPage.getSignInPage().autoSignInUsing('admin', 'admin');
        browser.waitForAngular();
    });

    it('should load Factures', () => {
        navBarPage.goToEntity('facture');
        factureComponentsPage = new FactureComponentsPage();
        expect(factureComponentsPage.getTitle()).toMatch(/Factures/);

    });

    it('should load create Facture dialog', () => {
        factureComponentsPage.clickOnCreateButton();
        factureDialogPage = new FactureDialogPage();
        expect(factureDialogPage.getModalTitle()).toMatch(/Create or edit a Facture/);
        factureDialogPage.close();
    });

    it('should create and save Factures', () => {
        factureComponentsPage.clickOnCreateButton();
        factureDialogPage.setCreatedDateInput(12310020012301);
        expect(factureDialogPage.getCreatedDateInput()).toMatch('2001-12-31T02:30');
        factureDialogPage.setUpdatedDateInput(12310020012301);
        expect(factureDialogPage.getUpdatedDateInput()).toMatch('2001-12-31T02:30');
        factureDialogPage.setDateDebutInput(12310020012301);
        expect(factureDialogPage.getDateDebutInput()).toMatch('2001-12-31T02:30');
        factureDialogPage.setDateFinInput(12310020012301);
        expect(factureDialogPage.getDateFinInput()).toMatch('2001-12-31T02:30');
        factureDialogPage.setFichierFactureInput(absolutePath);
        factureDialogPage.save();
        expect(factureDialogPage.getSaveButton().isPresent()).toBeFalsy();
    }); 

    afterAll(() => {
        navBarPage.autoSignOut();
    });
});

export class FactureComponentsPage {
    createButton = element(by.css('.jh-create-entity'));
    title = element.all(by.css('custom-facture div h2 span')).first();

    clickOnCreateButton() {
        return this.createButton.click();
    }

    getTitle() {
        return this.title.getText();
    }
}

export class FactureDialogPage {
    modalTitle = element(by.css('h4#myFactureLabel'));
    saveButton = element(by.css('.modal-footer .btn.btn-primary'));
    closeButton = element(by.css('button.close'));
    createdDateInput = element(by.css('input#field_createdDate'));
    updatedDateInput = element(by.css('input#field_updatedDate'));
    dateDebutInput = element(by.css('input#field_dateDebut'));
    dateFinInput = element(by.css('input#field_dateFin'));
    fichierFactureInput = element(by.css('input#file_fichierFacture'));

    getModalTitle() {
        return this.modalTitle.getText();
    }

    setCreatedDateInput = function (createdDate) {
        this.createdDateInput.sendKeys(createdDate);
    }

    getCreatedDateInput = function () {
        return this.createdDateInput.getAttribute('value');
    }

    setUpdatedDateInput = function (updatedDate) {
        this.updatedDateInput.sendKeys(updatedDate);
    }

    getUpdatedDateInput = function () {
        return this.updatedDateInput.getAttribute('value');
    }

    setDateDebutInput = function (dateDebut) {
        this.dateDebutInput.sendKeys(dateDebut);
    }

    getDateDebutInput = function () {
        return this.dateDebutInput.getAttribute('value');
    }

    setDateFinInput = function (dateFin) {
        this.dateFinInput.sendKeys(dateFin);
    }

    getDateFinInput = function () {
        return this.dateFinInput.getAttribute('value');
    }

    setFichierFactureInput = function (fichierFacture) {
        this.fichierFactureInput.sendKeys(fichierFacture);
    }

    getFichierFactureInput = function () {
        return this.fichierFactureInput.getAttribute('value');
    }

    save() {
        this.saveButton.click();
    }

    close() {
        this.closeButton.click();
    }

    getSaveButton() {
        return this.saveButton;
    }
}
