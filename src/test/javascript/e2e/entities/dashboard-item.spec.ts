import { browser, element, by, $ } from 'protractor';
import { NavBarPage } from './../page-objects/jhi-page-objects';
const path = require('path');

describe('DashboardItem e2e test', () => {

    let navBarPage: NavBarPage;
    let dashboardItemDialogPage: DashboardItemDialogPage;
    let dashboardItemComponentsPage: DashboardItemComponentsPage;
    const fileToUpload = '../../../../main/webapp/content/images/logo-jhipster.png';
    const absolutePath = path.resolve(__dirname, fileToUpload);
    

    beforeAll(() => {
        browser.get('/');
        browser.waitForAngular();
        navBarPage = new NavBarPage();
        navBarPage.getSignInPage().autoSignInUsing('admin', 'admin');
        browser.waitForAngular();
    });

    it('should load DashboardItems', () => {
        navBarPage.goToEntity('dashboard-item');
        dashboardItemComponentsPage = new DashboardItemComponentsPage();
        expect(dashboardItemComponentsPage.getTitle()).toMatch(/Dashboard Items/);

    });

    it('should load create DashboardItem dialog', () => {
        dashboardItemComponentsPage.clickOnCreateButton();
        dashboardItemDialogPage = new DashboardItemDialogPage();
        expect(dashboardItemDialogPage.getModalTitle()).toMatch(/Create or edit a Dashboard Item/);
        dashboardItemDialogPage.close();
    });

    it('should create and save DashboardItems', () => {
        dashboardItemComponentsPage.clickOnCreateButton();
        dashboardItemDialogPage.setHostTypeInput('hostType');
        expect(dashboardItemDialogPage.getHostTypeInput()).toMatch('hostType');
        dashboardItemDialogPage.setNumberOfEquipmentsInput('numberOfEquipments');
        expect(dashboardItemDialogPage.getNumberOfEquipmentsInput()).toMatch('numberOfEquipments');
        dashboardItemDialogPage.setNumberOfTicketsInput('numberOfTickets');
        expect(dashboardItemDialogPage.getNumberOfTicketsInput()).toMatch('numberOfTickets');
        dashboardItemDialogPage.setColorInput('color');
        expect(dashboardItemDialogPage.getColorInput()).toMatch('color');
        dashboardItemDialogPage.setImgInput(absolutePath);
        dashboardItemDialogPage.save();
        expect(dashboardItemDialogPage.getSaveButton().isPresent()).toBeFalsy();
    }); 

    afterAll(() => {
        navBarPage.autoSignOut();
    });
});

export class DashboardItemComponentsPage {
    createButton = element(by.css('.jh-create-entity'));
    title = element.all(by.css('custom-dashboard-item div h2 span')).first();

    clickOnCreateButton() {
        return this.createButton.click();
    }

    getTitle() {
        return this.title.getText();
    }
}

export class DashboardItemDialogPage {
    modalTitle = element(by.css('h4#myDashboardItemLabel'));
    saveButton = element(by.css('.modal-footer .btn.btn-primary'));
    closeButton = element(by.css('button.close'));
    hostTypeInput = element(by.css('input#field_hostType'));
    numberOfEquipmentsInput = element(by.css('input#field_numberOfEquipments'));
    numberOfTicketsInput = element(by.css('input#field_numberOfTickets'));
    colorInput = element(by.css('input#field_color'));
    imgInput = element(by.css('input#file_img'));

    getModalTitle() {
        return this.modalTitle.getText();
    }

    setHostTypeInput = function (hostType) {
        this.hostTypeInput.sendKeys(hostType);
    }

    getHostTypeInput = function () {
        return this.hostTypeInput.getAttribute('value');
    }

    setNumberOfEquipmentsInput = function (numberOfEquipments) {
        this.numberOfEquipmentsInput.sendKeys(numberOfEquipments);
    }

    getNumberOfEquipmentsInput = function () {
        return this.numberOfEquipmentsInput.getAttribute('value');
    }

    setNumberOfTicketsInput = function (numberOfTickets) {
        this.numberOfTicketsInput.sendKeys(numberOfTickets);
    }

    getNumberOfTicketsInput = function () {
        return this.numberOfTicketsInput.getAttribute('value');
    }

    setColorInput = function (color) {
        this.colorInput.sendKeys(color);
    }

    getColorInput = function () {
        return this.colorInput.getAttribute('value');
    }

    setImgInput = function (img) {
        this.imgInput.sendKeys(img);
    }

    getImgInput = function () {
        return this.imgInput.getAttribute('value');
    }

    save() {
        this.saveButton.click();
    }

    close() {
        this.closeButton.click();
    }

    getSaveButton() {
        return this.saveButton;
    }
}
