/* tslint:disable max-line-length */
import { ComponentFixture, TestBed, async } from '@angular/core/testing';
import { DatePipe } from '@angular/common';
import { ActivatedRoute } from '@angular/router';
import { Observable } from 'rxjs/Rx';
import { JhiDateUtils, JhiDataUtils, JhiEventManager } from 'ng-jhipster';
import { SupervisionapiTestModule } from '../../../test.module';
import { MockActivatedRoute } from '../../../helpers/mock-route.service';
import { DemandStoryDetailComponent } from '../../../../../../main/webapp/app/entities/demand-story/demand-story-detail.component';
import { DemandStoryService } from '../../../../../../main/webapp/app/entities/demand-story/demand-story.service';
import { DemandStory } from '../../../../../../main/webapp/app/entities/demand-story/demand-story.model';

describe('Component Tests', () => {

    describe('DemandStory Management Detail Component', () => {
        let comp: DemandStoryDetailComponent;
        let fixture: ComponentFixture<DemandStoryDetailComponent>;
        let service: DemandStoryService;

        beforeEach(async(() => {
            TestBed.configureTestingModule({
                imports: [SupervisionapiTestModule],
                declarations: [DemandStoryDetailComponent],
                providers: [
                    JhiDateUtils,
                    JhiDataUtils,
                    DatePipe,
                    {
                        provide: ActivatedRoute,
                        useValue: new MockActivatedRoute({id: 123})
                    },
                    DemandStoryService,
                    JhiEventManager
                ]
            }).overrideTemplate(DemandStoryDetailComponent, '')
            .compileComponents();
        }));

        beforeEach(() => {
            fixture = TestBed.createComponent(DemandStoryDetailComponent);
            comp = fixture.componentInstance;
            service = fixture.debugElement.injector.get(DemandStoryService);
        });

        describe('OnInit', () => {
            it('Should call load all on init', () => {
            // GIVEN

            spyOn(service, 'find').and.returnValue(Observable.of(new DemandStory(10)));

            // WHEN
            comp.ngOnInit();

            // THEN
            expect(service.find).toHaveBeenCalledWith(123);
            expect(comp.demandStory).toEqual(jasmine.objectContaining({id: 10}));
            });
        });
    });

});
