import { Injectable } from '@angular/core';
import { Http, Response } from '@angular/http';
import { Observable } from 'rxjs/Rx';
import { SERVER_API_URL } from '../../app.constants';

import { JhiDateUtils } from 'ng-jhipster';

import { Contract } from './contract.model';
import { ResponseWrapper, createRequestOption } from '../../shared';

@Injectable()
export class ContractService {

    private resourceUrl = SERVER_API_URL + 'api/kaylene/contracts';

    constructor(private http: Http, private dateUtils: JhiDateUtils) { }

    create(contract: Contract): Observable<Contract> {
        const copy = this.convert(contract);
        return this.http.post(this.resourceUrl, copy).map((res: Response) => {
            const jsonResponse = res.json();
            return this.convertItemFromServer(jsonResponse);
        });
    }

    update(contract: Contract): Observable<Contract> {
        const copy = this.convert(contract);
        return this.http.put(this.resourceUrl, copy).map((res: Response) => {
            const jsonResponse = res.json();
            return this.convertItemFromServer(jsonResponse);
        });
    }

    find(id: number): Observable<Contract> {
        return this.http.get(`${this.resourceUrl}/${id}`).map((res: Response) => {
            const jsonResponse = res.json();
            return this.convertItemFromServer(jsonResponse);
        });
    }

    query(req?: any): Observable<ResponseWrapper> {
        const options = createRequestOption(req);
        return this.http.get(this.resourceUrl, options)
            .map((res: Response) => this.convertResponse(res));
    }

    delete(id: number): Observable<Response> {
        return this.http.delete(`${this.resourceUrl}/${id}`);
    }

    private convertResponse(res: Response): ResponseWrapper {
        const jsonResponse = res.json();
        const result = [];
        for (let i = 0; i < jsonResponse.length; i++) {
            result.push(this.convertItemFromServer(jsonResponse[i]));
        }
        return new ResponseWrapper(res.headers, result, res.status);
    }

    /**
     * Convert a returned JSON object to Contract.
     */
    private convertItemFromServer(json: any): Contract {
        const entity: Contract = Object.assign(new Contract(), json);
        entity.createDate = this.dateUtils
            .convertDateTimeFromServer(json.createDate);
        entity.beginDate = this.dateUtils
            .convertDateTimeFromServer(json.beginDate);
        entity.endDate = this.dateUtils
            .convertDateTimeFromServer(json.endDate);
        return entity;
    }

    /**
     * Convert a Contract to a JSON which can be sent to the server.
     */
    private convert(contract: Contract): Contract {
        const copy: Contract = Object.assign({}, contract);

        copy.createDate = this.dateUtils.toDate(contract.createDate);

        copy.beginDate = this.dateUtils.toDate(contract.beginDate);

        copy.endDate = this.dateUtils.toDate(contract.endDate);
        return copy;
    }
}
