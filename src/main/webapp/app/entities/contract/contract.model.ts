import { BaseEntity } from './../../shared';

export const enum ContractStatus {
    'REQUESTED',
    'VALIDATED',
    'ONGOING',
    'FINISH'
}

export class Contract implements BaseEntity {
    constructor(
        public id?: number,
        public currentStatus?: ContractStatus,
        public createDate?: any,
        public beginDate?: any,
        public endDate?: any,
        public companyId?: number,
        public offerId?: number,
        public hosts?: BaseEntity[],
    ) {
    }
}
