import { Component, OnInit, OnDestroy } from '@angular/core';
import { ActivatedRoute } from '@angular/router';
import { Subscription } from 'rxjs/Rx';
import { JhiEventManager } from 'ng-jhipster';

import { HostDemand } from './host-demand.model';
import { HostDemandService } from './host-demand.service';

@Component({
    selector: 'custom-host-demand-detail',
    templateUrl: './host-demand-detail.component.html'
})
export class HostDemandDetailComponent implements OnInit, OnDestroy {

    hostDemand: HostDemand;
    private subscription: Subscription;
    private eventSubscriber: Subscription;

    constructor(
        private eventManager: JhiEventManager,
        private hostDemandService: HostDemandService,
        private route: ActivatedRoute
    ) {
    }

    ngOnInit() {
        this.subscription = this.route.params.subscribe((params) => {
            this.load(params['id']);
        });
        this.registerChangeInHostDemands();
    }

    load(id) {
        this.hostDemandService.find(id).subscribe((hostDemand) => {
            this.hostDemand = hostDemand;
        });
    }
    previousState() {
        window.history.back();
    }

    ngOnDestroy() {
        this.subscription.unsubscribe();
        this.eventManager.destroy(this.eventSubscriber);
    }

    registerChangeInHostDemands() {
        this.eventSubscriber = this.eventManager.subscribe(
            'hostDemandListModification',
            (response) => this.load(this.hostDemand.id)
        );
    }
}
