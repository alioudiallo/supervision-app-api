import { NgModule, CUSTOM_ELEMENTS_SCHEMA } from '@angular/core';
import { RouterModule } from '@angular/router';

import { SupervisionapiSharedModule } from '../../shared';
import {
    HostDemandService,
    HostDemandPopupService,
    HostDemandComponent,
    HostDemandDetailComponent,
    HostDemandDialogComponent,
    HostDemandPopupComponent,
    HostDemandDeletePopupComponent,
    HostDemandDeleteDialogComponent,
    hostDemandRoute,
    hostDemandPopupRoute,
    HostDemandResolvePagingParams,
} from './';

const ENTITY_STATES = [
    ...hostDemandRoute,
    ...hostDemandPopupRoute,
];

@NgModule({
    imports: [
        SupervisionapiSharedModule,
        RouterModule.forRoot(ENTITY_STATES, { useHash: true })
    ],
    declarations: [
        HostDemandComponent,
        HostDemandDetailComponent,
        HostDemandDialogComponent,
        HostDemandDeleteDialogComponent,
        HostDemandPopupComponent,
        HostDemandDeletePopupComponent,
    ],
    entryComponents: [
        HostDemandComponent,
        HostDemandDialogComponent,
        HostDemandPopupComponent,
        HostDemandDeleteDialogComponent,
        HostDemandDeletePopupComponent,
    ],
    providers: [
        HostDemandService,
        HostDemandPopupService,
        HostDemandResolvePagingParams,
    ],
    schemas: [CUSTOM_ELEMENTS_SCHEMA]
})
export class SupervisionapiHostDemandModule {}
