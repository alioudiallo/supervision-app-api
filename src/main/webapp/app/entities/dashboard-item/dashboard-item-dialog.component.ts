import { Component, OnInit, OnDestroy } from '@angular/core';
import { ActivatedRoute } from '@angular/router';
import { Response } from '@angular/http';

import { Observable } from 'rxjs/Rx';
import { NgbActiveModal } from '@ng-bootstrap/ng-bootstrap';
import { JhiEventManager, JhiAlertService, JhiDataUtils } from 'ng-jhipster';

import { DashboardItem } from './dashboard-item.model';
import { DashboardItemPopupService } from './dashboard-item-popup.service';
import { DashboardItemService } from './dashboard-item.service';

@Component({
    selector: 'custom-dashboard-item-dialog',
    templateUrl: './dashboard-item-dialog.component.html'
})
export class DashboardItemDialogComponent implements OnInit {

    dashboardItem: DashboardItem;
    isSaving: boolean;

    constructor(
        public activeModal: NgbActiveModal,
        private dataUtils: JhiDataUtils,
        private jhiAlertService: JhiAlertService,
        private dashboardItemService: DashboardItemService,
        private eventManager: JhiEventManager
    ) {
    }

    ngOnInit() {
        this.isSaving = false;
    }

    byteSize(field) {
        return this.dataUtils.byteSize(field);
    }

    openFile(contentType, field) {
        return this.dataUtils.openFile(contentType, field);
    }

    setFileData(event, entity, field, isImage) {
        this.dataUtils.setFileData(event, entity, field, isImage);
    }

    clear() {
        this.activeModal.dismiss('cancel');
    }

    save() {
        this.isSaving = true;
        if (this.dashboardItem.id !== undefined) {
            this.subscribeToSaveResponse(
                this.dashboardItemService.update(this.dashboardItem));
        } else {
            this.subscribeToSaveResponse(
                this.dashboardItemService.create(this.dashboardItem));
        }
    }

    private subscribeToSaveResponse(result: Observable<DashboardItem>) {
        result.subscribe((res: DashboardItem) =>
            this.onSaveSuccess(res), (res: Response) => this.onSaveError());
    }

    private onSaveSuccess(result: DashboardItem) {
        this.eventManager.broadcast({ name: 'dashboardItemListModification', content: 'OK'});
        this.isSaving = false;
        this.activeModal.dismiss(result);
    }

    private onSaveError() {
        this.isSaving = false;
    }

    private onError(error: any) {
        this.jhiAlertService.error(error.message, null, null);
    }
}

@Component({
    selector: 'custom-dashboard-item-popup',
    template: ''
})
export class DashboardItemPopupComponent implements OnInit, OnDestroy {

    routeSub: any;

    constructor(
        private route: ActivatedRoute,
        private dashboardItemPopupService: DashboardItemPopupService
    ) {}

    ngOnInit() {
        this.routeSub = this.route.params.subscribe((params) => {
            if ( params['id'] ) {
                this.dashboardItemPopupService
                    .open(DashboardItemDialogComponent as Component, params['id']);
            } else {
                this.dashboardItemPopupService
                    .open(DashboardItemDialogComponent as Component);
            }
        });
    }

    ngOnDestroy() {
        this.routeSub.unsubscribe();
    }
}
