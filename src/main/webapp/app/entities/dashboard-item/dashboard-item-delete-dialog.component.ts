import { Component, OnInit, OnDestroy } from '@angular/core';
import { ActivatedRoute } from '@angular/router';

import { NgbActiveModal } from '@ng-bootstrap/ng-bootstrap';
import { JhiEventManager } from 'ng-jhipster';

import { DashboardItem } from './dashboard-item.model';
import { DashboardItemPopupService } from './dashboard-item-popup.service';
import { DashboardItemService } from './dashboard-item.service';

@Component({
    selector: 'custom-dashboard-item-delete-dialog',
    templateUrl: './dashboard-item-delete-dialog.component.html'
})
export class DashboardItemDeleteDialogComponent {

    dashboardItem: DashboardItem;

    constructor(
        private dashboardItemService: DashboardItemService,
        public activeModal: NgbActiveModal,
        private eventManager: JhiEventManager
    ) {
    }

    clear() {
        this.activeModal.dismiss('cancel');
    }

    confirmDelete(id: number) {
        this.dashboardItemService.delete(id).subscribe((response) => {
            this.eventManager.broadcast({
                name: 'dashboardItemListModification',
                content: 'Deleted an dashboardItem'
            });
            this.activeModal.dismiss(true);
        });
    }
}

@Component({
    selector: 'custom-dashboard-item-delete-popup',
    template: ''
})
export class DashboardItemDeletePopupComponent implements OnInit, OnDestroy {

    routeSub: any;

    constructor(
        private route: ActivatedRoute,
        private dashboardItemPopupService: DashboardItemPopupService
    ) {}

    ngOnInit() {
        this.routeSub = this.route.params.subscribe((params) => {
            this.dashboardItemPopupService
                .open(DashboardItemDeleteDialogComponent as Component, params['id']);
        });
    }

    ngOnDestroy() {
        this.routeSub.unsubscribe();
    }
}
