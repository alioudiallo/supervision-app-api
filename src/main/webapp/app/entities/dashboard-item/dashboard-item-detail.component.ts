import { Component, OnInit, OnDestroy } from '@angular/core';
import { ActivatedRoute } from '@angular/router';
import { Subscription } from 'rxjs/Rx';
import { JhiEventManager, JhiDataUtils } from 'ng-jhipster';

import { DashboardItem } from './dashboard-item.model';
import { DashboardItemService } from './dashboard-item.service';

@Component({
    selector: 'custom-dashboard-item-detail',
    templateUrl: './dashboard-item-detail.component.html'
})
export class DashboardItemDetailComponent implements OnInit, OnDestroy {

    dashboardItem: DashboardItem;
    private subscription: Subscription;
    private eventSubscriber: Subscription;

    constructor(
        private eventManager: JhiEventManager,
        private dataUtils: JhiDataUtils,
        private dashboardItemService: DashboardItemService,
        private route: ActivatedRoute
    ) {
    }

    ngOnInit() {
        this.subscription = this.route.params.subscribe((params) => {
            this.load(params['id']);
        });
        this.registerChangeInDashboardItems();
    }

    load(id) {
        this.dashboardItemService.find(id).subscribe((dashboardItem) => {
            this.dashboardItem = dashboardItem;
        });
    }
    byteSize(field) {
        return this.dataUtils.byteSize(field);
    }

    openFile(contentType, field) {
        return this.dataUtils.openFile(contentType, field);
    }
    previousState() {
        window.history.back();
    }

    ngOnDestroy() {
        this.subscription.unsubscribe();
        this.eventManager.destroy(this.eventSubscriber);
    }

    registerChangeInDashboardItems() {
        this.eventSubscriber = this.eventManager.subscribe(
            'dashboardItemListModification',
            (response) => this.load(this.dashboardItem.id)
        );
    }
}
