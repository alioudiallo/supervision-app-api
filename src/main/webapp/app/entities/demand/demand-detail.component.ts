import { Component, OnInit, OnDestroy } from '@angular/core';
import { ActivatedRoute } from '@angular/router';
import { Subscription } from 'rxjs/Rx';
import { JhiEventManager } from 'ng-jhipster';

import { Demand } from './demand.model';
import { DemandService } from './demand.service';

@Component({
    selector: 'custom-demand-detail',
    templateUrl: './demand-detail.component.html'
})
export class DemandDetailComponent implements OnInit, OnDestroy {

    demand: Demand;
    private subscription: Subscription;
    private eventSubscriber: Subscription;

    constructor(
        private eventManager: JhiEventManager,
        private demandService: DemandService,
        private route: ActivatedRoute
    ) {
    }

    ngOnInit() {
        this.subscription = this.route.params.subscribe((params) => {
            this.load(params['id']);
        });
        this.registerChangeInDemands();
    }

    load(id) {
        this.demandService.find(id).subscribe((demand) => {
            this.demand = demand;
        });
    }
    previousState() {
        window.history.back();
    }

    ngOnDestroy() {
        this.subscription.unsubscribe();
        this.eventManager.destroy(this.eventSubscriber);
    }

    registerChangeInDemands() {
        this.eventSubscriber = this.eventManager.subscribe(
            'demandListModification',
            (response) => this.load(this.demand.id)
        );
    }
}
