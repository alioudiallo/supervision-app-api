import { Injectable } from '@angular/core';
import { Resolve, ActivatedRouteSnapshot, RouterStateSnapshot, Routes } from '@angular/router';

import { UserRouteAccessService } from '../../shared';
import { JhiPaginationUtil } from 'ng-jhipster';

import { DemandComponent } from './demand.component';
import { DemandDetailComponent } from './demand-detail.component';
import { DemandPopupComponent } from './demand-dialog.component';
import { DemandDeletePopupComponent } from './demand-delete-dialog.component';

@Injectable()
export class DemandResolvePagingParams implements Resolve<any> {

    constructor(private paginationUtil: JhiPaginationUtil) {}

    resolve(route: ActivatedRouteSnapshot, state: RouterStateSnapshot) {
        const page = route.queryParams['page'] ? route.queryParams['page'] : '1';
        const sort = route.queryParams['sort'] ? route.queryParams['sort'] : 'id,asc';
        return {
            page: this.paginationUtil.parsePage(page),
            predicate: this.paginationUtil.parsePredicate(sort),
            ascending: this.paginationUtil.parseAscending(sort)
      };
    }
}

export const demandRoute: Routes = [
    {
        path: 'demand',
        component: DemandComponent,
        resolve: {
            'pagingParams': DemandResolvePagingParams
        },
        data: {
            authorities: ['ROLE_USER'],
            pageTitle: 'Demands'
        },
        canActivate: [UserRouteAccessService]
    }, {
        path: 'demand/:id',
        component: DemandDetailComponent,
        data: {
            authorities: ['ROLE_USER'],
            pageTitle: 'Demands'
        },
        canActivate: [UserRouteAccessService]
    }
];

export const demandPopupRoute: Routes = [
    {
        path: 'demand-new',
        component: DemandPopupComponent,
        data: {
            authorities: ['ROLE_USER'],
            pageTitle: 'Demands'
        },
        canActivate: [UserRouteAccessService],
        outlet: 'popup'
    },
    {
        path: 'demand/:id/edit',
        component: DemandPopupComponent,
        data: {
            authorities: ['ROLE_USER'],
            pageTitle: 'Demands'
        },
        canActivate: [UserRouteAccessService],
        outlet: 'popup'
    },
    {
        path: 'demand/:id/delete',
        component: DemandDeletePopupComponent,
        data: {
            authorities: ['ROLE_USER'],
            pageTitle: 'Demands'
        },
        canActivate: [UserRouteAccessService],
        outlet: 'popup'
    }
];
