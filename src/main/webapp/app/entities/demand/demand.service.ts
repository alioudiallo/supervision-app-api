import { Injectable } from '@angular/core';
import { Http, Response } from '@angular/http';
import { Observable } from 'rxjs/Rx';
import { SERVER_API_URL } from '../../app.constants';

import { JhiDateUtils } from 'ng-jhipster';

import { Demand } from './demand.model';
import { ResponseWrapper, createRequestOption } from '../../shared';

@Injectable()
export class DemandService {

    private resourceUrl = SERVER_API_URL + 'api/kaylene/demands';

    constructor(private http: Http, private dateUtils: JhiDateUtils) { }

    create(demand: Demand): Observable<Demand> {
        const copy = this.convert(demand);
        return this.http.post(this.resourceUrl, copy).map((res: Response) => {
            const jsonResponse = res.json();
            return this.convertItemFromServer(jsonResponse);
        });
    }

    update(demand: Demand): Observable<Demand> {
        const copy = this.convert(demand);
        return this.http.put(this.resourceUrl, copy).map((res: Response) => {
            const jsonResponse = res.json();
            return this.convertItemFromServer(jsonResponse);
        });
    }

    find(id: number): Observable<Demand> {
        return this.http.get(`${this.resourceUrl}/${id}`).map((res: Response) => {
            const jsonResponse = res.json();
            return this.convertItemFromServer(jsonResponse);
        });
    }

    query(req?: any): Observable<ResponseWrapper> {
        const options = createRequestOption(req);
        return this.http.get(this.resourceUrl, options)
            .map((res: Response) => this.convertResponse(res));
    }

    delete(id: number): Observable<Response> {
        return this.http.delete(`${this.resourceUrl}/${id}`);
    }

    private convertResponse(res: Response): ResponseWrapper {
        const jsonResponse = res.json();
        const result = [];
        for (let i = 0; i < jsonResponse.length; i++) {
            result.push(this.convertItemFromServer(jsonResponse[i]));
        }
        return new ResponseWrapper(res.headers, result, res.status);
    }

    /**
     * Convert a returned JSON object to Demand.
     */
    private convertItemFromServer(json: any): Demand {
        const entity: Demand = Object.assign(new Demand(), json);
        entity.createdDate = this.dateUtils
            .convertDateTimeFromServer(json.createdDate);
        entity.lastModifiedDate = this.dateUtils
            .convertDateTimeFromServer(json.lastModifiedDate);
        return entity;
    }

    /**
     * Convert a Demand to a JSON which can be sent to the server.
     */
    private convert(demand: Demand): Demand {
        const copy: Demand = Object.assign({}, demand);

        copy.createdDate = this.dateUtils.toDate(demand.createdDate);

        copy.lastModifiedDate = this.dateUtils.toDate(demand.lastModifiedDate);
        return copy;
    }
}
