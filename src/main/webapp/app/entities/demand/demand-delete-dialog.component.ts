import { Component, OnInit, OnDestroy } from '@angular/core';
import { ActivatedRoute } from '@angular/router';

import { NgbActiveModal } from '@ng-bootstrap/ng-bootstrap';
import { JhiEventManager } from 'ng-jhipster';

import { Demand } from './demand.model';
import { DemandPopupService } from './demand-popup.service';
import { DemandService } from './demand.service';

@Component({
    selector: 'custom-demand-delete-dialog',
    templateUrl: './demand-delete-dialog.component.html'
})
export class DemandDeleteDialogComponent {

    demand: Demand;

    constructor(
        private demandService: DemandService,
        public activeModal: NgbActiveModal,
        private eventManager: JhiEventManager
    ) {
    }

    clear() {
        this.activeModal.dismiss('cancel');
    }

    confirmDelete(id: number) {
        this.demandService.delete(id).subscribe((response) => {
            this.eventManager.broadcast({
                name: 'demandListModification',
                content: 'Deleted an demand'
            });
            this.activeModal.dismiss(true);
        });
    }
}

@Component({
    selector: 'custom-demand-delete-popup',
    template: ''
})
export class DemandDeletePopupComponent implements OnInit, OnDestroy {

    routeSub: any;

    constructor(
        private route: ActivatedRoute,
        private demandPopupService: DemandPopupService
    ) {}

    ngOnInit() {
        this.routeSub = this.route.params.subscribe((params) => {
            this.demandPopupService
                .open(DemandDeleteDialogComponent as Component, params['id']);
        });
    }

    ngOnDestroy() {
        this.routeSub.unsubscribe();
    }
}
