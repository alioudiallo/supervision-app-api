import { NgModule, CUSTOM_ELEMENTS_SCHEMA } from '@angular/core';
import { RouterModule } from '@angular/router';

import { SupervisionapiSharedModule } from '../../shared';
import {
    HostContractService,
    HostContractPopupService,
    HostContractComponent,
    HostContractDetailComponent,
    HostContractDialogComponent,
    HostContractPopupComponent,
    HostContractDeletePopupComponent,
    HostContractDeleteDialogComponent,
    hostContractRoute,
    hostContractPopupRoute,
    HostContractResolvePagingParams,
} from './';

const ENTITY_STATES = [
    ...hostContractRoute,
    ...hostContractPopupRoute,
];

@NgModule({
    imports: [
        SupervisionapiSharedModule,
        RouterModule.forRoot(ENTITY_STATES, { useHash: true })
    ],
    declarations: [
        HostContractComponent,
        HostContractDetailComponent,
        HostContractDialogComponent,
        HostContractDeleteDialogComponent,
        HostContractPopupComponent,
        HostContractDeletePopupComponent,
    ],
    entryComponents: [
        HostContractComponent,
        HostContractDialogComponent,
        HostContractPopupComponent,
        HostContractDeleteDialogComponent,
        HostContractDeletePopupComponent,
    ],
    providers: [
        HostContractService,
        HostContractPopupService,
        HostContractResolvePagingParams,
    ],
    schemas: [CUSTOM_ELEMENTS_SCHEMA]
})
export class SupervisionapiHostContractModule {}
