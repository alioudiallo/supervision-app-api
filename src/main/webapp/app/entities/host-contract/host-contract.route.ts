import { Injectable } from '@angular/core';
import { Resolve, ActivatedRouteSnapshot, RouterStateSnapshot, Routes } from '@angular/router';

import { UserRouteAccessService } from '../../shared';
import { JhiPaginationUtil } from 'ng-jhipster';

import { HostContractComponent } from './host-contract.component';
import { HostContractDetailComponent } from './host-contract-detail.component';
import { HostContractPopupComponent } from './host-contract-dialog.component';
import { HostContractDeletePopupComponent } from './host-contract-delete-dialog.component';

@Injectable()
export class HostContractResolvePagingParams implements Resolve<any> {

    constructor(private paginationUtil: JhiPaginationUtil) {}

    resolve(route: ActivatedRouteSnapshot, state: RouterStateSnapshot) {
        const page = route.queryParams['page'] ? route.queryParams['page'] : '1';
        const sort = route.queryParams['sort'] ? route.queryParams['sort'] : 'id,asc';
        return {
            page: this.paginationUtil.parsePage(page),
            predicate: this.paginationUtil.parsePredicate(sort),
            ascending: this.paginationUtil.parseAscending(sort)
      };
    }
}

export const hostContractRoute: Routes = [
    {
        path: 'host-contract',
        component: HostContractComponent,
        resolve: {
            'pagingParams': HostContractResolvePagingParams
        },
        data: {
            authorities: ['ROLE_USER'],
            pageTitle: 'HostContracts'
        },
        canActivate: [UserRouteAccessService]
    }, {
        path: 'host-contract/:id',
        component: HostContractDetailComponent,
        data: {
            authorities: ['ROLE_USER'],
            pageTitle: 'HostContracts'
        },
        canActivate: [UserRouteAccessService]
    }
];

export const hostContractPopupRoute: Routes = [
    {
        path: 'host-contract-new',
        component: HostContractPopupComponent,
        data: {
            authorities: ['ROLE_USER'],
            pageTitle: 'HostContracts'
        },
        canActivate: [UserRouteAccessService],
        outlet: 'popup'
    },
    {
        path: 'host-contract/:id/edit',
        component: HostContractPopupComponent,
        data: {
            authorities: ['ROLE_USER'],
            pageTitle: 'HostContracts'
        },
        canActivate: [UserRouteAccessService],
        outlet: 'popup'
    },
    {
        path: 'host-contract/:id/delete',
        component: HostContractDeletePopupComponent,
        data: {
            authorities: ['ROLE_USER'],
            pageTitle: 'HostContracts'
        },
        canActivate: [UserRouteAccessService],
        outlet: 'popup'
    }
];
