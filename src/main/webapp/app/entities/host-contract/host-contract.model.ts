import { BaseEntity } from './../../shared';

export class HostContract implements BaseEntity {
    constructor(
        public id?: number,
        public zabbixHostId?: string,
        public contractId?: number,
    ) {
    }
}
