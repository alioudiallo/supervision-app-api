import { Component, OnInit, OnDestroy } from '@angular/core';
import { ActivatedRoute } from '@angular/router';
import { Subscription } from 'rxjs/Rx';
import { JhiEventManager } from 'ng-jhipster';

import { DemandStory } from './demand-story.model';
import { DemandStoryService } from './demand-story.service';

@Component({
    selector: 'custom-demand-story-detail',
    templateUrl: './demand-story-detail.component.html'
})
export class DemandStoryDetailComponent implements OnInit, OnDestroy {

    demandStory: DemandStory;
    private subscription: Subscription;
    private eventSubscriber: Subscription;

    constructor(
        private eventManager: JhiEventManager,
        private demandStoryService: DemandStoryService,
        private route: ActivatedRoute
    ) {
    }

    ngOnInit() {
        this.subscription = this.route.params.subscribe((params) => {
            this.load(params['id']);
        });
        this.registerChangeInDemandStories();
    }

    load(id) {
        this.demandStoryService.find(id).subscribe((demandStory) => {
            this.demandStory = demandStory;
        });
    }
    previousState() {
        window.history.back();
    }

    ngOnDestroy() {
        this.subscription.unsubscribe();
        this.eventManager.destroy(this.eventSubscriber);
    }

    registerChangeInDemandStories() {
        this.eventSubscriber = this.eventManager.subscribe(
            'demandStoryListModification',
            (response) => this.load(this.demandStory.id)
        );
    }
}
