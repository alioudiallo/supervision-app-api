import { Injectable } from '@angular/core';
import { Http, Response } from '@angular/http';
import { Observable } from 'rxjs/Rx';
import { SERVER_API_URL } from '../../app.constants';

import { JhiDateUtils } from 'ng-jhipster';

import { DemandStory } from './demand-story.model';
import { ResponseWrapper, createRequestOption } from '../../shared';

@Injectable()
export class DemandStoryService {

    private resourceUrl = SERVER_API_URL + 'api/demand-stories';

    constructor(private http: Http, private dateUtils: JhiDateUtils) { }

    create(demandStory: DemandStory): Observable<DemandStory> {
        const copy = this.convert(demandStory);
        return this.http.post(this.resourceUrl, copy).map((res: Response) => {
            const jsonResponse = res.json();
            return this.convertItemFromServer(jsonResponse);
        });
    }

    update(demandStory: DemandStory): Observable<DemandStory> {
        const copy = this.convert(demandStory);
        return this.http.put(this.resourceUrl, copy).map((res: Response) => {
            const jsonResponse = res.json();
            return this.convertItemFromServer(jsonResponse);
        });
    }

    find(id: number): Observable<DemandStory> {
        return this.http.get(`${this.resourceUrl}/${id}`).map((res: Response) => {
            const jsonResponse = res.json();
            return this.convertItemFromServer(jsonResponse);
        });
    }

    query(req?: any): Observable<ResponseWrapper> {
        const options = createRequestOption(req);
        return this.http.get(this.resourceUrl, options)
            .map((res: Response) => this.convertResponse(res));
    }

    delete(id: number): Observable<Response> {
        return this.http.delete(`${this.resourceUrl}/${id}`);
    }

    private convertResponse(res: Response): ResponseWrapper {
        const jsonResponse = res.json();
        const result = [];
        for (let i = 0; i < jsonResponse.length; i++) {
            result.push(this.convertItemFromServer(jsonResponse[i]));
        }
        return new ResponseWrapper(res.headers, result, res.status);
    }

    /**
     * Convert a returned JSON object to DemandStory.
     */
    private convertItemFromServer(json: any): DemandStory {
        const entity: DemandStory = Object.assign(new DemandStory(), json);
        entity.createdDate = this.dateUtils
            .convertDateTimeFromServer(json.createdDate);
        return entity;
    }

    /**
     * Convert a DemandStory to a JSON which can be sent to the server.
     */
    private convert(demandStory: DemandStory): DemandStory {
        const copy: DemandStory = Object.assign({}, demandStory);

        copy.createdDate = this.dateUtils.toDate(demandStory.createdDate);
        return copy;
    }
}
