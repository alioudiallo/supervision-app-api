import { BaseEntity } from './../../shared';

export const enum DemandStatus {
    'REQUESTED',
    'ACCEPTED',
    'REJECTED',
    'DONE',
    'INCOMPLETE'
}

export class DemandStory implements BaseEntity {
    constructor(
        public id?: number,
        public createdDate?: any,
        public status?: DemandStatus,
        public adminId?: number,
        public demandId?: number,
    ) {
    }
}
