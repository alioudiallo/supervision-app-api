import { NgModule, CUSTOM_ELEMENTS_SCHEMA } from '@angular/core';
import { RouterModule } from '@angular/router';

import { SupervisionapiSharedModule } from '../../shared';
import { SupervisionapiAdminModule } from '../../admin/admin.module';
import {
    DemandStoryService,
    DemandStoryPopupService,
    DemandStoryComponent,
    DemandStoryDetailComponent,
    DemandStoryDialogComponent,
    DemandStoryPopupComponent,
    DemandStoryDeletePopupComponent,
    DemandStoryDeleteDialogComponent,
    demandStoryRoute,
    demandStoryPopupRoute,
    DemandStoryResolvePagingParams,
} from './';

const ENTITY_STATES = [
    ...demandStoryRoute,
    ...demandStoryPopupRoute,
];

@NgModule({
    imports: [
        SupervisionapiSharedModule,
        SupervisionapiAdminModule,
        RouterModule.forRoot(ENTITY_STATES, { useHash: true })
    ],
    declarations: [
        DemandStoryComponent,
        DemandStoryDetailComponent,
        DemandStoryDialogComponent,
        DemandStoryDeleteDialogComponent,
        DemandStoryPopupComponent,
        DemandStoryDeletePopupComponent,
    ],
    entryComponents: [
        DemandStoryComponent,
        DemandStoryDialogComponent,
        DemandStoryPopupComponent,
        DemandStoryDeleteDialogComponent,
        DemandStoryDeletePopupComponent,
    ],
    providers: [
        DemandStoryService,
        DemandStoryPopupService,
        DemandStoryResolvePagingParams,
    ],
    schemas: [CUSTOM_ELEMENTS_SCHEMA]
})
export class SupervisionapiDemandStoryModule {}
