import { BaseEntity } from './../../shared';

export class TicketMessage implements BaseEntity {
    constructor(
        public id?: number,
        public ticketId?: number,
        public text?: string,
        public userId?: number,
        public userName?: string,
        public createdDate?: any,
    ) {
    }
}
