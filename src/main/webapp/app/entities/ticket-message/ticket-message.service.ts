import { Injectable } from '@angular/core';
import { Http, Response } from '@angular/http';
import { Observable } from 'rxjs/Rx';
import { SERVER_API_URL } from '../../app.constants';

import { JhiDateUtils } from 'ng-jhipster';

import { TicketMessage } from './ticket-message.model';
import { ResponseWrapper, createRequestOption } from '../../shared';

@Injectable()
export class TicketMessageService {

    private resourceUrl = SERVER_API_URL + 'api/ticket-messages';

    constructor(private http: Http, private dateUtils: JhiDateUtils) { }

    create(ticketMessage: TicketMessage): Observable<TicketMessage> {
        const copy = this.convert(ticketMessage);
        return this.http.post(this.resourceUrl, copy).map((res: Response) => {
            const jsonResponse = res.json();
            return this.convertItemFromServer(jsonResponse);
        });
    }

    update(ticketMessage: TicketMessage): Observable<TicketMessage> {
        const copy = this.convert(ticketMessage);
        return this.http.put(this.resourceUrl, copy).map((res: Response) => {
            const jsonResponse = res.json();
            return this.convertItemFromServer(jsonResponse);
        });
    }

    find(id: number): Observable<TicketMessage> {
        return this.http.get(`${this.resourceUrl}/${id}`).map((res: Response) => {
            const jsonResponse = res.json();
            return this.convertItemFromServer(jsonResponse);
        });
    }

    query(req?: any): Observable<ResponseWrapper> {
        const options = createRequestOption(req);
        return this.http.get(this.resourceUrl, options)
            .map((res: Response) => this.convertResponse(res));
    }

    delete(id: number): Observable<Response> {
        return this.http.delete(`${this.resourceUrl}/${id}`);
    }

    private convertResponse(res: Response): ResponseWrapper {
        const jsonResponse = res.json();
        const result = [];
        for (let i = 0; i < jsonResponse.length; i++) {
            result.push(this.convertItemFromServer(jsonResponse[i]));
        }
        return new ResponseWrapper(res.headers, result, res.status);
    }

    /**
     * Convert a returned JSON object to TicketMessage.
     */
    private convertItemFromServer(json: any): TicketMessage {
        const entity: TicketMessage = Object.assign(new TicketMessage(), json);
        entity.createdDate = this.dateUtils
            .convertDateTimeFromServer(json.createdDate);
        return entity;
    }

    /**
     * Convert a TicketMessage to a JSON which can be sent to the server.
     */
    private convert(ticketMessage: TicketMessage): TicketMessage {
        const copy: TicketMessage = Object.assign({}, ticketMessage);

        copy.createdDate = this.dateUtils.toDate(ticketMessage.createdDate);
        return copy;
    }
}
