import { Component, OnInit, OnDestroy } from '@angular/core';
import { ActivatedRoute } from '@angular/router';

import { NgbActiveModal } from '@ng-bootstrap/ng-bootstrap';
import { JhiEventManager } from 'ng-jhipster';

import { TicketMessage } from './ticket-message.model';
import { TicketMessagePopupService } from './ticket-message-popup.service';
import { TicketMessageService } from './ticket-message.service';

@Component({
    selector: 'custom-ticket-message-delete-dialog',
    templateUrl: './ticket-message-delete-dialog.component.html'
})
export class TicketMessageDeleteDialogComponent {

    ticketMessage: TicketMessage;

    constructor(
        private ticketMessageService: TicketMessageService,
        public activeModal: NgbActiveModal,
        private eventManager: JhiEventManager
    ) {
    }

    clear() {
        this.activeModal.dismiss('cancel');
    }

    confirmDelete(id: number) {
        this.ticketMessageService.delete(id).subscribe((response) => {
            this.eventManager.broadcast({
                name: 'ticketMessageListModification',
                content: 'Deleted an ticketMessage'
            });
            this.activeModal.dismiss(true);
        });
    }
}

@Component({
    selector: 'custom-ticket-message-delete-popup',
    template: ''
})
export class TicketMessageDeletePopupComponent implements OnInit, OnDestroy {

    routeSub: any;

    constructor(
        private route: ActivatedRoute,
        private ticketMessagePopupService: TicketMessagePopupService
    ) {}

    ngOnInit() {
        this.routeSub = this.route.params.subscribe((params) => {
            this.ticketMessagePopupService
                .open(TicketMessageDeleteDialogComponent as Component, params['id']);
        });
    }

    ngOnDestroy() {
        this.routeSub.unsubscribe();
    }
}
