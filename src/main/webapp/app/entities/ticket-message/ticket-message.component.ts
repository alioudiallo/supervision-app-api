import { Component, OnInit, OnDestroy } from '@angular/core';
import { ActivatedRoute, Router } from '@angular/router';
import { Subscription } from 'rxjs/Rx';
import { JhiEventManager, JhiParseLinks, JhiAlertService } from 'ng-jhipster';

import { TicketMessage } from './ticket-message.model';
import { TicketMessageService } from './ticket-message.service';
import { ITEMS_PER_PAGE, Principal, ResponseWrapper } from '../../shared';

@Component({
    selector: 'custom-ticket-message',
    templateUrl: './ticket-message.component.html'
})
export class TicketMessageComponent implements OnInit, OnDestroy {
ticketMessages: TicketMessage[];
    currentAccount: any;
    eventSubscriber: Subscription;

    constructor(
        private ticketMessageService: TicketMessageService,
        private jhiAlertService: JhiAlertService,
        private eventManager: JhiEventManager,
        private principal: Principal
    ) {
    }

    loadAll() {
        this.ticketMessageService.query().subscribe(
            (res: ResponseWrapper) => {
                this.ticketMessages = res.json;
            },
            (res: ResponseWrapper) => this.onError(res.json)
        );
    }
    ngOnInit() {
        this.loadAll();
        this.principal.identity().then((account) => {
            this.currentAccount = account;
        });
        this.registerChangeInTicketMessages();
    }

    ngOnDestroy() {
        this.eventManager.destroy(this.eventSubscriber);
    }

    trackId(index: number, item: TicketMessage) {
        return item.id;
    }
    registerChangeInTicketMessages() {
        this.eventSubscriber = this.eventManager.subscribe('ticketMessageListModification', (response) => this.loadAll());
    }

    private onError(error) {
        this.jhiAlertService.error(error.message, null, null);
    }
}
