import { Injectable } from '@angular/core';
import { Resolve, ActivatedRouteSnapshot, RouterStateSnapshot, Routes } from '@angular/router';

import { UserRouteAccessService } from '../../shared';
import { JhiPaginationUtil } from 'ng-jhipster';

import { HostTypeComponent } from './host-type.component';
import { HostTypeDetailComponent } from './host-type-detail.component';
import { HostTypePopupComponent } from './host-type-dialog.component';
import { HostTypeDeletePopupComponent } from './host-type-delete-dialog.component';

export const hostTypeRoute: Routes = [
    {
        path: 'host-type',
        component: HostTypeComponent,
        data: {
            authorities: ['ROLE_USER'],
            pageTitle: 'HostTypes'
        },
        canActivate: [UserRouteAccessService]
    }, {
        path: 'host-type/:id',
        component: HostTypeDetailComponent,
        data: {
            authorities: ['ROLE_USER'],
            pageTitle: 'HostTypes'
        },
        canActivate: [UserRouteAccessService]
    }
];

export const hostTypePopupRoute: Routes = [
    {
        path: 'host-type-new',
        component: HostTypePopupComponent,
        data: {
            authorities: ['ROLE_USER'],
            pageTitle: 'HostTypes'
        },
        canActivate: [UserRouteAccessService],
        outlet: 'popup'
    },
    {
        path: 'host-type/:id/edit',
        component: HostTypePopupComponent,
        data: {
            authorities: ['ROLE_USER'],
            pageTitle: 'HostTypes'
        },
        canActivate: [UserRouteAccessService],
        outlet: 'popup'
    },
    {
        path: 'host-type/:id/delete',
        component: HostTypeDeletePopupComponent,
        data: {
            authorities: ['ROLE_USER'],
            pageTitle: 'HostTypes'
        },
        canActivate: [UserRouteAccessService],
        outlet: 'popup'
    }
];
