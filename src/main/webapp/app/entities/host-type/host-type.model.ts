import { BaseEntity } from './../../shared';

export class HostType implements BaseEntity {
    constructor(
        public id?: number,
        public name?: string,
        public label?: string,
        public imgContentType?: string,
        public img?: any,
    ) {
    }
}
