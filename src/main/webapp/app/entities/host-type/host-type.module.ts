import { NgModule, CUSTOM_ELEMENTS_SCHEMA } from '@angular/core';
import { RouterModule } from '@angular/router';

import { SupervisionapiSharedModule } from '../../shared';
import {
    HostTypeService,
    HostTypePopupService,
    HostTypeComponent,
    HostTypeDetailComponent,
    HostTypeDialogComponent,
    HostTypePopupComponent,
    HostTypeDeletePopupComponent,
    HostTypeDeleteDialogComponent,
    hostTypeRoute,
    hostTypePopupRoute,
} from './';

const ENTITY_STATES = [
    ...hostTypeRoute,
    ...hostTypePopupRoute,
];

@NgModule({
    imports: [
        SupervisionapiSharedModule,
        RouterModule.forRoot(ENTITY_STATES, { useHash: true })
    ],
    declarations: [
        HostTypeComponent,
        HostTypeDetailComponent,
        HostTypeDialogComponent,
        HostTypeDeleteDialogComponent,
        HostTypePopupComponent,
        HostTypeDeletePopupComponent,
    ],
    entryComponents: [
        HostTypeComponent,
        HostTypeDialogComponent,
        HostTypePopupComponent,
        HostTypeDeleteDialogComponent,
        HostTypeDeletePopupComponent,
    ],
    providers: [
        HostTypeService,
        HostTypePopupService,
    ],
    schemas: [CUSTOM_ELEMENTS_SCHEMA]
})
export class SupervisionapiHostTypeModule {}
