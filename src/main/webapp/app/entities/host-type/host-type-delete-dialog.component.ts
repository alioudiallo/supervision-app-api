import { Component, OnInit, OnDestroy } from '@angular/core';
import { ActivatedRoute } from '@angular/router';

import { NgbActiveModal } from '@ng-bootstrap/ng-bootstrap';
import { JhiEventManager } from 'ng-jhipster';

import { HostType } from './host-type.model';
import { HostTypePopupService } from './host-type-popup.service';
import { HostTypeService } from './host-type.service';

@Component({
    selector: 'custom-host-type-delete-dialog',
    templateUrl: './host-type-delete-dialog.component.html'
})
export class HostTypeDeleteDialogComponent {

    hostType: HostType;

    constructor(
        private hostTypeService: HostTypeService,
        public activeModal: NgbActiveModal,
        private eventManager: JhiEventManager
    ) {
    }

    clear() {
        this.activeModal.dismiss('cancel');
    }

    confirmDelete(id: number) {
        this.hostTypeService.delete(id).subscribe((response) => {
            this.eventManager.broadcast({
                name: 'hostTypeListModification',
                content: 'Deleted an hostType'
            });
            this.activeModal.dismiss(true);
        });
    }
}

@Component({
    selector: 'custom-host-type-delete-popup',
    template: ''
})
export class HostTypeDeletePopupComponent implements OnInit, OnDestroy {

    routeSub: any;

    constructor(
        private route: ActivatedRoute,
        private hostTypePopupService: HostTypePopupService
    ) {}

    ngOnInit() {
        this.routeSub = this.route.params.subscribe((params) => {
            this.hostTypePopupService
                .open(HostTypeDeleteDialogComponent as Component, params['id']);
        });
    }

    ngOnDestroy() {
        this.routeSub.unsubscribe();
    }
}
