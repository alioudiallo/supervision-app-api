import { BaseEntity } from './../../shared';

export const enum TicketStatus {
    'OPEN',
    'INPROGRESS',
    'CLOSE'
}

export class TicketStory implements BaseEntity {
    constructor(
        public id?: number,
        public date?: any,
        public status?: TicketStatus,
        public ticketId?: number,
        public adminId?: number,
    ) {
    }
}
