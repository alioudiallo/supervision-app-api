import { Component, OnInit, OnDestroy } from '@angular/core';
import { ActivatedRoute } from '@angular/router';
import { Response } from '@angular/http';

import { Observable } from 'rxjs/Rx';
import { NgbActiveModal } from '@ng-bootstrap/ng-bootstrap';
import { JhiEventManager, JhiAlertService } from 'ng-jhipster';

import { TicketStory } from './ticket-story.model';
import { TicketStoryPopupService } from './ticket-story-popup.service';
import { TicketStoryService } from './ticket-story.service';
import { Ticket, TicketService } from '../ticket';
import { User, UserService } from '../../shared';
import { ResponseWrapper } from '../../shared';

@Component({
    selector: 'custom-ticket-story-dialog',
    templateUrl: './ticket-story-dialog.component.html'
})
export class TicketStoryDialogComponent implements OnInit {

    ticketStory: TicketStory;
    isSaving: boolean;

    tickets: Ticket[];

    users: User[];

    constructor(
        public activeModal: NgbActiveModal,
        private jhiAlertService: JhiAlertService,
        private ticketStoryService: TicketStoryService,
        private ticketService: TicketService,
        private userService: UserService,
        private eventManager: JhiEventManager
    ) {
    }

    ngOnInit() {
        this.isSaving = false;
        this.ticketService.query()
            .subscribe((res: ResponseWrapper) => { this.tickets = res.json; }, (res: ResponseWrapper) => this.onError(res.json));
        this.userService.query()
            .subscribe((res: ResponseWrapper) => { this.users = res.json; }, (res: ResponseWrapper) => this.onError(res.json));
    }

    clear() {
        this.activeModal.dismiss('cancel');
    }

    save() {
        this.isSaving = true;
        if (this.ticketStory.id !== undefined) {
            this.subscribeToSaveResponse(
                this.ticketStoryService.update(this.ticketStory));
        } else {
            this.subscribeToSaveResponse(
                this.ticketStoryService.create(this.ticketStory));
        }
    }

    private subscribeToSaveResponse(result: Observable<TicketStory>) {
        result.subscribe((res: TicketStory) =>
            this.onSaveSuccess(res), (res: Response) => this.onSaveError());
    }

    private onSaveSuccess(result: TicketStory) {
        this.eventManager.broadcast({ name: 'ticketStoryListModification', content: 'OK'});
        this.isSaving = false;
        this.activeModal.dismiss(result);
    }

    private onSaveError() {
        this.isSaving = false;
    }

    private onError(error: any) {
        this.jhiAlertService.error(error.message, null, null);
    }

    trackTicketById(index: number, item: Ticket) {
        return item.id;
    }

    trackUserById(index: number, item: User) {
        return item.id;
    }
}

@Component({
    selector: 'custom-ticket-story-popup',
    template: ''
})
export class TicketStoryPopupComponent implements OnInit, OnDestroy {

    routeSub: any;

    constructor(
        private route: ActivatedRoute,
        private ticketStoryPopupService: TicketStoryPopupService
    ) {}

    ngOnInit() {
        this.routeSub = this.route.params.subscribe((params) => {
            if ( params['id'] ) {
                this.ticketStoryPopupService
                    .open(TicketStoryDialogComponent as Component, params['id']);
            } else {
                this.ticketStoryPopupService
                    .open(TicketStoryDialogComponent as Component);
            }
        });
    }

    ngOnDestroy() {
        this.routeSub.unsubscribe();
    }
}
