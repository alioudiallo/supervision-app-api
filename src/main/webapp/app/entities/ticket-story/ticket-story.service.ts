import { Injectable } from '@angular/core';
import { Http, Response } from '@angular/http';
import { Observable } from 'rxjs/Rx';
import { SERVER_API_URL } from '../../app.constants';

import { JhiDateUtils } from 'ng-jhipster';

import { TicketStory } from './ticket-story.model';
import { ResponseWrapper, createRequestOption } from '../../shared';

@Injectable()
export class TicketStoryService {

    private resourceUrl = SERVER_API_URL + 'api/ticket-stories';

    constructor(private http: Http, private dateUtils: JhiDateUtils) { }

    create(ticketStory: TicketStory): Observable<TicketStory> {
        const copy = this.convert(ticketStory);
        return this.http.post(this.resourceUrl, copy).map((res: Response) => {
            const jsonResponse = res.json();
            return this.convertItemFromServer(jsonResponse);
        });
    }

    update(ticketStory: TicketStory): Observable<TicketStory> {
        const copy = this.convert(ticketStory);
        return this.http.put(this.resourceUrl, copy).map((res: Response) => {
            const jsonResponse = res.json();
            return this.convertItemFromServer(jsonResponse);
        });
    }

    find(id: number): Observable<TicketStory> {
        return this.http.get(`${this.resourceUrl}/${id}`).map((res: Response) => {
            const jsonResponse = res.json();
            return this.convertItemFromServer(jsonResponse);
        });
    }

    query(req?: any): Observable<ResponseWrapper> {
        const options = createRequestOption(req);
        return this.http.get(this.resourceUrl, options)
            .map((res: Response) => this.convertResponse(res));
    }

    delete(id: number): Observable<Response> {
        return this.http.delete(`${this.resourceUrl}/${id}`);
    }

    private convertResponse(res: Response): ResponseWrapper {
        const jsonResponse = res.json();
        const result = [];
        for (let i = 0; i < jsonResponse.length; i++) {
            result.push(this.convertItemFromServer(jsonResponse[i]));
        }
        return new ResponseWrapper(res.headers, result, res.status);
    }

    /**
     * Convert a returned JSON object to TicketStory.
     */
    private convertItemFromServer(json: any): TicketStory {
        const entity: TicketStory = Object.assign(new TicketStory(), json);
        entity.date = this.dateUtils
            .convertDateTimeFromServer(json.date);
        return entity;
    }

    /**
     * Convert a TicketStory to a JSON which can be sent to the server.
     */
    private convert(ticketStory: TicketStory): TicketStory {
        const copy: TicketStory = Object.assign({}, ticketStory);

        copy.date = this.dateUtils.toDate(ticketStory.date);
        return copy;
    }
}
