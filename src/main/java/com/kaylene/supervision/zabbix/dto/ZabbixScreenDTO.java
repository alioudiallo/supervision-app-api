package com.kaylene.supervision.zabbix.dto;

import com.fasterxml.jackson.annotation.JsonProperty;
import com.kaylene.supervision.zabbix.model.ZabbixScreen;
import com.kaylene.supervision.zabbix.model.ZabbixScreenItem;
import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.NoArgsConstructor;

import java.util.List;

/**
 * @author Mamadou Lamine NIANG
 **/
@Data
@EqualsAndHashCode(callSuper = true)
@NoArgsConstructor
public class ZabbixScreenDTO extends ZabbixScreen {

    @JsonProperty("screenitems")
    private List<ZabbixScreenItem> items;
}
