package com.kaylene.supervision.zabbix.dto;

import com.fasterxml.jackson.annotation.JsonProperty;
import lombok.Data;
import lombok.NoArgsConstructor;

/**
 * @author Mamadou Lamine NIANG
 **/
@Data
@NoArgsConstructor
public class ZabbixImageDTO {

    @JsonProperty("image")
    private String base64Img;
}
