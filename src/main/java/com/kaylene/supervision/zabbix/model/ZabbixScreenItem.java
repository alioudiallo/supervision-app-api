package com.kaylene.supervision.zabbix.model;

import com.fasterxml.jackson.annotation.JsonProperty;
import lombok.Data;
import lombok.NoArgsConstructor;

/**
 * @author Mamadou Lamine NIANG
 **/
@Data
@NoArgsConstructor
public class ZabbixScreenItem {

    @JsonProperty("screenitemid")
    private String id;
    @JsonProperty("resourcetype")
    private int resourceType;
    @JsonProperty("screenid")
    private String screenId;
    private String application;
    private int colspan;
    private int dynamic;
    private int elements;
    private int halign;
    private int height;
    @JsonProperty("max_columns")
    private int maxColumns;
    @JsonProperty("resourceid")
    private String resourceId;
    private int rowspan;
    @JsonProperty("sort_triggers")
    private int sortTriggers;
    private int style;
    private String url;
    private int valign;
    private int width;
    private int x;
    private int y;
    private String period = "1h";
}
