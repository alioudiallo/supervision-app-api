package com.kaylene.supervision.zabbix;

import com.fasterxml.jackson.annotation.JsonInclude;
import com.fasterxml.jackson.annotation.JsonProperty;
import com.kaylene.supervision.zabbix.request.CommonGetParams;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.util.List;

/**
 * @author Mamadou Lamine NIANG
 **/
@Data
@NoArgsConstructor
@JsonInclude(JsonInclude.Include.NON_NULL)
public class GraphGetParams extends CommonGetParams {

    @JsonProperty("screenids")
    private List<String> ids;
    @JsonProperty("userids")
    private List<String> userIds;
    private String selectScreenItems = "extend";
}
