package com.kaylene.supervision.domain;

import com.fasterxml.jackson.annotation.JsonIgnore;
import com.kaylene.supervision.domain.enumeration.ContractStatus;
import org.hibernate.annotations.Cache;
import org.hibernate.annotations.CacheConcurrencyStrategy;

import javax.persistence.*;
import java.io.Serializable;
import java.time.Instant;
import java.util.HashSet;
import java.util.Objects;
import java.util.Set;

/**
 * A Contract.
 */
@Entity
@Table(name = "contract")
@Cache(usage = CacheConcurrencyStrategy.NONSTRICT_READ_WRITE)
public class Contract implements Serializable {

	private static final long serialVersionUID = 1L;

	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	private Long id;

	@Enumerated(EnumType.STRING)
	@Column(name = "current_status")
	private ContractStatus currentStatus;

	@Column(name = "create_date")
	private Instant createDate;

	@Column(name = "begin_date")
	private Instant beginDate;

	public static long getSerialversionuid() {
		return serialVersionUID;
	}

	@Column(name = "end_date")
	private Instant endDate;

	@ManyToOne
	private Company company;

	@OneToOne
	@JoinColumn(unique = true)
	private Offer offer;

	@OneToMany(mappedBy = "contract")
	@JsonIgnore
	@Cache(usage = CacheConcurrencyStrategy.NONSTRICT_READ_WRITE)
	private Set<HostContract> hosts = new HashSet<>();

	public Long getId() {
		return id;
	}

	public void setId(Long id) {
		this.id = id;
	}

	public ContractStatus getCurrentStatus() {
		return currentStatus;
	}

	public Contract currentStatus(ContractStatus currentStatus) {
		this.currentStatus = currentStatus;
		return this;
	}

	public void setCurrentStatus(ContractStatus currentStatus) {
		this.currentStatus = currentStatus;
	}

	public Instant getCreateDate() {
		return createDate;
	}

	public Contract createDate(Instant createDate) {
		this.createDate = createDate;
		return this;
	}

	public void setCreateDate(Instant createDate) {
		this.createDate = createDate;
	}

	public Instant getBeginDate() {
		return beginDate;
	}

	public Contract beginDate(Instant beginDate) {
		this.beginDate = beginDate;
		return this;
	}

	public void setBeginDate(Instant beginDate) {
		this.beginDate = beginDate;
	}

	public Instant getEndDate() {
		return endDate;
	}

	public Contract endDate(Instant endDate) {
		this.endDate = endDate;
		return this;
	}

	public void setEndDate(Instant endDate) {
		this.endDate = endDate;
	}

	public Company getCompany() {
		return company;
	}

	public Contract company(Company company) {
		this.company = company;
		return this;
	}

	public void setCompany(Company company) {
		this.company = company;
	}

	public Offer getOffer() {
		return offer;
	}

	public Contract offer(Offer offer) {
		this.offer = offer;
		return this;
	}

	public void setOffer(Offer offer) {
		this.offer = offer;
	}

	public Set<HostContract> getHosts() {
		return hosts;
	}

	public Contract hosts(Set<HostContract> hostContracts) {
		this.hosts = hostContracts;
		return this;
	}

	public Contract addHosts(HostContract hostContract) {
		this.hosts.add(hostContract);
		hostContract.setContract(this);
		return this;
	}

	public Contract removeHosts(HostContract hostContract) {
		this.hosts.remove(hostContract);
		hostContract.setContract(null);
		return this;
	}

	public void setHosts(Set<HostContract> hostContracts) {
		this.hosts = hostContracts;
	}
	
	@Override
	public boolean equals(Object o) {
		if (this == o) {
			return true;
		}
		if (o == null || getClass() != o.getClass()) {
			return false;
		}
		Contract contract = (Contract) o;
		if (contract.getId() == null || getId() == null) {
			return false;
		}
		return Objects.equals(getId(), contract.getId());
	}

	@Override
	public int hashCode() {
		return Objects.hashCode(getId());
	}

	@Override
	public String toString() {
		return "Contract{" + "id=" + getId() + ", currentStatus='" + getCurrentStatus() + "'" + ", createDate='"
				+ getCreateDate() + "'" + ", beginDate='" + getBeginDate() + "'" + ", endDate='" + getEndDate() + "'"
				+ ", company='" + getCompany().toString() + "'" + ", offer='" + getOffer().toString() + "'" + "}";
	}
}
