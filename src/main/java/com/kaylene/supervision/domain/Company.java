package com.kaylene.supervision.domain;

import com.fasterxml.jackson.annotation.JsonIgnore;
import org.hibernate.annotations.Cache;
import org.hibernate.annotations.CacheConcurrencyStrategy;

import javax.persistence.*;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Size;
import java.io.Serializable;
import java.util.HashSet;
import java.util.Objects;
import java.util.Set;
import java.util.function.Function;
import java.util.stream.Collectors;

import static com.kaylene.supervision.config.Constants.PHONE_NUMBER_MAX_LENGTH;
import static com.kaylene.supervision.config.Constants.PHONE_NUMBER_MIN_LENGTH;

/**
 * A Company.
 */
@Entity
@Table(name = "company")
@Cache(usage = CacheConcurrencyStrategy.NONSTRICT_READ_WRITE)
public class Company implements Serializable {

	private static final long serialVersionUID = 1L;

	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	private Long id;

	@Column(name = "zabbix_login")
	private String zabbixLogin;

	@Column(name = "zabbix_password")
	private String zabbixPassword;

	@NotNull
	@Column(name = "name", nullable = false)
	private String name;

	@Column(name = "website")
	private String website;

	@Column(name = "address")
	private String address;

	@Size(min = PHONE_NUMBER_MIN_LENGTH, max = PHONE_NUMBER_MAX_LENGTH)
	@Column(name = "phone", length = PHONE_NUMBER_MAX_LENGTH)
	private String phone;

	@Column(name = "main_contact")
	private String mainContact;

	@Column(name = "main_email")
	private String mainEmail;

	@OneToMany(mappedBy = "company", cascade = CascadeType.ALL, fetch = FetchType.EAGER)
	@JsonIgnore
	@Cache(usage = CacheConcurrencyStrategy.NONSTRICT_READ_WRITE)
	private Set<ContactPerson> contactPersons = new HashSet<>();

	@OneToMany(mappedBy = "company", cascade = CascadeType.ALL, fetch = FetchType.EAGER)
	@JsonIgnore
	@Cache(usage = CacheConcurrencyStrategy.NONSTRICT_READ_WRITE)
	private Set<Contract> contracts = new HashSet<>();

	@OneToMany(mappedBy = "company", cascade = CascadeType.ALL, fetch = FetchType.EAGER)
	@JsonIgnore
	@Cache(usage = CacheConcurrencyStrategy.NONSTRICT_READ_WRITE)
	private Set<Site> sites = new HashSet<>();

	@OneToMany(mappedBy = "company", cascade = CascadeType.ALL, fetch = FetchType.EAGER)
	@JsonIgnore
	// @Cache(usage = CacheConcurrencyStrategy.NONSTRICT_READ_WRITE)
	private Set<User> users = new HashSet<>();

	// jhipster-needle-entity-add-field - JHipster will add fields here, do not
	// remove
	public Long getId() {
		return id;
	}

	public void setId(Long id) {
		this.id = id;
	}

	// public Company zabbixLogin(String login) {
	// this.zabbixLogin = login;
	// return this;
	// }

	public String getZabbixLogin() {
		return zabbixLogin;
	}

	public void setZabbixLogin(String zabbixLogin) {
		this.zabbixLogin = zabbixLogin;
	}

	public String getZabbixPassword() {
		return zabbixPassword;
	}

	public void setZabbixPassword(String zabbixPassword) {
		this.zabbixPassword = zabbixPassword;
	}

	public String getName() {
		return name;
	}

	public Company name(String name) {
		this.name = name;
		return this;
	}

	public void setName(String name) {
		this.name = name;
	}

	public String getWebsite() {
		return website;
	}

	public Company website(String website) {
		this.website = website;
		return this;
	}

	public void setWebsite(String website) {
		this.website = website;
	}

	public String getAddress() {
		return address;
	}

	public Company address(String address) {
		this.address = address;
		return this;
	}

	public void setAddress(String address) {
		this.address = address;
	}

	public String getPhone() {
		return phone;
	}

	public Company phone(String phone) {
		this.phone = phone;
		return this;
	}

	public void setPhone(String phone) {
		this.phone = phone;
	}

	public Set<ContactPerson> getContactPersons() {
		return contactPersons;
	}

	public Company contactPersons(Set<ContactPerson> contactPeople) {
		this.contactPersons = contactPeople;
		return this;
	}

	public Company addContactPersons(ContactPerson contactPerson) {
		this.contactPersons.add(contactPerson);
		contactPerson.setCompany(this);
		return this;
	}

	public Company removeContactPersons(ContactPerson contactPerson) {
		this.contactPersons.remove(contactPerson);
		contactPerson.setCompany(null);
		return this;
	}

	public void setContactPersons(Set<ContactPerson> contactPeople) {
		this.contactPersons = contactPeople;
	}

	public Set<Contract> getContracts() {
		return contracts;
	}

	public Company contracts(Set<Contract> contracts) {
		this.contracts = contracts;
		return this;
	}

	public Company addContracts(Contract contract) {
		this.contracts.add(contract);
		contract.setCompany(this);
		return this;
	}

	public Company removeContracts(Contract contract) {
		this.contracts.remove(contract);
		contract.setCompany(null);
		return this;
	}

	public void setContracts(Set<Contract> contracts) {
		this.contracts = contracts;
	}

	public Set<Site> getSites() {
		return sites;
	}

	public void setSites(Set<Site> sites) {
		Function<Site, Site> mapper = x -> x.company(this);
		this.sites = sites.stream().map(mapper).collect(Collectors.toSet());
	}

	public Company sites(Set<Site> sites) {
		this.sites = sites;
		return this;
	}

	public Company addSites(Site site) {
		this.sites.add(site);
		site.setCompany(this);
		return this;
	}

	public Company removeSites(Site site) {
		this.sites.remove(site);
		site.setCompany(null);
		return this;
	}

	public String getMainEmail() {
		return mainEmail;
	}

	public void setMainEmail(String mainEmail) {
		this.mainEmail = mainEmail;
	}

	public Set<User> getUsers() {
		return users;
	}

	public void setUsers(Set<User> users) {
		Function<User, User> mapper = x -> x.company(this);
		this.users = users.stream().map(mapper).collect(Collectors.toSet());
		// this.users = users;
	}

	public String getMainContact() {
		return mainContact;
	}

	public void setMainContact(String mainContact) {
		this.mainContact = mainContact;
	}

	// jhipster-needle-entity-add-getters-setters - JHipster will add getters and
	// setters here, do not remove

	@Override
	public boolean equals(Object o) {
		if (this == o) {
			return true;
		}
		if (o == null || getClass() != o.getClass()) {
			return false;
		}
		Company company = (Company) o;
		if (company.getId() == null || getId() == null) {
			return false;
		}
		return Objects.equals(getId(), company.getId());
	}

	@Override
	public int hashCode() {
		return Objects.hashCode(getId());
	}

	@Override
	public String toString() {
		return "Company{" + "id=" + getId() + ", zabbixLogin='" + getZabbixLogin() + "'" + "'" + ", name='" + getName()
				+ "'" + ", website='" + getWebsite() + "'" + ", address='" + getAddress() + "'" + ", phone='"
				+ getPhone() + "'" + ", sites='" + getSites() + "'" + "}";
	}

}
