package com.kaylene.supervision.domain;

import org.hibernate.annotations.Cache;
import org.hibernate.annotations.CacheConcurrencyStrategy;

import javax.persistence.*;
import java.io.Serializable;
import java.util.Objects;

/**
 * A Site.
 */
@Entity
@Table(name = "site")
@Cache(usage = CacheConcurrencyStrategy.NONSTRICT_READ_WRITE)
public class Site implements Serializable {

	private static final long serialVersionUID = 1L;

	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	private Long id;

	@Column(name = "zabbix_proxy_id")
	private String zabbixProxyId;

	@Column(name = "custom_label")
	private String label;

	@Column(name = "address")
	private String address;

	@Column(name = "latitude")
	private String latitude;

	@Column(name = "longitude")
	private String longitude;

	@ManyToOne
	private Company company;

	public Long getId() {
		return id;
	}

	public void setId(Long id) {
		this.id = id;
	}

	public String getZabbixProxyId() {
		return zabbixProxyId;
	}

	public Site zabbixProxyId(String zabbixProxyId) {
		this.zabbixProxyId = zabbixProxyId;
		return this;
	}

	public void setZabbixProxyId(String zabbixProxyId) {
		this.zabbixProxyId = zabbixProxyId;
	}

	public String getLabel() {
		return label;
	}

	public Site label(String label) {
		this.label = label;
		return this;
	}

	public void setLabel(String label) {
		this.label = label;
	}

	public String getAddress() {
		return address;
	}

	public Site address(String address) {
		this.address = address;
		return this;
	}

	public void setAddress(String address) {
		this.address = address;
	}

	public String getLatitude() {
		return latitude;
	}

	public Site latitude(String latitude) {
		this.latitude = latitude;
		return this;
	}

	public void setLatitude(String latitude) {
		this.latitude = latitude;
	}

	public String getLongitude() {
		return longitude;
	}

	public Site longitude(String longitude) {
		this.longitude = longitude;
		return this;
	}

	public void setLongitude(String longitude) {
		this.longitude = longitude;
	}

	public Company getCompany() {
		return company;
	}

	public Site company(Company company) {
		this.company = company;
		return this;
	}

	public void setCompany(Company company) {
		this.company = company;
	}

	@Override
	public boolean equals(Object o) {
		if (this == o) {
			return true;
		}
		if (o == null || getClass() != o.getClass()) {
			return false;
		}
		Site site = (Site) o;
		if (site.getId() == null || getId() == null) {
			return false;
		}
		return Objects.equals(getId(), site.getId());
	}

	@Override
	public int hashCode() {
		return Objects.hashCode(getId());
	}

	@Override
	public String toString() {
		return "Site{" + "id=" + getId() + ", zabbixProxyId='" + getZabbixProxyId() + "'" + ", label='" + getLabel()
				+ "'" + ", address='" + getAddress() + "'" + ", latitude='" + getLatitude() + "'" + ", longitude='"
				+ getLongitude() + "'" + "}";
	}
}
