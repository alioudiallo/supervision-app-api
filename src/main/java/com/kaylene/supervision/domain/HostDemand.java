package com.kaylene.supervision.domain;

import org.hibernate.annotations.Cache;
import org.hibernate.annotations.CacheConcurrencyStrategy;

import javax.persistence.*;
import java.io.Serializable;
import java.time.Instant;
import java.util.Objects;

/**
 * A HostDemand.
 */
@Entity
@Table(name = "host_demand")
@Cache(usage = CacheConcurrencyStrategy.NONSTRICT_READ_WRITE)
public class HostDemand implements Serializable {

	private static final long serialVersionUID = 1L;

	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	private Long id;

	@Column(name = "zabbix_id")
	private String zabbixId;

	@Column(name = "initial_label")
	private String initialLabel;

	@Column(name = "label")
	private String label;

	@Column(name = "initial_ip_address")
	private String initialIpAddress;

	@Column(name = "ip_address")
	private String ipAddress;

	@Column(name = "initial_site_id")
	private String initialSiteId;

	@Column(name = "site_id")
	private Long siteId;

	@Column(name = "custom_type_label")
	private String typeLabel;

	@Column(name = "code_snmp")
	private String codeSnmp;

	@Column(name = "date_integration")
	private Instant dateIntegration;

	@Column(name = "is_done_in_zabbix")
	private boolean isDoneInZabbix = false;

	@ManyToOne
	private Demand demand;

	private String marque;

	private String modele;

	private String role;

	public Long getId() {
		return id;
	}

	public void setId(Long id) {
		this.id = id;
	}

	public String getZabbixId() {
		return zabbixId;
	}

	public HostDemand zabbixId(String zabbixId) {
		this.zabbixId = zabbixId;
		return this;
	}

	public void setZabbixId(String zabbixId) {
		this.zabbixId = zabbixId;
	}

	public String getLabel() {
		return label;
	}

	public HostDemand label(String label) {
		this.label = label;
		return this;
	}

	public void setLabel(String label) {
		this.label = label;
	}

	public String getIpAddress() {
		return ipAddress;
	}

	public HostDemand ipAddress(String ipAddress) {
		this.ipAddress = ipAddress;
		return this;
	}

	public void setIpAddress(String ipAddress) {
		this.ipAddress = ipAddress;
	}

	public Long getSiteId() {
		return siteId;
	}

	public HostDemand siteId(Long siteId) {
		this.siteId = siteId;
		return this;
	}

	public void setSiteId(Long siteId) {
		this.siteId = siteId;
	}

	public String getType() {
		return type;
	}

	public HostDemand type(String type) {
		this.type = type;
		return this;
	}

	public void setType(String type) {
		this.type = type;
	}

	@Column(name = "host_type")
	private String type;

	public String getMarque() {
		return marque;
	}

	public void setMarque(String marque) {
		this.marque = marque;
	}

	public String getModele() {
		return modele;
	}

	public void setModele(String modele) {
		this.modele = modele;
	}

	public String getRole() {
		return role;
	}

	public void setRole(String role) {
		this.role = role;
	}

	public String getCodeSnmp() {
		return codeSnmp;
	}

	public HostDemand codeSnmp(String codeSnmp) {
		this.codeSnmp = codeSnmp;
		return this;
	}

	public void setCodeSnmp(String codeSnmp) {
		this.codeSnmp = codeSnmp;
	}

	public Instant getDateIntegration() {
		return dateIntegration;
	}

	public HostDemand dateIntegration(Instant dateIntegration) {
		this.dateIntegration = dateIntegration;
		return this;
	}

	public void setDateIntegration(Instant dateIntegration) {
		this.dateIntegration = dateIntegration;
	}

	public Demand getDemand() {
		return demand;
	}

	public HostDemand demand(Demand demand) {
		this.demand = demand;
		return this;
	}

	public void setDemand(Demand demand) {
		this.demand = demand;
	}

	public String getInitialLabel() {
		return initialLabel;
	}

	public void setInitialLabel(String initialLabel) {
		this.initialLabel = initialLabel;
	}

	public String getInitialIpAddress() {
		return initialIpAddress;
	}

	public void setInitialIpAddress(String initialIpAddress) {
		this.initialIpAddress = initialIpAddress;
	}

	public String getInitialSiteId() {
		return initialSiteId;
	}

	public void setInitialSiteId(String initialSiteId) {
		this.initialSiteId = initialSiteId;
	}

	public boolean getIsDoneInZabbix() {
		return isDoneInZabbix;
	}

	public void setIsDoneInZabbix(boolean isDoneInZabbix) {
		this.isDoneInZabbix = isDoneInZabbix;
	}

	public String getTypeLabel() {
		return typeLabel;
	}

	public void setTypeLabel(String typeLabel) {
		this.typeLabel = typeLabel;
	}

	@Override
	public boolean equals(Object o) {
		if (this == o) {
			return true;
		}
		if (o == null || getClass() != o.getClass()) {
			return false;
		}
		HostDemand hostDemand = (HostDemand) o;
		if (hostDemand.getId() == null || getId() == null) {
			return false;
		}
		return Objects.equals(getId(), hostDemand.getId());
	}

	@Override
	public int hashCode() {
		return Objects.hashCode(getId());
	}

	@Override
	public String toString() {
		return "HostDemand{" + "id=" + getId() + ", zabbixId='" + getZabbixId() + "'" + ", label='" + getLabel() + "'"
				+ ", ipAddress='" + getIpAddress() + "'" + ", siteId='" + getSiteId() + "'" + ", type='" + getType()
				+ "'" + ", codeSnmp='" + getCodeSnmp() + "'" + ", dateIntegration='" + getDateIntegration() + "'"
				+ ", demand='" + getDemand().getId() + "'" + ", modele='" + getModele() + "'" + ", marque='"
				+ getMarque() + "'" + ", role='" + getRole() + "'" + "}";
	}
}
