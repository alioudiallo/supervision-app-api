package com.kaylene.supervision.domain;

import com.kaylene.supervision.domain.enumeration.DemandStatus;
import org.hibernate.annotations.Cache;
import org.hibernate.annotations.CacheConcurrencyStrategy;
import org.springframework.data.annotation.CreatedDate;
import org.springframework.data.jpa.domain.support.AuditingEntityListener;

import javax.persistence.*;
import java.io.Serializable;
import java.time.Instant;
import java.util.Objects;

/**
 * A DemandStory.
 */
@Entity
@EntityListeners(AuditingEntityListener.class)
@Table(name = "demand_story")
@Cache(usage = CacheConcurrencyStrategy.NONSTRICT_READ_WRITE)
public class DemandStory implements Serializable {

	private static final long serialVersionUID = 1L;

	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	private Long id;

	@Column(name = "created_date")
	@CreatedDate
	private Instant createdDate;

	@Enumerated(EnumType.STRING)
	@Column(name = "status")
	private DemandStatus status;

	@Column(name = "user_id")
	private Long userId;

	@Column(name = "demand_id")
	private Long demandId;

	public Long getId() {
		return id;
	}

	public void setId(Long id) {
		this.id = id;
	}

	public Instant getCreatedDate() {
		return createdDate;
	}

	public DemandStory createdDate(Instant createdDate) {
		this.createdDate = createdDate;
		return this;
	}

	public void setCreatedDate(Instant createdDate) {
		this.createdDate = createdDate;
	}

	public DemandStatus getStatus() {
		return status;
	}

	public DemandStory status(DemandStatus status) {
		this.status = status;
		return this;
	}

	public void setStatus(DemandStatus status) {
		this.status = status;
	}

	public Long getUserId() {
		return userId;
	}

	public void setUserId(Long userId) {
		this.userId = userId;
	}

	public Long getDemandId() {
		return demandId;
	}

	public DemandStory demand(Long demandId) {
		this.demandId = demandId;
		return this;
	}

	public void setDemandId(Long demandId) {
		this.demandId = demandId;
	}
	// jhipster-needle-entity-add-getters-setters - JHipster will add getters and
	// setters here, do not remove

	@Override
	public boolean equals(Object o) {
		if (this == o) {
			return true;
		}
		if (o == null || getClass() != o.getClass()) {
			return false;
		}
		DemandStory demandStory = (DemandStory) o;
		if (demandStory.getId() == null || getId() == null) {
			return false;
		}
		return Objects.equals(getId(), demandStory.getId());
	}

	@Override
	public int hashCode() {
		return Objects.hashCode(getId());
	}

	@Override
	public String toString() {
		return "DemandStory{" + "id=" + getId() + ", createdDate='" + getCreatedDate() + "'" + ", status='"
				+ getStatus() + "'" + "}";
	}
}
