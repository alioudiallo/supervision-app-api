package com.kaylene.supervision.domain;

import org.hibernate.annotations.Cache;
import org.hibernate.annotations.CacheConcurrencyStrategy;

import javax.persistence.*;
import java.io.Serializable;
import java.util.Objects;

/**
 * A HostType.
 */
@Entity
@Table(name = "host_type")
@Cache(usage = CacheConcurrencyStrategy.NONSTRICT_READ_WRITE)
public class HostType implements Serializable {

	private static final long serialVersionUID = 1L;

	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	private Long id;

	@Column(name = "name")
	private String name;

	@Column(name = "custom_label", unique = true)
	private String label;

	@Lob
	@Column(name = "img")
	private byte[] img;

	@Column(name = "img_content_type")
	private String imgContentType;

	public Long getId() {
		return id;
	}

	public void setId(Long id) {
		this.id = id;
	}

	public String getName() {
		return name;
	}

	public HostType name(String name) {
		this.name = name;
		return this;
	}

	public void setName(String name) {
		this.name = name;
	}

	public String getLabel() {
		return label;
	}

	public HostType label(String label) {
		this.label = label;
		return this;
	}

	public void setLabel(String label) {
		this.label = label;
	}

	public byte[] getImg() {
		return img;
	}

	public HostType img(byte[] img) {
		this.img = img;
		return this;
	}

	public void setImg(byte[] img) {
		this.img = img;
	}

	public String getImgContentType() {
		return imgContentType;
	}

	public HostType imgContentType(String imgContentType) {
		this.imgContentType = imgContentType;
		return this;
	}

	public void setImgContentType(String imgContentType) {
		this.imgContentType = imgContentType;
	}

	@Override
	public boolean equals(Object o) {
		if (this == o) {
			return true;
		}
		if (o == null || getClass() != o.getClass()) {
			return false;
		}
		HostType hostType = (HostType) o;
		if (hostType.getId() == null || getId() == null) {
			return false;
		}

		return Objects.equals(getId(), hostType.getId());
	}

	@Override
	public int hashCode() {
		return Objects.hashCode(getId());
	}

	@Override
	public String toString() {
		return "HostType{" + "id=" + getId() + ", name='" + getName() + "'" + ", label='" + getLabel() + "'" + ", img='"
				+ getImg() + "'" + ", imgContentType='" + imgContentType + "'" + "}";
	}
}
