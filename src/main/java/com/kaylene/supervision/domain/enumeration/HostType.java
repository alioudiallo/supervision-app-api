package com.kaylene.supervision.domain.enumeration;

/**
 * The HostType enumeration.
 */
public enum HostType {
	ROUTER,

	SWITCH,

	FIREWALL,

	SERVER,

	WEBSERVER,

	LOAD_BALANCER,

	PABX,

	RADIO_LINK,

	WIFI_AP,

	CAMERA,

	HYPERVISOR,

	VIRTUAL_MACHINE,

	DATABASE,

	INVERTER
}
