package com.kaylene.supervision.domain.enumeration;

/**
 * The TicketType enumeration.
 */
public enum TicketType {
	OPEN,

	CLOSE,

	INPROGRESS
}
