package com.kaylene.supervision.domain.enumeration;

/**
 * The ContractStatus enumeration.
 */
public enum ContractStatus {
    ONGOING, FINISH
}
