package com.kaylene.supervision.constants;

public final class Messages {
	
	public final static String SAME_NAMES_ERROR_MSG = "Deux équipements ne peuvent avoir le même nom sur le même site";
	public final static String SAME_IP_ADDRESSES_ERROR_MSG = "Deux équipements ne peuvent avoir la même adresse IP sur le même site";
	public final static String INTEGRATION_DATE_ERROR_MSG = "La date d'intégration souhaitée ne peut être avant aujourd'hui";
	public final static String NOT_DONE_DEMAND_ON_HOST_ERROR_MSG= "L\'équipement concerné fait déja l'objet d'une demande non encore réalisée";
	public final static String INVALID_IP_ADDRESS_ERROR_MSG = "Les adresses IP doivent être bien formées"; 
	
	public final static String ZABBIX_LOGIN_ALREADY_USED_ERROR_MSG = "Le login ZABBIX est déja utilisé";
	public final static String NO_USER_FOUND_WITH_GIVEN_LOGIN_ERROR_MSG = "Aucun utilisateur trouvé";
	public final static String NO_CONTRACT_FOUND_ERROR_MSG = "Cher client, il semblerait que votre contrat soit arrivé à terme."
			+ " Cette demande ne peut donc malheureusement pas être soumise. Nous vous prions de bien vouloir contacter nos administrateurs.";
	public final static String ONGOING_CONTRACT_EXISTS_ERROR_MSG = "Un contrat est déja en cours pour cette entreprise";
	public final static String COMPANY_NAME_ALREADY_USED_ERROR_MSG = "Ce nom d'entreprise est déja utilisé.";
	public final static String TOO_MANY_HOSTS_ON_DEMAND_ERROR_MSG = "Le nombre d'équipements dans cette demande ne peut être ajouté au nombre d'équipements déja supervisés. "
			+ "Il vous est néanmoins possible de souscrire à une autre offre.";
	public final static String ALREADY_DONE_DEMAND_ERROR_MSG = "Cette demande est déja realisée.";
	public final static String PHONE_NUMBER_ALREADY_USED_ERROR_MSG = "Ce numéro de téléphone est déja utilisé";
	public final static String EMAIL_ALREADY_USED_ERROR_MSG = "Cet email est déja utilisé.";
	public final static String ALREADY_USED_LOGIN_ERROR_MSG = "Ce login est déja utilisé.";
	public final static String UNCLOSED_TICKET_EXISTS_ERROR_MSG = "Un ticket non cloturé existe sur cet équipement.";
	public final static String INVALID_EMAIL_ERROR_MSG = "L\'email saisi est invalide";
	public final static String EXISTING_HOST_TYPE_NAME_ERROR_MSG = "Ce nom de type d\'équipements existe déja.";
	public final static String EXISTING_HOST_TYPE_LABEL_ERROR_MSG = "Ce label de type d\'équipements existe déja.";
	public final static String NO_HOST_TYPE_FOUND_ERROR_MSG = "Aucun type trouvé";
	
	public final static String EXISTING_INCIDENT_TYPE_KEY_ERROR_MSG = "Ce type d'incident existe déja.";
	public final static String EXISTING_INCIDENT_TYPE_LABEL_ERROR_MSG = "Ce label de type d'incident existe déja.";
	
}
