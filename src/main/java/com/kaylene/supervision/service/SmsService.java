package com.kaylene.supervision.service;

import com.kaylene.supervision.domain.Ticket;
import com.kaylene.supervision.domain.User;
import com.kaylene.supervision.service.others.orangeSmsAPI.OrangeSmsApiService;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.scheduling.annotation.Async;
import org.springframework.stereotype.Service;

import java.text.Normalizer;

@Service
public class SmsService {

	private final Logger log = LoggerFactory.getLogger(MailService.class);

	@Autowired
	OrangeSmsApiService orangeSmsApiService;

	@Autowired
	MailService mailService;

	@Autowired
	IncidentTypeService incidentTypeService;

	@Async
	public void sendTicketCreationSms(Ticket ticket, User user) {
		String phoneNumber = user.getPhoneNumber();
		if (phoneNumber == null) {
			log.error("Aucun numéro de téléphone trouvé pour l'utilisateur: '{}'", user.getLogin());
		} else {
			String message = createTicketSmsMessage(ticket);
			orangeSmsApiService.sendSms(phoneNumber, message);
		}
	}

	private String createTicketSmsMessage(Ticket ticket) {
		Long ticketId = ticket.getId();
		String hostName = ticket.getHostLabel();
		String incidentType = incidentTypeService.getIncidentTypeLabelByKey(ticket.getIncidentType());
		String severite = mailService.getTranslatedSeverity(ticket.getSeverity());
		String message = "Ticket numero: " + ticketId + "\n" + "Equipement: " + hostName + "\n" + "Severite: "
				+ severite + "\n" + "Incident de type: " + incidentType;

		// enlever les accents du message
		message = Normalizer.normalize(message, Normalizer.Form.NFD);
		message = message.replaceAll("[^\\p{ASCII}]", "");

		return message;
	}

}
