package com.kaylene.supervision.service.dto;

import java.io.Serializable;
import java.util.Objects;

/**
 * A DTO for the HostContract entity.
 */
public class HostContractDTO implements Serializable {

	private static final long serialVersionUID = 1L;

	private Long id;

	private String zabbixHostId;

	private Long contractId;

	public Long getId() {
		return id;
	}

	public void setId(Long id) {
		this.id = id;
	}

	public String getZabbixHostId() {
		return zabbixHostId;
	}

	public void setZabbixHostId(String zabbixHostId) {
		this.zabbixHostId = zabbixHostId;
	}

	public Long getContractId() {
		return contractId;
	}

	public void setContractId(Long contractId) {
		this.contractId = contractId;
	}

	@Override
	public boolean equals(Object o) {
		if (this == o) {
			return true;
		}
		if (o == null || getClass() != o.getClass()) {
			return false;
		}

		HostContractDTO hostContractDTO = (HostContractDTO) o;
		if (hostContractDTO.getId() == null || getId() == null) {
			return false;
		}
		return Objects.equals(getId(), hostContractDTO.getId());
	}

	@Override
	public int hashCode() {
		return Objects.hashCode(getId());
	}

	@Override
	public String toString() {
		return "HostContractDTO{" + "id=" + getId() + ", zabbixHostId='" + getZabbixHostId() + "'" + "}";
	}
}
