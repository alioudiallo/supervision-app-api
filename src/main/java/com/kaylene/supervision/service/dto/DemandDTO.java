package com.kaylene.supervision.service.dto;

import com.kaylene.supervision.domain.enumeration.DemandStatus;
import com.kaylene.supervision.domain.enumeration.DemandType;

import javax.persistence.Lob;
import java.io.Serializable;
import java.time.Instant;
import java.util.HashSet;
import java.util.Objects;
import java.util.Set;

/**
 * A DTO for the Demand entity.
 */
public class DemandDTO implements Serializable {

	private static final long serialVersionUID = 1L;

	private Long id;

	private DemandType type;

	private DemandStatus currentStatus;

	@Lob
	private String description;

	private Instant createdDate;

	private Instant lastModifiedDate;

	private Long companyId;

	private Set<HostDemandDTO> hostsToAdds = new HashSet<>();

	private Long contractId;

	private Long userId;

	private String userName;

	private Long adminId;

	private String adminName;

	public Set<HostDemandDTO> getHostsToAdds() {
		return hostsToAdds;
	}

	public void setHostsToAdds(Set<HostDemandDTO> hostsToAdds) {
		this.hostsToAdds = hostsToAdds;
	}

	public Long getId() {
		return id;
	}

	public void setId(Long id) {
		this.id = id;
	}

	public DemandType getType() {
		return type;
	}

	public void setType(DemandType type) {
		this.type = type;
	}

	public DemandStatus getCurrentStatus() {
		return currentStatus;
	}

	public void setCurrentStatus(DemandStatus currentStatus) {
		this.currentStatus = currentStatus;
	}

	public String getDescription() {
		return description;
	}

	public void setDescription(String description) {
		this.description = description;
	}

	public Instant getCreatedDate() {
		return createdDate;
	}

	public void setCreatedDate(Instant createdDate) {
		this.createdDate = createdDate;
	}

	public Instant getLastModifiedDate() {
		return lastModifiedDate;
	}

	public void setLastModifiedDate(Instant lastModifiedDate) {
		this.lastModifiedDate = lastModifiedDate;
	}

	public Long getCompanyId() {
		return companyId;
	}

	public void setCompanyId(Long companyId) {
		this.companyId = companyId;
	}

	public Long getContractId() {
		return contractId;
	}

	public void setContractId(Long contractId) {
		this.contractId = contractId;
	}

	public Long getUserId() {
		return userId;
	}

	public void setUserId(Long userId) {
		this.userId = userId;
	}

	public String getUserName() {
		return userName;
	}

	public void setUserName(String userName) {
		this.userName = userName;
	}

	public Long getAdminId() {
		return adminId;
	}

	public void setAdminId(Long adminId) {
		this.adminId = adminId;
	}

	public String getAdminName() {
		return adminName;
	}

	public void setAdminName(String adminName) {
		this.adminName = adminName;
	}

	@Override
	public boolean equals(Object o) {
		if (this == o) {
			return true;
		}
		if (o == null || getClass() != o.getClass()) {
			return false;
		}

		DemandDTO demandDTO = (DemandDTO) o;
		if (demandDTO.getId() == null || getId() == null) {
			return false;
		}
		return Objects.equals(getId(), demandDTO.getId());
	}

	@Override
	public int hashCode() {
		return Objects.hashCode(getId());
	}

	@Override
	public String toString() {
		return "DemandDTO{" + "id=" + getId() + ", type='" + getType() + "'" + ", currentStatus='" + getCurrentStatus()
				+ "'" + ", description='" + getDescription() + "'" + ", createdDate='" + getCreatedDate() + "'"
				+ ", lastModifiedDate='" + getLastModifiedDate() + "'" + "}";
	}
}
