package com.kaylene.supervision.service.dto;

import com.kaylene.supervision.domain.TicketMessage;
import com.kaylene.supervision.domain.enumeration.Severity;
import com.kaylene.supervision.domain.enumeration.TicketStatus;

import javax.persistence.Lob;
import java.io.Serializable;
import java.time.Instant;
import java.util.List;
import java.util.Objects;

/**
 * A DTO for the Ticket entity.
 */
public class TicketDTO implements Serializable {

	private static final long serialVersionUID = 1L;

	private Long id;

	private String incidentType;

	private String incidentTypeLabel;

	private Instant incidentDate;

	private String trackIdZabbix;

	private String hostIdzabbix;

	private String hostLabel;

	private String hostType;

	private String hostTypeLabel;

	@Lob
	private String description;

	private TicketStatus status;

	private Instant closeDate;

	private Severity severity;

	private Instant createdDate;

	private Instant modifiedDate;

	// private Long adminId;

	private Long companyId;

	private String siteLabel;

	private Long adminId;

	private String adminName;

	private Long modifierId;

	private String modifierName;

	private List<TicketMessage> messages;

	public Long getId() {
		return id;
	}

	public void setId(Long id) {
		this.id = id;
	}

	public String getIncidentType() {
		return incidentType;
	}

	public void setIncidentType(String incidentType) {
		this.incidentType = incidentType;
	}

	public String getIncidentTypeLabel() {
		return incidentTypeLabel;
	}

	public void setIncidentTypeLabel(String incidentTypeLabel) {
		this.incidentTypeLabel = incidentTypeLabel;
	}

	public Instant getIncidentDate() {
		return incidentDate;
	}

	public void setIncidentDate(Instant incidentDate) {
		this.incidentDate = incidentDate;
	}

	public String getTrackIdZabbix() {
		return trackIdZabbix;
	}

	public void setTrackIdZabbix(String trackIdZabbix) {
		this.trackIdZabbix = trackIdZabbix;
	}

	public String getHostIdzabbix() {
		return hostIdzabbix;
	}

	public void setHostIdzabbix(String hostIdzabbix) {
		this.hostIdzabbix = hostIdzabbix;
	}

	public String getDescription() {
		return description;
	}

	public void setDescription(String description) {
		this.description = description;
	}

	public TicketStatus getStatus() {
		return status;
	}

	public void setStatus(TicketStatus status) {
		this.status = status;
	}

	public Instant getCloseDate() {
		return closeDate;
	}

	public void setCloseDate(Instant closeDate) {
		this.closeDate = closeDate;
	}

	public Severity getSeverity() {
		return severity;
	}

	public void setSeverity(Severity severity) {
		this.severity = severity;
	}

	public Instant getCreatedDate() {
		return createdDate;
	}

	public void setCreatedDate(Instant createdDate) {
		this.createdDate = createdDate;
	}

	public Instant getModifiedDate() {
		return modifiedDate;
	}

	public void setModifiedDate(Instant modifiedDate) {
		this.modifiedDate = modifiedDate;
	}

	public Long getCompanyId() {
		return companyId;
	}

	public void setCompanyId(Long companyId) {
		this.companyId = companyId;
	}

	public String getSiteLabel() {
		return siteLabel;
	}

	public void setSiteLabel(String siteLabel) {
		this.siteLabel = siteLabel;
	}

	public Long getAdminId() {
		return adminId;
	}

	public void setAdminId(Long adminId) {
		this.adminId = adminId;
	}

	public String getAdminName() {
		return adminName;
	}

	public void setAdminName(String adminName) {
		this.adminName = adminName;
	}

	public Long getModifierId() {
		return modifierId;
	}

	public void setModifierId(Long modifierId) {
		this.modifierId = modifierId;
	}

	public String getModifierName() {
		return modifierName;
	}

	public void setModifierName(String modifierName) {
		this.modifierName = modifierName;
	}

	public List<TicketMessage> getMessages() {
		return messages;
	}

	public void setMessages(List<TicketMessage> messages) {
		this.messages = messages;
	}

	@Override
	public boolean equals(Object o) {
		if (this == o) {
			return true;
		}
		if (o == null || getClass() != o.getClass()) {
			return false;
		}

		TicketDTO ticketDTO = (TicketDTO) o;
		if (ticketDTO.getId() == null || getId() == null) {
			return false;
		}
		return Objects.equals(getId(), ticketDTO.getId());
	}

	@Override
	public int hashCode() {
		return Objects.hashCode(getId());
	}

	@Override
	public String toString() {
		return "TicketDTO{" + "id=" + getId() + ", incidentType='" + getIncidentType() + "'" + ", incidentDate='"
				+ getIncidentDate() + "'" + ", trackIdZabbix='" + getTrackIdZabbix() + "'" + ", hostIdzabbix='"
				+ getHostIdzabbix() + "'" + ", hostLabel='" + getHostLabel() + "'" + ", adminId='" + getAdminId() + "'"
				+ ", adminName='" + getAdminName() + "'" + ", description='" + getDescription() + "'" + ", status='"
				+ getStatus() + "'" + ", closeDate='" + getCloseDate() + "'" + ", severity='" + getSeverity() + "'"
				+ ", createdDate='" + getCreatedDate() + "'" + ", modifiedDate='" + getModifiedDate() + "'"
				+ ", siteLabel='" + getSiteLabel() + "'" + "}";
	}

	public String getHostLabel() {
		return hostLabel;
	}

	public void setHostLabel(String hostLabel) {
		this.hostLabel = hostLabel;
	}

	public String getHostType() {
		return hostType;
	}

	public void setHostType(String hostType) {
		this.hostType = hostType;
	}

	public String getHostTypeLabel() {
		return hostTypeLabel;
	}

	public void setHostTypeLabel(String hostTypeLabel) {
		this.hostTypeLabel = hostTypeLabel;
	}

}
