package com.kaylene.supervision.service.dto;

import java.io.Serializable;
import java.time.Instant;
import java.util.Objects;

/**
 * A DTO for the TicketMessage entity.
 */
public class TicketMessageDTO implements Serializable {

	private static final long serialVersionUID = 1L;

	private Long id;

	private Long ticketId;

	private String text;

	private Long userId;

	private String userName;

	private Instant createdDate;

	public Long getId() {
		return id;
	}

	public void setId(Long id) {
		this.id = id;
	}

	public Long getTicketId() {
		return ticketId;
	}

	public void setTicketId(Long ticketId) {
		this.ticketId = ticketId;
	}

	public String getText() {
		return text;
	}

	public void setText(String text) {
		this.text = text;
	}

	public Long getUserId() {
		return userId;
	}

	public void setUserId(Long userId) {
		this.userId = userId;
	}

	public String getUserName() {
		return userName;
	}

	public void setUserName(String userName) {
		this.userName = userName;
	}

	public Instant getCreatedDate() {
		return createdDate;
	}

	public void setCreatedDate(Instant createdDate) {
		this.createdDate = createdDate;
	}

	@Override
	public boolean equals(Object o) {
		if (this == o) {
			return true;
		}
		if (o == null || getClass() != o.getClass()) {
			return false;
		}

		TicketMessageDTO ticketMessageDTO = (TicketMessageDTO) o;
		if (ticketMessageDTO.getId() == null || getId() == null) {
			return false;
		}
		return Objects.equals(getId(), ticketMessageDTO.getId());
	}

	@Override
	public int hashCode() {
		return Objects.hashCode(getId());
	}

	@Override
	public String toString() {
		return "TicketMessageDTO{" + "id=" + getId() + ", ticketId='" + getTicketId() + "'" + ", text='" + getText()
				+ "'" + ", userId='" + getUserId() + "'" + ", userName='" + getUserName() + "'" + ", createdDate='"
				+ getCreatedDate() + "'" + "}";
	}
}
