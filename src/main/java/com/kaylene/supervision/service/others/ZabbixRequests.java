package com.kaylene.supervision.service.others;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.http.HttpEntity;
import org.springframework.http.HttpHeaders;
import org.springframework.http.MediaType;
import org.springframework.web.client.HttpClientErrorException;
import org.springframework.web.client.RestTemplate;

import java.util.List;

public class ZabbixRequests {

	private final static Logger log = LoggerFactory.getLogger(ZabbixRequests.class);

	public List<Host> getHosts(String url, String login, String password) {

		AuthenticationToZabbix authenticationToZabbix = new AuthenticationToZabbix();

		String authResult = null;
		try {
			authResult = authenticationToZabbix.authenticate(url, login, password);
		} catch (JSONException e) {
			log.error("{}", e.getMessage());
		}

		RestTemplate restTemplate = new RestTemplate();

		JSONArray selectInterfacesProperties = new JSONArray();
		selectInterfacesProperties.put("interfaceid");
		selectInterfacesProperties.put("ip");

		JSONObject params = new JSONObject();

		JSONObject request = new JSONObject();
		try {
			params.put("selectInterfaces", selectInterfacesProperties);
			request.put("jsonrpc", "2.0");
			request.put("method", "host.get");
			request.put("params", params);
			request.put("id", 1);
			request.put("auth", authResult);
		} catch (JSONException je) {
			log.error("{}", je.getMessage());
		}

		if (authResult == null)
			return null;

		HttpHeaders headers = new HttpHeaders();
		headers.setContentType(MediaType.APPLICATION_JSON);
		HttpEntity<String> entity = new HttpEntity<String>(request.toString(), headers);
		try {
			Hosts hosts = restTemplate.postForObject(url, entity, Hosts.class);
			return hosts.getResult() != null ? hosts.getResult() : null;
			
		} catch (HttpClientErrorException hcee) {
			log.error("{}", hcee.getMessage());
			return null;
		}
	}

}
