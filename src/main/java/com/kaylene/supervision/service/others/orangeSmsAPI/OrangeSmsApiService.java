package com.kaylene.supervision.service.others.orangeSmsAPI;

import org.json.JSONObject;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.http.HttpEntity;
import org.springframework.http.HttpHeaders;
import org.springframework.http.MediaType;
import org.springframework.stereotype.Service;
import org.springframework.web.client.RestTemplate;
import org.springframework.web.util.UriComponents;
import org.springframework.web.util.UriComponentsBuilder;

import java.net.URI;

import static com.kaylene.supervision.constants.KayleneInfos.*;

@Service
public class OrangeSmsApiService {

	private final static Logger log = LoggerFactory.getLogger(OrangeSmsApiService.class);

	RestTemplate restTemplate = new RestTemplate();

	public String getAccesToken() {

		HttpHeaders headers = new HttpHeaders();
		headers.setContentType(MediaType.APPLICATION_FORM_URLENCODED);
		headers.add("Authorization", GET_TOKEN_AUTHORIZATION_HEADER);

		log.info("Sending getToken request to Orange SMS API");

		HttpEntity<String> entity = new HttpEntity<String>(GET_TOKEN_REQUEST_BODY, headers);
		try {
			Token response = restTemplate.postForObject(GET_TOKEN_URL, entity, Token.class);
			String accessToken = response.getAccess_token();
			return accessToken;
		} catch (Exception e) {
			log.error("Failed to get access token from Orange SMS API : {}", e.getMessage());
			return null;
		}
	}

	public void sendSms(String phoneNumber, String message) {

		// taille maximale d'un SMS: 140 caractères, si le message dépasse cette limite,
		// prendre les 160 premiers caractères
		if (message.length() > 140) {
			message = message.substring(0, 140);
		}

		JSONObject requestBody = new JSONObject();
		JSONObject outboundSMSMessageRequest = new JSONObject();
		JSONObject outboundSMSTextMessage = new JSONObject();

		try {
			outboundSMSTextMessage.put("message", message);

			outboundSMSMessageRequest.put("address", "tel:+221" + phoneNumber);
			outboundSMSMessageRequest.put("senderAddress", SMS_SENDER_ADDRESS);
			outboundSMSMessageRequest.put("senderName", SMS_SENDER_NAME);
			outboundSMSMessageRequest.put("outboundSMSTextMessage", outboundSMSTextMessage);
			requestBody.put("outboundSMSMessageRequest", outboundSMSMessageRequest);
		} catch (Exception e) {
			log.error("{}", e.getMessage());
		}

		HttpHeaders headers = new HttpHeaders();
		headers.setContentType(MediaType.APPLICATION_JSON);
		headers.add("Authorization", "Bearer" + " " + getAccesToken());

		// ******** Build URI from URL to prevent restTemplate of encoding it ******
		String url = SEND_SMS_URL;
		UriComponentsBuilder builder = UriComponentsBuilder.fromHttpUrl(url);
		UriComponents components = builder.build(true);
		URI uri = components.toUri();
		// **************************************************************************

		HttpEntity<String> entity = new HttpEntity<>(requestBody.toString(), headers);

		try {
			restTemplate.postForObject(uri, entity, Object.class);
			log.debug("Sent SMS to '{}'", phoneNumber);
		} catch (Exception e) {
			log.error("An error occured when trying to send SMS to '{}'", phoneNumber);
			log.error("{}", e.getMessage());
		}
	}
}
