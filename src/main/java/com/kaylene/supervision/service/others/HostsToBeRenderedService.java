package com.kaylene.supervision.service.others;

import com.kaylene.supervision.config.ApplicationProperties;
import com.kaylene.supervision.domain.Company;
import com.kaylene.supervision.domain.HostDemand;
import com.kaylene.supervision.domain.Site;
import com.kaylene.supervision.domain.enumeration.DemandStatus;
import com.kaylene.supervision.repository.CompanyRepository;
import com.kaylene.supervision.repository.HostDemandRepository;
import com.kaylene.supervision.repository.SiteRepository;
import com.kaylene.supervision.service.DemandService;
import com.kaylene.supervision.service.HostTypeService;
import com.kaylene.supervision.service.TicketService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.ArrayList;
import java.util.List;

import static com.kaylene.supervision.constants.KayleneInfos.KAYLENE_COMPANY_ID;

@Service
public class HostsToBeRenderedService {

	@Autowired
	HostDemandRepository hostDemandRepository;

	@Autowired
	SiteRepository siteRepository;

	@Autowired
	TicketService ticketService;

	@Autowired
	DemandService demandService;

	@Autowired
	HostTypeService hostTypeService;

	@Autowired
	CompanyRepository companyRepository;

	@Autowired
	private ApplicationProperties applicationProperties;


	public List<HostsToBeRendered> getHostsToBeRendered(Long companyId, boolean isAdmin) {
		Company company = companyRepository.findOne(companyId);
		if (company == null) {
			return new ArrayList<HostsToBeRendered>();
		}

		List<Host> hostsFromZabbix = new ArrayList<Host>();
		ZabbixRequests zabbixRequests = new ZabbixRequests();
		String url = this.applicationProperties.getZabbix().getUrl();
		if (company.getId() == KAYLENE_COMPANY_ID && isAdmin) {
			String login = this.applicationProperties.getZabbix().getLogin();
			String password = this.applicationProperties.getZabbix().getPassword();
			hostsFromZabbix = zabbixRequests.getHosts(url, login, password);
		} else {
			String login = company.getZabbixLogin();
			String password = company.getZabbixPassword();
			hostsFromZabbix = zabbixRequests.getHosts(url, login, password);
		}

		List<HostsToBeRendered> hostsToBeRendered = new ArrayList<HostsToBeRendered>();
		if (hostsFromZabbix != null) {
			HostsToBeRendered currentHostToBeRendered = new HostsToBeRendered();
			Host currentHostFromZabbix = new Host();
			String currentHostId;
			//String currentHostName;
			List<HostDemand> hostsDemand = new ArrayList<HostDemand>();
			HostDemand currentHostDemand = new HostDemand();
			Site currentSite = new Site();
			for (int i = 0; i < hostsFromZabbix.size(); i++) {
				// Infos from ZabbixServer API
				currentHostFromZabbix = hostsFromZabbix.get(i);
				currentHostId = currentHostFromZabbix.getHostid();
				//currentHostName = currentHostFromZabbix.getName();
				currentHostToBeRendered.setHostid(currentHostId);
				currentHostToBeRendered.setName(currentHostFromZabbix.getName());
				currentHostToBeRendered.setInterfaces(currentHostFromZabbix.getInterfaces());
				currentHostToBeRendered.setStatus(currentHostFromZabbix.getStatus());
				currentHostToBeRendered.setMaintenance_status(currentHostFromZabbix.getMaintenance_status());
				currentHostToBeRendered.setProxy_hostid(currentHostFromZabbix.getProxy_hostid());

				// Infos from our API
				// hostsDemand = hostDemandRepository.findByZabbixId(currentHostId);
				hostsDemand = hostDemandRepository.findByZabbixIdAndDemandCurrentStatus(currentHostId, DemandStatus.DONE);
				//hostsDemand = hostDemandRepository.findByLabelAndDemandCurrentStatus(currentHostName, DemandStatus.DONE);
				if (!hostsDemand.isEmpty()) {
					currentHostDemand = hostsDemand.get(hostsDemand.size() - 1);
					currentHostToBeRendered.setIntegrationDate(currentHostDemand.getDateIntegration().toString());
					currentHostToBeRendered.setType(currentHostDemand.getType());
					currentHostToBeRendered
							.setTypeLabel(hostTypeService.getHostTypeLabel(currentHostToBeRendered.getType()));
					try {
						currentSite = siteRepository.findOne(currentHostDemand.getSiteId());
						currentHostToBeRendered.setSite(currentSite.getLabel());
						currentHostToBeRendered.setSiteId(currentSite.getId());
					} catch (Exception e) {
						currentHostToBeRendered.setSite("");
						currentHostToBeRendered.setSiteId(null);
					}
					currentHostToBeRendered.setNumberOfTickets(ticketService.getNumberOfTicketsOnHost(currentHostId, companyId));
					currentHostToBeRendered.setNumberOfDemands(demandService.getNumberOfDemandsOnHost(currentHostId));
					currentHostToBeRendered.setTicketSeverity(ticketService.getTicketSeverityOnHost(currentHostId, companyId));
					hostsToBeRendered.add(currentHostToBeRendered);
				}
				currentHostToBeRendered = new HostsToBeRendered();
			}
			return hostsToBeRendered;
		} else {
			return hostsToBeRendered;
		}

	}
}
