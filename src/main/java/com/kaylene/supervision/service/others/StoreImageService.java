package com.kaylene.supervision.service.others;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.core.io.Resource;
import org.springframework.core.io.UrlResource;
import org.springframework.stereotype.Service;
import org.springframework.util.FileSystemUtils;
import org.springframework.web.multipart.MultipartFile;

import java.io.IOException;
import java.net.MalformedURLException;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;

// TODO Malick check  

@Service
public class StoreImageService {
	private final Logger log = LoggerFactory.getLogger(StoreImageService.class);

	private final Path rootLocation = Paths.get("src/main/resources");
	private static final String FAIL = "FAIL";

	public void store(MultipartFile file) {
		try {
			Files.copy(file.getInputStream(), this.rootLocation.resolve(file.getOriginalFilename()));
		} catch (Exception e) {
			throw new RuntimeException(FAIL);
		}
	}

	public Resource loadFile(String filename) {
		try {
			Path file = rootLocation.resolve(filename);
			Resource resource = new UrlResource(file.toUri());
			if (resource.exists() || resource.isReadable()) {
				log.debug("********  ABOUT TO RETURN RESOURCE  ****************");
				return resource;
			} else {
				throw new RuntimeException(FAIL);
			}
		} catch (MalformedURLException e) {
			throw new RuntimeException(FAIL);
		}
	}

	public void deleteAll() {
		FileSystemUtils.deleteRecursively(rootLocation.toFile());
	}

	public void init() {
		try {
			Files.createDirectory(rootLocation);
		} catch (IOException e) {
			throw new RuntimeException("Could not initialize storage!");
		}
	}

}
