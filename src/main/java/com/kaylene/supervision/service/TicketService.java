package com.kaylene.supervision.service;

import com.kaylene.supervision.domain.Ticket;
import com.kaylene.supervision.domain.User;
import com.kaylene.supervision.domain.enumeration.TicketStatus;
import com.kaylene.supervision.repository.ContractRepository;
import com.kaylene.supervision.repository.TicketRepository;
import com.kaylene.supervision.repository.UserRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;
import java.util.Optional;
import java.util.stream.Collectors;

@Service
public class TicketService {

	@Autowired
	TicketRepository ticketRepository;

	@Autowired
	UserRepository userRepository;

	@Autowired
	MailService mailService;

	@Autowired
	SmsService smsService;

	@Autowired
	ContractRepository contractRepository;

	public boolean isTicketOpenedOnHost(String hostIdzabbix, Long companyId) {
		List<Ticket> ticketsOnHost = ticketRepository.findByHostIdzabbix(hostIdzabbix).stream()
            .filter(ticket -> ticket.getCompany().getId().equals(companyId))
            .collect(Collectors.toList());
		if (ticketsOnHost.isEmpty()) {
			return false;
		} else {
			Ticket lastTicket = ticketsOnHost.get(ticketsOnHost.size() - 1);
			return lastTicket.getStatus() != TicketStatus.CLOSE;
		}
	}

	public int getNumberOfTicketsOnHost(String hostIdzabbix, Long companyId) {
		List<Ticket> ticketsOnHost = ticketRepository.findByHostIdzabbix(hostIdzabbix).stream()
            .filter(ticket -> ticket.getCompany().getId().equals(companyId))
            .collect(Collectors.toList());
		return ticketsOnHost.size();
	}

	public void sendTicket(Ticket ticket) {
		List<User> users = userRepository.findByCompanyId(ticket.getCompany().getId());
		if (users.isEmpty()) {
			return;
		}
        users.stream().filter(User::getActivated).forEach( user -> {
                if (user.getOnTicketEMail()) {
                    mailService.sendTicketCreationEmail(ticket, user);
                }
                /*if (user.getOnTicketPhoneCall()) {
                    // phoneCallService, à implémenter
                }*/
                if (user.getOnTicketSMS()) {
                    smsService.sendTicketCreationSms(ticket, user);
                }
            }
        );
	}

	public List<Ticket> getCompanyOpenedTicketsForGivenType(Long companyId, String hostTypeName, TicketStatus status) {
		return ticketRepository.findByCompanyIdAndHostTypeAndStatus(companyId, hostTypeName, status);
	}

	public int getNumberOfCompanyOpenedTicketsForGivenType(Long companyId, String hostTypeName, TicketStatus status) {
		List<Ticket> tickets = getCompanyOpenedTicketsForGivenType(companyId, hostTypeName, status);
		if (tickets.isEmpty()) {
			return 0;
		}
		return tickets.size();
	}

	public boolean isIncidentTypeReferencedByTicket(String incidentType) {
		List<Ticket> tickets = ticketRepository.findByIncidentType(incidentType);
		return tickets.isEmpty() ? false : true;
	}

	public String getTicketSeverityOnHost(String hostIdzabbix, Long companyId) {
		Optional<Ticket> ticket = ticketRepository.findByHostIdzabbixAndStatus(hostIdzabbix, TicketStatus.OPEN).stream()
            .filter(t -> t.getCompany().getId().equals(companyId))
            .findFirst();
		if (ticket.isPresent() && ticket.get().getSeverity() != null) {
			return ticket.get().getSeverity().toString();
		} else {
			return "noTicket";
		}
	}
}
