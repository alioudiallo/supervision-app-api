package com.kaylene.supervision.service;

import com.kaylene.supervision.config.Constants;
import com.kaylene.supervision.domain.Authority;
import com.kaylene.supervision.domain.Company;
import com.kaylene.supervision.domain.User;
import com.kaylene.supervision.repository.AuthorityRepository;
import com.kaylene.supervision.repository.UserRepository;
import com.kaylene.supervision.security.AuthoritiesConstants;
import com.kaylene.supervision.security.SecurityUtils;
import com.kaylene.supervision.service.dto.UserDTO;
import com.kaylene.supervision.service.mapper.UserMapper;
import com.kaylene.supervision.service.util.RandomUtil;
import com.kaylene.supervision.web.rest.vm.KeyAndLoginVM;
import com.kaylene.supervision.web.rest.vm.ManagedUserVM;
import org.apache.commons.validator.routines.EmailValidator;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.cache.CacheManager;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.scheduling.annotation.Scheduled;
import org.springframework.security.crypto.password.PasswordEncoder;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.time.Instant;
import java.time.temporal.ChronoUnit;
import java.util.HashSet;
import java.util.List;
import java.util.Optional;
import java.util.Set;
import java.util.stream.Collectors;

import static com.kaylene.supervision.constants.KayleneInfos.KAYLENE_COMPANY_ID;

/**
 * Service class for managing users.
 */
@Service
@Transactional
public class UserService {

	private final Logger log = LoggerFactory.getLogger(UserService.class);

	private static final String USERS_CACHE = "users";

	private final UserRepository userRepository;

	private final PasswordEncoder passwordEncoder;

	private final AuthorityRepository authorityRepository;

	private final CacheManager cacheManager;

	@Autowired
	UserMapper userMapper;

	public UserService(UserRepository userRepository, PasswordEncoder passwordEncoder,
			AuthorityRepository authorityRepository, CacheManager cacheManager) {
		this.userRepository = userRepository;
		this.passwordEncoder = passwordEncoder;
		this.authorityRepository = authorityRepository;
		this.cacheManager = cacheManager;
	}

	public void modifierPassword(String login, String newpassword) {
		userRepository.findOneByLogin(login).ifPresent(user -> {
			user.setPassword(newpassword);
			userRepository.save(user);
			cacheManager.getCache(USERS_CACHE).evict(user.getLogin());
			log.debug("Changed password for User: {}", user);

		});
	}

	public Optional<User> activateRegistration(String key) {
		log.debug("Activating user for activation key {}", key);
		return userRepository.findOneByActivationKey(key).map(user -> {
			// activate given user for the registration key.
			user.setActivated(true);
			user.setActivationKey(null);
			cacheManager.getCache(USERS_CACHE).evict(user.getLogin());
			log.debug("Activated user: {}", user);
			return user;
		});
	}

	public Optional<User> completePasswordReset(String newPassword, String key) {
		log.debug("Reset user password for reset key {}", key);

		return userRepository.findOneByResetKey(key)
				.filter(user -> user.getResetDate().isAfter(Instant.now().minusSeconds(86400))).map(user -> {
					user.setPassword(passwordEncoder.encode(newPassword));
					user.setResetKey(null);
					user.setResetDate(null);
					cacheManager.getCache(USERS_CACHE).evict(user.getLogin());
					return user;
				});
	}

	public Optional<User> requestPasswordReset(String mail) {
		return userRepository.findOneByEmailIgnoreCase(mail).filter(User::getActivated).map(user -> {
			user.setResetKey(RandomUtil.generateResetKey());
			user.setResetDate(Instant.now());
			cacheManager.getCache(USERS_CACHE).evict(user.getLogin());
			return user;
		});
	}

	public User registerUser(ManagedUserVM userDTO) {

		User newUser = new User();
		Authority authority = authorityRepository.findOne(AuthoritiesConstants.USER);
		Set<Authority> authorities = new HashSet<>();
		String encryptedPassword = passwordEncoder.encode(userDTO.getPassword());
		newUser.setLogin(userDTO.getLogin());
		// new user gets initially a generated password
		newUser.setPassword(encryptedPassword);
		newUser.setFirstName(userDTO.getFirstName());
		newUser.setLastName(userDTO.getLastName());
		newUser.setEmail(userDTO.getEmail());
		newUser.setImageUrl(userDTO.getImageUrl());
		newUser.setLangKey(userDTO.getLangKey());
		// new user is not active
		newUser.setActivated(false);
		// new user gets registration key
		newUser.setActivationKey(RandomUtil.generateActivationKey());
		authorities.add(authority);
		newUser.setAuthorities(authorities);
		userRepository.save(newUser);
		log.debug("Created Information for User: {}", newUser);
		return newUser;
	}

	public User createUser(ManagedUserVM userDTO) {
		User user = new User();
		user.setLogin(userDTO.getLogin());
		user.setFirstName(userDTO.getFirstName());
		user.setLastName(userDTO.getLastName());
		// user.setEmail(userDTO.getEmail());
		user.setEmail(userDTO.getLogin());// le login est utilisé comme email
		user.setOnTicketEMail(userDTO.getOnTicketEMail());
		user.setOnTicketSMS(userDTO.getOnTicketSMS());
		user.setOnTicketPhoneCall(userDTO.getOnTicketPhoneCall());
		user.setPoste(userDTO.getPoste());
		user.setPhoneNumber(userDTO.getPhoneNumber());
		user.setImageUrl(userDTO.getImageUrl());
		user.setAccesFacture(userDTO.getAccesFacture());
		if (userDTO.getLangKey() == null) {
			user.setLangKey(Constants.DEFAULT_LANGUAGE); // default language
		} else {
			user.setLangKey(userDTO.getLangKey());
		}
		if (userDTO.getAuthorities() != null) {
			Set<Authority> authorities = userDTO.getAuthorities().stream().map(authorityRepository::findOne)
					.collect(Collectors.toSet());
			user.setAuthorities(authorities);
		}
		String encryptedPassword = passwordEncoder.encode(userDTO.getPassword());
		user.setPassword(encryptedPassword);
		// user.setResetKey(RandomUtil.generateResetKey());
		user.setResetKey(null);
		// user.setResetDate(Instant.now());
		user.setResetDate(null);
		user.setActivated(true);
		Company company = new Company();
		company.setId(userDTO.getCompanyId());
		user.setCompany(company);
		userRepository.save(user);
		log.debug("Created Information for User: {}", user);
		return user;
	}

	/**
	 * Update basic information (first name, last name, email, language) for the
	 * current user.
	 *
	 * @param firstName
	 *            first name of user
	 * @param lastName
	 *            last name of user
	 * @param email
	 *            email id of user
	 * @param langKey
	 *            language key
	 * @param imageUrl
	 *            image URL of user
	 */
	public void updateUser(String firstName, String lastName, String email, String langKey, String imageUrl) {
		userRepository.findOneByLogin(SecurityUtils.getCurrentUserLogin()).ifPresent(user -> {
			user.setFirstName(firstName);
			user.setLastName(lastName);
			user.setEmail(email);
			user.setLangKey(langKey);
			user.setImageUrl(imageUrl);
			cacheManager.getCache(USERS_CACHE).evict(user.getLogin());
			log.debug("Changed Information for User: {}", user);
		});
	}

	/**
	 * Update all information for a specific user, and return the modified user.
	 *
	 * @param userDTO
	 *            user to update
	 * @return updated user
	 */
	public Optional<UserDTO> updateUser(UserDTO userDTO) {
		return Optional.of(userRepository.findOne(userDTO.getId())).map(user -> {
			// user.setLogin(userDTO.getLogin());
			// user.setPassword(passwordEncoder.encode(userDTO.getPassword()));
			user.setFirstName(userDTO.getFirstName());
			user.setLastName(userDTO.getLastName());
			// user.setEmail(userDTO.getEmail());
			user.setImageUrl(userDTO.getImageUrl());
			user.setActivated(userDTO.isActivated());
			user.setLangKey(userDTO.getLangKey());
			user.setAccesFacture(userDTO.getAccesFacture());

			Company company = new Company();
			company.setId(userDTO.getCompanyId());
			user.setCompany(company);
			user.setOnTicketEMail(userDTO.getOnTicketEMail());
			user.setOnTicketSMS(userDTO.getOnTicketSMS());
			user.setOnTicketPhoneCall(userDTO.getOnTicketPhoneCall());
			user.setPhoneNumber(userDTO.getPhoneNumber());
			user.setPoste(userDTO.getPoste());
			Set<Authority> managedAuthorities = user.getAuthorities();
			managedAuthorities.clear();
			userDTO.getAuthorities().stream().map(authorityRepository::findOne).forEach(managedAuthorities::add);
			cacheManager.getCache(USERS_CACHE).evict(user.getLogin());
			log.debug("Changed Information for User: {}", user);
			return user;
		}).map(userMapper::userToUserDTO);
	}

	public void deleteUser(String login) {
		userRepository.findOneByLogin(login).ifPresent(user -> {
		    // delete authorities
            user.getAuthorities().clear();
            userRepository.logicalDelete(user);
			cacheManager.getCache(USERS_CACHE).evict(login);
			log.warn("Deleted User: {}", login);
		});
	}

	public void unDeleteUser(String login) {
		userRepository.findOneByLogin(login).ifPresent(user -> {
            userRepository.logicalUnDelete(user);
			log.warn("User: {} un-deleted", login);
		});
	}

	public void deleteUserById(Long id) {
		userRepository.delete(id);

	}

	public void changePassword(String password) {
		userRepository.findOneByLogin(SecurityUtils.getCurrentUserLogin()).ifPresent(user -> {
			String encryptedPassword = passwordEncoder.encode(password);
			user.setPassword(encryptedPassword);
			cacheManager.getCache(USERS_CACHE).evict(user.getLogin());
			log.debug("Changed password for User: {}", user);
		});
	}

	public void changeLogin(UserDTO userDTO) {
		userRepository.findOneByLogin(SecurityUtils.getCurrentUserLogin()).ifPresent(user -> {
			user.setActivationKey(RandomUtil.generateResetKey());
			user.setLogin(userDTO.getLogin());
			cacheManager.getCache(USERS_CACHE).evict(user.getLogin());
			log.debug("Changed login for User: {}", user);
		});
	}

	public void customChangePassword(String login, String password) {
		userRepository.findOneByLogin(login).ifPresent(user -> {
			String encryptedPassword = passwordEncoder.encode(password);
			user.setPassword(encryptedPassword);
			cacheManager.getCache(USERS_CACHE).evict(user.getLogin());
			log.debug("Changed password for User: {}", user);
		});
	}

	public boolean isPhoneNumberAlreadyInUse(String phone) {
		phone = phone.trim();
		if (phone == null) {
			return false;
		}
		User user = userRepository.findByPhoneNumber(phone);
		return user == null ? false : true;
	}

	public Long getCurrentUserId() {
		String login = SecurityUtils.getCurrentUserLogin();
		User currentUser = userRepository.findByLogin(login);
		return currentUser.getId();
	}

	public String getCurrentUserName() {
		Long id = getCurrentUserId();
		User currentUser = userRepository.findOne(id);
		return currentUser.getFirstName() + " " + currentUser.getLastName();
	}

	public User findByLogin(String login) {
		return userRepository.findByLogin(login);
	}

	public boolean isCurrentUserKayleneAdmin() {
		String login = SecurityUtils.getCurrentUserLogin();
		User currentUser = userRepository.findByLogin(login);
		Company company = currentUser.getCompany();
		if (company == null) {
			return false;
		}
		return (company.getId() == KAYLENE_COMPANY_ID) ? true : false; 
	}

	public boolean isEmailInvalid(String email) {
		return (!EmailValidator.getInstance().isValid(email)) ? true : false;
	}

	@Transactional(readOnly = true)
	public Page<UserDTO> getAllManagedUsers(Pageable pageable) {
		return userRepository.findAllByLoginNot(pageable, Constants.ANONYMOUS_USER).map(userMapper::userToUserDTO);
	}

	@Transactional(readOnly = true)
	public Optional<User> getUserWithAuthoritiesByLogin(String login) {
		return userRepository.findOneWithAuthoritiesByLogin(login);
	}

	@Transactional(readOnly = true)
	public User getUserWithAuthorities(Long id) {
		return userRepository.findOneWithAuthoritiesById(id);
	}

	@Transactional(readOnly = true)
	public User getUserWithAuthorities() {
		return userRepository.findOneWithAuthoritiesByLogin(SecurityUtils.getCurrentUserLogin()).orElse(null);
	}

	/**
	 * Not activated users should be automatically deleted after 3 days.
	 * <p>
	 * This is scheduled to get fired everyday, at 01:00 (am).
	 */
	@Scheduled(cron = "0 0 1 * * ?")
	public void removeNotActivatedUsers() {
		List<User> users = userRepository
				.findAllByActivatedIsFalseAndCreatedDateBefore(Instant.now().minus(3, ChronoUnit.DAYS));
		for (User user : users) {
			log.debug("Deleting not activated user {}", user.getLogin());
			userRepository.delete(user);
			cacheManager.getCache(USERS_CACHE).evict(user.getLogin());
		}
	}

	/**
	 * @return a list of all the authorities
	 */
	public List<String> getAuthorities() {
		return authorityRepository.findAll().stream().map(Authority::getName).collect(Collectors.toList());
	}

	// completer la resialiation du login
	// TODO malick
	User user;

	public Optional<User> completLoginReset(KeyAndLoginVM keyAndLogin) {
		log.debug("Reset user login for reset key {}", keyAndLogin.getKey());

		return userRepository.findOneByEmailIgnoreCase(keyAndLogin.getOldLogin())
				.filter(user -> user.getResetDate().isAfter(Instant.now().minusSeconds(86400))
						&& user.getResetKey().equals(keyAndLogin.getKey()))
				.map(user -> {
					user.setLogin(keyAndLogin.getNewLogin());
					user.setEmail(keyAndLogin.getNewLogin());
					user.setResetKey(null);
					user.setResetDate(null);
					cacheManager.getCache(USERS_CACHE).evict(user.getLogin());
					return user;
				});
	}

	public Optional<User> requestLoginReset(String newlogin) {
		return userRepository.findOneByEmailIgnoreCase(SecurityUtils.getCurrentUserLogin()).map(user -> {
			user.setResetKey(RandomUtil.generateResetKey());
			user.setResetDate(Instant.now());
			cacheManager.getCache(USERS_CACHE).evict(user.getLogin());
			return user;
		});
	}

	public Optional<UserDTO> updateUserPassword(UserDTO userDTO) {
		return Optional.of(userRepository.findOne(userDTO.getId())).map(user -> {
			user.setLogin(userDTO.getLogin());
			user.setPassword(passwordEncoder.encode(userDTO.getPassword()));
			log.debug("Changed Information for User: {}", user);
			return user;
		}).map(userMapper::userToUserDTO);
	}

	// Liste des utilisateurs d'une company
	public List<User> getAllUsersByCompanyId(Long id) {
		return userRepository.findByCompanyId(id);
	}

	public void disableUserWhenContractExpired(Long companyId) {
		List<User> users = getAllUsersByCompanyId(companyId);
		for (User user : users) {
			log.debug("Contract extension - Reactivation des users  : {}", user.getEmail());
			user.setActivated(false);
			cacheManager.getCache(USERS_CACHE).evict(user.getLogin());
			userRepository.save(user);
			log.debug("Changed Information for User: {}", user);
		}
	}
	
	public void reactiveUsersAfterContractExtension(Long companyId) {
		List<User> users = getAllUsersByCompanyId(companyId);
		for (User user : users) {
			log.debug("Contract extension - Reactivation des users  : {}", user.getEmail());
			user.setActivated(true);
			cacheManager.getCache(USERS_CACHE).evict(user.getLogin());
			userRepository.save(user);
			log.debug("Changed Information for User: {}", user);
		}
	}

}
