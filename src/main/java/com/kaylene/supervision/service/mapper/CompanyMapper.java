package com.kaylene.supervision.service.mapper;

import com.kaylene.supervision.domain.Company;
import com.kaylene.supervision.domain.Site;
import com.kaylene.supervision.domain.User;
import com.kaylene.supervision.service.dto.CompanyDTO;
import com.kaylene.supervision.service.dto.SiteDTO;
import com.kaylene.supervision.service.dto.UserDTO;
import org.mapstruct.IterableMapping;
import org.mapstruct.Mapper;
import org.mapstruct.Mapping;
import org.mapstruct.Named;

import java.util.Set;

/**
 * Mapper for the entity Company and its DTO CompanyDTO.
 */
@Mapper(componentModel = "spring", uses = {SiteMapper.class, UserMapper.class})
public interface CompanyMapper extends EntityMapper<CompanyDTO, Company> {
    
    @Mapping(target = "sites", expression = "java(toSiteEntities(companyDTO.getSites()))")
    @Mapping(target = "contactPersons", ignore = true)
    @Mapping(target = "contracts", ignore = true)
    Company toEntity(CompanyDTO companyDTO);
    
    CompanyDTO toDto(Company company);
	

    default Company fromId(Long id) {
        if (id == null) {
            return null;
        }
        Company company = new Company();
        company.setId(id);
        return company;
    }
    
    @Named("mapSiteToDto")   
    @Mapping(source = "companyId", target = "company") 
    Site toSiteEntity(SiteDTO siteDTO); 
     
    @IterableMapping(qualifiedByName="mapSiteToDto") 
    Set<Site> toSiteEntities(Set<SiteDTO> siteDTO);
    
    @Named("mapUserToDto")   
    @Mapping(source = "companyId", target = "company") 
    User toUserEntity(UserDTO userDTO); 
     
    @IterableMapping(qualifiedByName="mapUserToDto") 
    Set<User> toUsersEntities(Set<UserDTO> userDTO);
    
    
}
