package com.kaylene.supervision.service.mapper;

import com.kaylene.supervision.domain.Authority;
import com.kaylene.supervision.domain.User;
import com.kaylene.supervision.service.dto.UserDTO;
import org.mapstruct.Mapping;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;
import java.util.Objects;
import java.util.Set;
import java.util.stream.Collectors;

/**
 * Mapper for the entity User and its DTO called UserDTO.
 *
 * Normal mappers are generated using MapStruct, this one is hand-coded as
 * MapStruct support is still in beta, and requires a manual step with an IDE.
 */
@Service
public class UserMapper {

	@Autowired
	CompanyMapper companyMapper;

	@Mapping(source = "company.id", target = "companyId")
	public UserDTO userToUserDTO(User user) {
		UserDTO userDTO = new UserDTO(user);
		if (user.getCompany() == null) {
			return userDTO;
		}
		userDTO.setOnTicketEMail(user.getOnTicketEMail());
		userDTO.setOnTicketSMS(user.getOnTicketSMS());
		userDTO.setOnTicketPhoneCall(user.getOnTicketPhoneCall());
		userDTO.setPhoneNumber(user.getPhoneNumber());
		userDTO.setPoste(user.getPoste());
		userDTO.setCompanyId(user.getCompany().getId());
		userDTO.setAccesFacture(user.getAccesFacture());
		return userDTO;
	}

	public List<UserDTO> usersToUserDTOs(List<User> users) {
		return users.stream().filter(Objects::nonNull).map(this::userToUserDTO).collect(Collectors.toList());
	}

	@Mapping(source = "companyId", target = "company")
	public User userDTOToUser(UserDTO userDTO) {
		if (userDTO == null) {
			return null;
		} else {
			User user = new User();
			user.setId(userDTO.getId());
			user.setLogin(userDTO.getLogin());
			user.setFirstName(userDTO.getFirstName());
			user.setLastName(userDTO.getLastName());
			user.setEmail(userDTO.getEmail());
			user.setImageUrl(userDTO.getImageUrl());
			user.setActivated(userDTO.isActivated());
			user.setLangKey(userDTO.getLangKey());
			user.setPhoneNumber(userDTO.getPhoneNumber());
			user.setPoste(userDTO.getPoste());
			user.setOnTicketEMail(userDTO.getOnTicketEMail());
			user.setOnTicketSMS(userDTO.getOnTicketSMS());
			user.setOnTicketPhoneCall(userDTO.getOnTicketPhoneCall());
			user.setCompany(companyMapper.fromId(userDTO.getCompanyId()));
			user.setAccesFacture(userDTO.getAccesFacture());
			Set<Authority> authorities = this.authoritiesFromStrings(userDTO.getAuthorities());
			if (authorities != null) {
				user.setAuthorities(authorities);
			}
			return user;
		}
	}

	public List<User> userDTOsToUsers(List<UserDTO> userDTOs) {
		return userDTOs.stream().filter(Objects::nonNull).map(this::userDTOToUser).collect(Collectors.toList());
	}

	public User userFromId(Long id) {
		if (id == null) {
			return null;
		}
		User user = new User();
		user.setId(id);
		return user;
	}

	public Set<Authority> authoritiesFromStrings(Set<String> strings) {
		return strings.stream().map(string -> {
			Authority auth = new Authority();
			auth.setName(string);
			return auth;
		}).collect(Collectors.toSet());
	}

}
