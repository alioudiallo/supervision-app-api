package com.kaylene.supervision.service.mapper;

import com.kaylene.supervision.domain.DemandStory;
import com.kaylene.supervision.service.dto.DemandStoryDTO;
import org.mapstruct.Mapper;

/**
 * Mapper for the entity DemandStory and its DTO DemandStoryDTO.
 */
@Mapper(componentModel = "spring", uses = { UserMapper.class, DemandMapper.class })
public interface DemandStoryMapper extends EntityMapper<DemandStoryDTO, DemandStory> {

	DemandStoryDTO toDto(DemandStory demandStory);

	DemandStory toEntity(DemandStoryDTO demandStoryDTO);

	default DemandStory fromId(Long id) {
		if (id == null) {
			return null;
		}
		DemandStory demandStory = new DemandStory();
		demandStory.setId(id);
		return demandStory;
	}
}
