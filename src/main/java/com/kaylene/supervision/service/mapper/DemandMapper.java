package com.kaylene.supervision.service.mapper;

import com.kaylene.supervision.domain.Demand;
import com.kaylene.supervision.domain.HostDemand;
import com.kaylene.supervision.service.dto.DemandDTO;
import com.kaylene.supervision.service.dto.HostDemandDTO;
import org.mapstruct.IterableMapping;
import org.mapstruct.Mapper;
import org.mapstruct.Mapping;
import org.mapstruct.Named;

import java.util.Set;

/**
 * Mapper for the entity Demand and its DTO DemandDTO.
 */
@Mapper(componentModel = "spring", uses = {ContractMapper.class, CompanyMapper.class, HostDemandMapper.class}) 
public interface DemandMapper extends EntityMapper<DemandDTO, Demand> {

    @Mapping(source = "company.id", target = "companyId")
    @Mapping(source = "contract.id", target = "contractId")
    DemandDTO toDto(Demand demand); 

    @Mapping(target = "hostsToAdds", expression = "java(toHostDemandEntities(demandDTO.getHostsToAdds()))") 
    @Mapping(source = "companyId", target = "company")
    @Mapping(source = "contractId", target = "contract")
    Demand toEntity(DemandDTO demandDTO);

    default Demand fromId(Long id) {
        if (id == null) {
            return null;
        }
        Demand demand = new Demand();
        demand.setId(id);
        return demand;
    }
    
    @Named("mapHostToDto")   
    @Mapping(source = "demandId", target = "demand") 
    HostDemand toHostDemandEntity(HostDemandDTO hostDemandDTO); 
     
    @IterableMapping(qualifiedByName="mapHostToDto") 
    Set<HostDemand> toHostDemandEntities(Set<HostDemandDTO> hostDemandDTO); 
}
