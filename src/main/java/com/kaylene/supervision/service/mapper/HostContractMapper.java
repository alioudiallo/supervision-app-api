package com.kaylene.supervision.service.mapper;

import com.kaylene.supervision.domain.HostContract;
import com.kaylene.supervision.service.dto.HostContractDTO;
import org.mapstruct.Mapper;
import org.mapstruct.Mapping;

/**
 * Mapper for the entity HostContract and its DTO HostContractDTO.
 */
@Mapper(componentModel = "spring", uses = {ContractMapper.class})
public interface HostContractMapper extends EntityMapper<HostContractDTO, HostContract> {

    @Mapping(source = "contract.id", target = "contractId")
    HostContractDTO toDto(HostContract hostContract); 

    @Mapping(source = "contractId", target = "contract")
    HostContract toEntity(HostContractDTO hostContractDTO);

    default HostContract fromId(Long id) {
        if (id == null) {
            return null;
        }
        HostContract hostContract = new HostContract();
        hostContract.setId(id);
        return hostContract;
    }
}
