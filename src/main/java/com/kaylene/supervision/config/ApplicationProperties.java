package com.kaylene.supervision.config;

import org.springframework.boot.context.properties.ConfigurationProperties;

/**
 * Properties specific to Supervisionapi.
 * <p>
 * Properties are configured in the application.yml file. See
 * {@link io.github.jhipster.config.JHipsterProperties} for a good example.
 */
@ConfigurationProperties(prefix = "application", ignoreUnknownFields = false)
public class ApplicationProperties {

    public final Zabbix zabbix = new Zabbix();
    public final Orange orange = new Orange();
    public final Frontend frontend = new Frontend();

    public Zabbix getZabbix() {
	return zabbix;
    }

    public Orange getOrange() {
	return orange;
    }

    public Frontend getFrontend() {
	return frontend;
    }

    public static class Orange {
    }

    public static class Frontend {
	private String url;
	private String urlticket;

	public String getUrl() {
	    return url;
	}

	public void setUrl(String url) {
	    this.url = url;
	}

	public String getUrlticketd() {
	    return urlticket;
	}

	public void setUrlticket(String urlticket) {
	    this.urlticket = urlticket;
	}

    }

    public static class Zabbix {
	private String url;
	private String login;
	private String password;

	public String getUrl() {
	    return url;
	}

	public void setUrl(String url) {
	    this.url = url;
	}

	public String getLogin() {
	    return login;
	}

	public void setLogin(String login) {
	    this.login = login;
	}

	public String getPassword() {
	    return password;
	}

	public void setPassword(String password) {
	    this.password = password;
	}

    }

}
