package com.kaylene.supervision.repository;

import com.kaylene.supervision.domain.ContactPerson;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import java.util.List;


/**
 * Spring Data JPA repository for the ContactPerson entity.
 */
@SuppressWarnings("unused")
@Repository
public interface ContactPersonRepository extends JpaRepository<ContactPerson, Long> {
	
	public List<ContactPerson> findByCompanyId(Long id);
	
	public List<ContactPerson> findByCompanyZabbixLogin(String zabbixLogin);
}
