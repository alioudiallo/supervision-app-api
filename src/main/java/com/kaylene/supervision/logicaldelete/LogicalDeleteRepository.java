package com.kaylene.supervision.logicaldelete;

import com.kaylene.supervision.security.SecurityUtils;
import org.springframework.data.jpa.repository.JpaRepository;

import java.time.Instant;
import java.util.List;

/**
 * @author Mouhamad Ndiankho THIAM (thiamouhamadpro@gmail.com)
 * @since 2019/05/04
 */
public interface LogicalDeleteRepository<T extends AbstractLogicalDelete> extends JpaRepository<T, Long> {

    @Override
    default List<T> findAll() {
        return findAllNotDeleted();
    }

    List<T> findAllByDeleted(boolean deleted);

    default void logicalDelete(T d) {
        if (d != null && !d.isDeleted()) {
            d.setDeleted(true);
            d.setDateDeleted(Instant.now());
            d.setUserDeleted(SecurityUtils.getCurrentUserLogin());
            this.save(d);
        }
    }

    default void logicalUnDelete(T d) {
        if (d != null && d.isDeleted()) {
            d.setDeleted(false);
            d.setDateDeleted(null);
            d.setUserDeleted(null);
            this.save(d);
        }
    }

    default List<T> findAllDeleted() {
        return this.findAllByDeleted(true);
    }

    default List<T> findAllNotDeleted() {
        return this.findAllByDeleted(false);
    }
}
