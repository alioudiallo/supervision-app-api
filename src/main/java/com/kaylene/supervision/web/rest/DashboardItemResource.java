package com.kaylene.supervision.web.rest;

import com.codahale.metrics.annotation.Timed;
import com.kaylene.supervision.domain.DashboardItem;
import com.kaylene.supervision.service.DashboardService;
import io.github.jhipster.web.util.ResponseUtil;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import java.util.List;
import java.util.Optional;

/**
 * REST controller for managing DashboardItem.
 * @deprecated TODO - delete
 */
@RestController
@RequestMapping("/api")
public class DashboardItemResource {

	private final Logger log = LoggerFactory.getLogger(DashboardItemResource.class);

	private final DashboardService dashboardService;

	public DashboardItemResource(DashboardService dashboardService) {
		this.dashboardService = dashboardService;
	}

	/**
	 * GET /dashboard-items/client-dashboard
	 *
	 * @return the ResponseEntity with status 200 (OK) and with body the
	 *         clientDashBoard, or with status 404 (Not Found)
	 * @deprecated TODO - delete
	 */
	@GetMapping("/dashboard-items/client-dashboard/{companyId}")
	@Timed
	public ResponseEntity<List<DashboardItem>> getClientDashboard(@PathVariable Long companyId) {
		log.debug("REST request to get Client DashboardItem");
		return ResponseUtil.wrapOrNotFound(Optional.ofNullable(dashboardService.getClientDashboardItems(companyId)));
	}

}
