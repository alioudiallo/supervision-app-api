package com.kaylene.supervision.web.rest;

import com.codahale.metrics.annotation.Timed;
import com.kaylene.supervision.config.Constants;
import com.kaylene.supervision.domain.Company;
import com.kaylene.supervision.domain.User;
import com.kaylene.supervision.repository.CompanyRepository;
import com.kaylene.supervision.repository.UserRepository;
import com.kaylene.supervision.service.CompanyService;
import com.kaylene.supervision.service.UserService;
import com.kaylene.supervision.service.dto.CompanyDTO;
import com.kaylene.supervision.service.dto.SiteDTO;
import com.kaylene.supervision.service.mapper.CompanyMapper;
import com.kaylene.supervision.web.rest.errors.BadRequestAlertException;
import com.kaylene.supervision.web.rest.util.HeaderUtil;
import com.kaylene.supervision.web.rest.util.PaginationUtil;
import com.kaylene.supervision.zabbix.service.GraphService;
import com.kaylene.supervision.zabbix.dto.ZabbixScreenDTO;
import com.kaylene.supervision.zabbix.exception.ZabbixApiException;
import io.github.jhipster.web.util.ResponseUtil;
import io.swagger.annotations.ApiParam;
import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import javax.validation.Valid;
import java.net.URI;
import java.net.URISyntaxException;
import java.util.ArrayList;
import java.util.List;
import java.util.NoSuchElementException;
import java.util.Optional;
import java.util.stream.Collectors;

import static com.kaylene.supervision.config.Constants.PHONE_NUMBER_LENGTH;
import static com.kaylene.supervision.constants.Messages.*;
/**
 * REST controller for managing Company.
 */
@RestController
@RequestMapping("/api/kaylene/companies")
@Slf4j
@RequiredArgsConstructor
public class CompanyResource {

    private static final String ENTITY_NAME = "company";

    private final CompanyRepository companyRepository;
    private final CompanyMapper companyMapper;
    private final CompanyService companyService;
    private final UserRepository userRepository;
    private final UserService userService;
    private final SiteResource siteResource;
    private final GraphService graphService;

    /**
     * POST  /companies : Create a new company.
     *
     * @param companyDTO the companyDTO to create
     * @return the ResponseEntity with status 201 (Created) and with body the new companyDTO, or with status 400 (Bad Request) if the company has already an ID
     * @throws URISyntaxException if the Location URI syntax is incorrect
     */
    @PostMapping
    @Timed
    public ResponseEntity<CompanyDTO> createCompany(@Valid @RequestBody CompanyDTO companyDTO) throws URISyntaxException {
        log.debug("REST request to save Company : {}", companyDTO);
        if (companyDTO.getId() != null) {
            throw new BadRequestAlertException("A new company cannot already have an ID", ENTITY_NAME, "idexists");
        }
        companyDTO.setName(companyDTO.getName().toUpperCase());
        companyDTO.setPhone(companyDTO.getPhone().replaceAll(" ", ""));
        if(companyService.isCompanyNameAlreadyInUse(companyDTO.getName())){
        	throw new BadRequestAlertException(COMPANY_NAME_ALREADY_USED_ERROR_MSG, ENTITY_NAME, "companyNameAlreadyInUse");
        }
        if(userService.isEmailInvalid(companyDTO.getMainEmail())) {
        	throw new BadRequestAlertException(INVALID_EMAIL_ERROR_MSG
        			, "USER", "emailinvalid");
        }
        if(companyService.isCompanyMainEmailAlreadyInUse(companyDTO.getMainEmail())){
        	throw new BadRequestAlertException(EMAIL_ALREADY_USED_ERROR_MSG, ENTITY_NAME, "companyEmailAlreadyInUse");
        }
        if(companyService.isZabbixLoginAlreadyInUse(companyDTO.getZabbixLogin())) {
        	throw new BadRequestAlertException(ZABBIX_LOGIN_ALREADY_USED_ERROR_MSG, ENTITY_NAME, "zabbixLoginAlreadyInUse");
        }
        if(companyService.isPhoneNumberInvalid(companyDTO.getPhone())) {
        	throw new BadRequestAlertException("Le numero de téléphone ne doit "
        			+ "contenir que des chiffres.", ENTITY_NAME, "phonenumberinvalid");
        }
        if(companyService.isPhoneNumberLengthInvalid(companyDTO.getPhone())) {
        	throw new BadRequestAlertException("Le numéro de téléphone doit être composé de "+
        			PHONE_NUMBER_LENGTH+" chiffres."
        			, ENTITY_NAME, "phonenumberlengthinvalid");
        }
        if(companyService.isPhoneNumberAlreadyInUse(companyDTO.getPhone())) {
        	throw new BadRequestAlertException(PHONE_NUMBER_ALREADY_USED_ERROR_MSG, ENTITY_NAME, "phoneNumberAlreadyInUse");
        }
        Optional<SiteDTO> siteWithoutProxy = companyDTO.getSites().stream()
            .filter(siteDTO -> "0".equalsIgnoreCase(siteDTO.getZabbixProxyId()))
            .findFirst();
        if(!siteWithoutProxy.isPresent()) {
            SiteDTO siteDTO = new SiteDTO();
            siteDTO.setZabbixProxyId("0");
            siteDTO.setLabel("Sans Proxy");
            companyDTO.getSites().add(siteDTO);
        }
        Company company = companyMapper.toEntity(companyDTO);
        company = companyRepository.save(company);
        CompanyDTO result = companyMapper.toDto(company);
        return ResponseEntity.created(new URI("/api/companies/" + result.getId()))
            .headers(HeaderUtil.createEntityCreationAlert(ENTITY_NAME, result.getId().toString()))
            .body(result);
    }

    /**
     * PUT  /companies : Updates an existing company.
     *
     * @param companyDTO the companyDTO to update
     * @return the ResponseEntity with status 200 (OK) and with body the updated companyDTO,
     * or with status 400 (Bad Request) if the companyDTO is not valid,
     * or with status 500 (Internal Server Error) if the companyDTO couldn't be updated
     * @throws URISyntaxException if the Location URI syntax is incorrect
     */
    @PutMapping
    @Timed
    public ResponseEntity<CompanyDTO> updateCompany(@Valid @RequestBody CompanyDTO companyDTO) throws URISyntaxException {
        log.debug("REST request to update Company : {}", companyDTO);
        if (companyDTO.getId() == null) {
            return createCompany(companyDTO);
        }
        companyDTO.setName(companyDTO.getName().toUpperCase());
        companyDTO.setPhone(companyDTO.getPhone().replaceAll(" ", ""));

        Company company = companyMapper.toEntity(companyDTO);
        company = companyRepository.save(company);
        CompanyDTO result = companyMapper.toDto(company);
        return ResponseEntity.ok()
            .headers(HeaderUtil.createEntityUpdateAlert(ENTITY_NAME, companyDTO.getId().toString()))
            .body(result);
    }


   @GetMapping
    @Timed
    public ResponseEntity<List<CompanyDTO>> getAllCompanies(@ApiParam Pageable pageable) {
        log.debug("REST request to get a page of Companies");
        Page<Company> page = companyRepository.findAll(pageable);
        siteResource.listSite();
        HttpHeaders headers = PaginationUtil.generatePaginationHttpHeaders(page, "/api/companies");
        return new ResponseEntity<>(companyMapper.toDto(page.getContent()), headers, HttpStatus.OK);
    }


    /**
     * GET  /companies/:id : get the "id" company.
     *
     * @param id the id of the companyDTO to retrieve
     * @return the ResponseEntity with status 200 (OK) and with body the companyDTO, or with status 404 (Not Found)
     */
    @GetMapping("/{id}")
    @Timed
    public ResponseEntity<List<CompanyDTO>> getCompany(@PathVariable Long id) {
        log.debug("REST request to get Company : {}", id);
        Company company = companyRepository.findOne(id);
        List<Company> companies = new ArrayList<Company>();
        companies.add(company);
        List<CompanyDTO> companyDTO = companyMapper.toDto(companies);
        return ResponseUtil.wrapOrNotFound(Optional.ofNullable(companyDTO));
    }

    @GetMapping("/{id}/screens")
    public ResponseEntity<List<ZabbixScreenDTO>> getScreens(@PathVariable("id") Long id) throws ZabbixApiException {
        Company company = companyRepository.findOne(id);
        if(company == null) {
            throw new NoSuchElementException("Company not found");
        }
        List<ZabbixScreenDTO> screens = graphService.getScreens(company.getZabbixLogin(), company.getZabbixPassword()).stream()
            .filter(screen -> !screen.getItems().isEmpty())
            .collect(Collectors.toList());
        return ResponseEntity.ok(screens);
    }

    /**
     * DELETE  /companies/:id : delete the "id" company.
     *
     * @param id the id of the companyDTO to delete
     * @return the ResponseEntity with status 200 (OK)
     */
    @DeleteMapping("/{id}")
    @Timed
    public ResponseEntity<Void> deleteCompany(@PathVariable Long id) {
        log.debug("REST request to delete Company : {}", id);
        companyRepository.delete(id);
        return ResponseEntity.ok().headers(HeaderUtil.createEntityDeletionAlert(ENTITY_NAME, id.toString())).build();
    }

    /**
     * GET  /companies/by-login/:login : get the "userLogin"
     *
     * @param userLogin the login of the user whose company is to retrieve
     * @return the ResponseEntity with status 200 (OK) and with body a list of companyDTO, or with status 404 (Not Found)
     */
    @GetMapping("/by-login/{userLogin:" + Constants.LOGIN_REGEX + "}")
    @Timed
    public ResponseEntity<List<CompanyDTO>> getCompany(@PathVariable String userLogin) {
        log.debug("REST request to get Company by login : {}", userLogin);
        User user = userRepository.findByLogin(userLogin);
        if(user == null) {
        	throw new BadRequestAlertException(NO_USER_FOUND_WITH_GIVEN_LOGIN_ERROR_MSG, ENTITY_NAME, "nouserfoundwithgivenlogin");
        }
        Company company = companyRepository.findOne(user.getCompany().getId());
        List<Company> companies = new ArrayList<Company>();
        companies.add(company);
        List<CompanyDTO> companyDTO = companyMapper.toDto(companies);
        return ResponseUtil.wrapOrNotFound(Optional.ofNullable(companyDTO));
    }

    @GetMapping("/name/{name}")
    public ResponseEntity<CompanyDTO> getCompanyByName(@PathVariable("name") String name) {
        List<Company> companies = companyRepository.findByNameIgnoreCase(name);
        Optional<CompanyDTO> company = Optional.empty();
        if(!companies.isEmpty()) {
            company = Optional.ofNullable(companyMapper.toDto(companies.get(0)));
        }
        return ResponseUtil.wrapOrNotFound(company);
    }

    /**
     * GET  /max-number-of-hosts-for-company/:id : get the "id" company.
     *
     * @param id the id of the company
     * @return the ResponseEntity with status 200 (OK) and with body the MaximalNumberOfHostsForCompany, or with status 404 (Not Found)
     */
    @GetMapping("/max-number-of-hosts-for-company/{id}")
    @Timed
    public ResponseEntity<Integer> getMaximalNumberOfHostsForCompany(@PathVariable Long id) {
        log.debug("REST request to get getMaximalNumberOfHostsForCompany : {}", id);
        Integer maxNumberOfHost = companyService.getMaximalNumberOfHostsForCompany(id);
        return ResponseUtil.wrapOrNotFound(Optional.ofNullable(maxNumberOfHost));
    }
}
