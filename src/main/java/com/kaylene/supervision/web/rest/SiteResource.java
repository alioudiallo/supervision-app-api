package com.kaylene.supervision.web.rest;

import com.codahale.metrics.annotation.Timed;
import com.kaylene.supervision.domain.Site;
import com.kaylene.supervision.repository.SiteRepository;
import com.kaylene.supervision.service.dto.SiteDTO;
import com.kaylene.supervision.service.mapper.SiteMapper;
import com.kaylene.supervision.web.rest.errors.BadRequestAlertException;
import com.kaylene.supervision.web.rest.util.HeaderUtil;
import com.kaylene.supervision.web.rest.util.PaginationUtil;
import io.github.jhipster.web.util.ResponseUtil;
import io.swagger.annotations.ApiParam;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import java.net.URI;
import java.net.URISyntaxException;
import java.util.List;
import java.util.Optional;

/**
 * REST controller for managing Site.
 */
@RestController
@RequestMapping("/api/kaylene")
public class SiteResource {

	private final Logger log = LoggerFactory.getLogger(SiteResource.class);

	private static final String ENTITY_NAME = "site";

	private final SiteRepository siteRepository;

	private final SiteMapper siteMapper;

	public SiteResource(SiteRepository siteRepository, SiteMapper siteMapper) {
		this.siteRepository = siteRepository;
		this.siteMapper = siteMapper;
	}

	/**
	 * POST /sites : Create a new site.
	 *
	 * @param siteDTO
	 *            the siteDTO to create
	 * @return the ResponseEntity with status 201 (Created) and with body the new
	 *         siteDTO, or with status 400 (Bad Request) if the site has already an
	 *         ID
	 * @throws URISyntaxException
	 *             if the Location URI syntax is incorrect
	 */
	@PostMapping("/sites")
	@Timed
	public ResponseEntity<SiteDTO> createSite(@RequestBody SiteDTO siteDTO) throws URISyntaxException {
		log.debug("REST request to save Site : {}", siteDTO);
		if (siteDTO.getId() != null) {
			throw new BadRequestAlertException("A new site cannot already have an ID", ENTITY_NAME, "idexists");
		}
		Site site = siteMapper.toEntity(siteDTO);
		site = siteRepository.save(site);
		listSite();
		SiteDTO result = siteMapper.toDto(site);
		return ResponseEntity.created(new URI("/api/sites/" + result.getId()))
				.headers(HeaderUtil.createEntityCreationAlert(ENTITY_NAME, result.getId().toString())).body(result);
	}

	/**
	 * PUT /sites : Updates an existing site.
	 *
	 * @param siteDTO
	 *            the siteDTO to update
	 * @return the ResponseEntity with status 200 (OK) and with body the updated
	 *         siteDTO, or with status 400 (Bad Request) if the siteDTO is not
	 *         valid, or with status 500 (Internal Server Error) if the siteDTO
	 *         couldn't be updated
	 * @throws URISyntaxException
	 *             if the Location URI syntax is incorrect
	 */
	@PutMapping("/sites")
	@Timed
	public ResponseEntity<SiteDTO> updateSite(@RequestBody SiteDTO siteDTO) throws URISyntaxException {
		log.debug("REST request to update Site : {}", siteDTO);
		if (siteDTO.getId() == null) {
			return createSite(siteDTO);
		}
		Site site = siteMapper.toEntity(siteDTO);
		site = siteRepository.save(site);
		SiteDTO result = siteMapper.toDto(site);
		return ResponseEntity.ok().headers(HeaderUtil.createEntityUpdateAlert(ENTITY_NAME, siteDTO.getId().toString()))
				.body(result);
	}

	/**
	 * GET /sites : get all the sites.
	 *
	 * @param pageable
	 *            the pagination information
	 * @return the ResponseEntity with status 200 (OK) and the list of sites in body
	 */
	@GetMapping("/sites")
	@Timed
	public ResponseEntity<List<SiteDTO>> getAllSites(@ApiParam Pageable pageable) {
		log.debug("REST request to get a page of Sites");
		Page<Site> page = siteRepository.findAll(pageable);
		HttpHeaders headers = PaginationUtil.generatePaginationHttpHeaders(page, "/api/sites");
		return new ResponseEntity<>(siteMapper.toDto(page.getContent()), headers, HttpStatus.OK);
	}

	@GetMapping("/sites/listeSite")
	@Timed
	public List<Site> listSite() {
		log.debug("REST request to get a page of Sites");
		List<Site> ls = siteRepository.findAll();
		return ls;

	}

	/**
	 * GET /sites/:id : get the "id" site.
	 *
	 * @param id
	 *            the id of the siteDTO to retrieve
	 * @return the ResponseEntity with status 200 (OK) and with body the siteDTO, or
	 *         with status 404 (Not Found)
	 */
	@GetMapping("/sites/{id}")
	@Timed
	public ResponseEntity<SiteDTO> getSite(@PathVariable Long id) {
		log.debug("REST request to get Site : {}", id);
		Site site = siteRepository.findOne(id);
		SiteDTO siteDTO = siteMapper.toDto(site);
		return ResponseUtil.wrapOrNotFound(Optional.ofNullable(siteDTO));
	}

	/**
	 * DELETE /sites/:id : delete the "id" site.
	 *
	 * @param id
	 *            the id of the siteDTO to delete
	 * @return the ResponseEntity with status 200 (OK)
	 */
	@DeleteMapping("/sites/{id}")
	@Timed
	public ResponseEntity<Void> deleteSite(@PathVariable Long id) {
		log.debug("REST request to delete Site : {}", id);
		siteRepository.delete(id);
		return ResponseEntity.ok().headers(HeaderUtil.createEntityDeletionAlert(ENTITY_NAME, id.toString())).build();
	}

	// @DeleteMapping("/sites/{id}")
	// @Timed
	// public ResponseEntity<Void> deleteSite(@Param("id") Long id) {
	// log.debug("REST request to delete Site : {}", id);
	// siteRepository.deleteSiteById(id);
	// return
	// ResponseEntity.ok().headers(HeaderUtil.createEntityDeletionAlert(ENTITY_NAME,
	// id.toString())).build();
	// }

	/**
	 * GET /sites/by-company-id/ : get all the sites by Company Id.
	 *
	 * @param id
	 *            the id of the Company
	 * @return the ResponseEntity with status 200 (OK) and the list of sites in body
	 */
	@GetMapping("/sites/by-company-id/{id}")
	@Timed
	public ResponseEntity<List<SiteDTO>> getSitesByCompanyId(@PathVariable Long id) {
		log.debug("REST request to get a page of Sites by comany-id");
		List<Site> sites = siteRepository.findByCompanyId(id);
		List<SiteDTO> dtos = siteMapper.toDto(sites);
		return ResponseUtil.wrapOrNotFound(Optional.ofNullable(dtos));
	}

	@GetMapping("/site/company/{id}")
	@Timed
	public List<Site> getSiteByCompany(@PathVariable Long id) {
		log.debug("REST request to get all BankAccounts");
		return siteRepository.findByCompanyId(id);
	}

	/**
	 * GET /sites : get a site by Proxy Id.
	 *
	 * @param id
	 *            the id of the Zabbix Proxy
	 * @return the ResponseEntity with status 200 (OK) and the site in body
	 */
	@GetMapping("/sites/by-zabbix-proxy-id/{id}")
	@Timed
	public ResponseEntity<List<SiteDTO>> getSiteByZabbixProxyId(@ApiParam Pageable pageable, @PathVariable String id) {
		log.debug("REST request to get a page of Site by proxy-id");
		List<Site> sites = siteRepository.findByZabbixProxyId(id);
		List<SiteDTO> sitesDTO = siteMapper.toDto(sites);
		// HttpHeaders headers = PaginationUtil.generatePaginationHttpHeaders(page,
		// "/api/sites");
		// return new ResponseEntity<>(sitesDTO, ,HttpStatus.OK);
		return ResponseUtil.wrapOrNotFound(Optional.ofNullable(sitesDTO));
	}
}
