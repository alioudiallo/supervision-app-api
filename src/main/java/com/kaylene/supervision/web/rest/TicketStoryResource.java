package com.kaylene.supervision.web.rest;

import com.codahale.metrics.annotation.Timed;
import com.kaylene.supervision.domain.TicketStory;
import com.kaylene.supervision.repository.TicketStoryRepository;
import com.kaylene.supervision.service.dto.TicketStoryDTO;
import com.kaylene.supervision.service.mapper.TicketStoryMapper;
import com.kaylene.supervision.web.rest.util.HeaderUtil;
import com.kaylene.supervision.web.rest.util.PaginationUtil;
import io.github.jhipster.web.util.ResponseUtil;
import io.swagger.annotations.ApiParam;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import java.net.URI;
import java.net.URISyntaxException;
import java.util.List;
import java.util.Optional;

/**
 * REST controller for managing TicketStory.
 */
@RestController
@RequestMapping("/api")
public class TicketStoryResource {

	private final Logger log = LoggerFactory.getLogger(TicketStoryResource.class);

	private static final String ENTITY_NAME = "ticketStory";

	private final TicketStoryRepository ticketStoryRepository;

	private final TicketStoryMapper ticketStoryMapper;

	public TicketStoryResource(TicketStoryRepository ticketStoryRepository, TicketStoryMapper ticketStoryMapper) {
		this.ticketStoryRepository = ticketStoryRepository;
		this.ticketStoryMapper = ticketStoryMapper;
	}

	/**
	 * POST /ticket-stories : Create a new ticketStory.
	 *
	 * @param ticketStoryDTO
	 *            the ticketStoryDTO to create
	 * @return the ResponseEntity with status 201 (Created) and with body the new
	 *         ticketStoryDTO, or with status 400 (Bad Request) if the ticketStory
	 *         has already an ID
	 * @throws URISyntaxException
	 *             if the Location URI syntax is incorrect
	 */
	@PostMapping("/ticket-stories")
	@Timed
	public ResponseEntity<TicketStoryDTO> createTicketStory(TicketStory ticketStory) throws URISyntaxException {
		ticketStory = ticketStoryRepository.save(ticketStory);
		TicketStoryDTO result = ticketStoryMapper.toDto(ticketStory);
		return ResponseEntity.created(new URI("/api/ticket-stories/" + result.getId()))
				.headers(HeaderUtil.createEntityCreationAlert(ENTITY_NAME, result.getId().toString())).body(result);
	}

	/**
	 * GET /ticket-stories : get all the ticketStories.
	 *
	 * @param pageable
	 *            the pagination information
	 * @return the ResponseEntity with status 200 (OK) and the list of ticketStories
	 *         in body
	 */
	@GetMapping("/ticket-stories")
	@Timed
	public ResponseEntity<List<TicketStoryDTO>> getAllTicketStories(@ApiParam Pageable pageable) {
		log.debug("REST request to get a page of TicketStories");
		Page<TicketStory> page = ticketStoryRepository.findAll(pageable);
		HttpHeaders headers = PaginationUtil.generatePaginationHttpHeaders(page, "/api/ticket-stories");
		return new ResponseEntity<>(ticketStoryMapper.toDto(page.getContent()), headers, HttpStatus.OK);
	}

	/**
	 * GET /ticket-stories/:id : get the "id" ticketStory.
	 *
	 * @param id
	 *            the id of the ticketStoryDTO to retrieve
	 * @return the ResponseEntity with status 200 (OK) and with body the
	 *         ticketStoryDTO, or with status 404 (Not Found)
	 */
	@GetMapping("/ticket-stories/{id}")
	@Timed
	public ResponseEntity<TicketStoryDTO> getTicketStory(@PathVariable Long id) {
		log.debug("REST request to get TicketStory : {}", id);
		TicketStory ticketStory = ticketStoryRepository.findOne(id);
		TicketStoryDTO ticketStoryDTO = ticketStoryMapper.toDto(ticketStory);
		return ResponseUtil.wrapOrNotFound(Optional.ofNullable(ticketStoryDTO));
	}

	/**
	 * DELETE /ticket-stories/:id : delete the "id" ticketStory.
	 *
	 * @param id
	 *            the id of the ticketStoryDTO to delete
	 * @return the ResponseEntity with status 200 (OK)
	 */
	@DeleteMapping("/ticket-stories/{id}")
	@Timed
	public ResponseEntity<Void> deleteTicketStory(@PathVariable Long id) {
		log.debug("REST request to delete TicketStory : {}", id);
		ticketStoryRepository.delete(id);
		return ResponseEntity.ok().headers(HeaderUtil.createEntityDeletionAlert(ENTITY_NAME, id.toString())).build();
	}
}
