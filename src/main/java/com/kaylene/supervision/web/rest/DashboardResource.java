package com.kaylene.supervision.web.rest;

import com.codahale.metrics.annotation.Timed;
import com.kaylene.supervision.domain.Dashboard;
import com.kaylene.supervision.service.DashboardService;
import io.github.jhipster.web.util.ResponseUtil;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import java.util.Optional;

@RestController
@RequestMapping("/api")
public class DashboardResource {

	private final Logger log = LoggerFactory.getLogger(DashboardItemResource.class);

	private final DashboardService dashboardService;

	public DashboardResource(DashboardService dashboardService) {
		this.dashboardService = dashboardService;
	}

	/**
	 * GET /dashboard-items/client-dashboard
	 *
	 * @return the ResponseEntity with status 200 (OK) and with body the
	 *         clientDashBoard, or with status 404 (Not Found)
	 */
	@GetMapping("/dashboard/client-dashboard/{companyId}")
	@Timed
	public ResponseEntity<Dashboard> getClientDashboard(@PathVariable Long companyId) {
		log.debug("REST request to get Client Dashboard");
		return ResponseUtil.wrapOrNotFound(Optional.ofNullable(dashboardService.getDashboard(companyId)));
	}
}
