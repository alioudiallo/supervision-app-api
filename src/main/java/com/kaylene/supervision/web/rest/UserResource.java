package com.kaylene.supervision.web.rest;

import com.codahale.metrics.annotation.Timed;
import com.kaylene.supervision.config.Constants;
import com.kaylene.supervision.domain.User;
import com.kaylene.supervision.repository.UserRepository;
import com.kaylene.supervision.security.AuthoritiesConstants;
import com.kaylene.supervision.security.jwt.TokenProvider;
import com.kaylene.supervision.service.CompanyService;
import com.kaylene.supervision.service.MailService;
import com.kaylene.supervision.service.UserService;
import com.kaylene.supervision.service.dto.UserDTO;
import com.kaylene.supervision.service.mapper.UserMapper;
import com.kaylene.supervision.web.rest.errors.BadRequestAlertException;
import com.kaylene.supervision.web.rest.errors.EmailAlreadyUsedException;
import com.kaylene.supervision.web.rest.errors.LoginAlreadyUsedException;
import com.kaylene.supervision.web.rest.util.HeaderUtil;
import com.kaylene.supervision.web.rest.util.PaginationUtil;
import com.kaylene.supervision.web.rest.vm.LoginCurrentPasswordNewPassword;
import com.kaylene.supervision.web.rest.vm.ManagedUserVM;
import io.github.jhipster.web.util.ResponseUtil;
import io.swagger.annotations.ApiParam;
import io.undertow.util.BadRequestException;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.security.access.annotation.Secured;
import org.springframework.security.authentication.AuthenticationManager;
import org.springframework.security.authentication.UsernamePasswordAuthenticationToken;
import org.springframework.security.crypto.password.PasswordEncoder;
import org.springframework.web.bind.annotation.*;

import javax.validation.Valid;
import java.net.URI;
import java.net.URISyntaxException;
import java.util.List;
import java.util.Optional;

import static com.kaylene.supervision.config.Constants.PHONE_NUMBER_LENGTH;
import static com.kaylene.supervision.constants.Messages.INVALID_EMAIL_ERROR_MSG;
import static com.kaylene.supervision.constants.Messages.PHONE_NUMBER_ALREADY_USED_ERROR_MSG;

/**
 * REST controller for managing users.
 * <p>
 * This class accesses the User entity, and needs to fetch its collection of
 * authorities.
 * <p>
 * For a normal use-case, it would be better to have an eager relationship
 * between User and Authority, and send everything to the client side: there
 * would be no View Model and DTO, a lot less code, and an outer-join which
 * would be good for performance.
 * <p>
 * We use a View Model and a DTO for 3 reasons:
 * <ul>
 * <li>We want to keep a lazy association between the user and the authorities,
 * because people will quite often do relationships with the user, and we don't
 * want them to get the authorities all the time for nothing (for performance
 * reasons). This is the #1 goal: we should not impact our users' application
 * because of this use-case.</li>
 * <li>Not having an outer join causes n+1 requests to the database. This is not
 * a real issue as we have by default a second-level cache. This means on the
 * first HTTP call we do the n+1 requests, but then all authorities come from
 * the cache, so in fact it's much better than doing an outer join (which will
 * get lots of data from the database, for each HTTP call).</li>
 * <li>As this manages users, for security reasons, we'd rather have a DTO
 * layer.</li>
 * </ul>
 * <p>
 * Another option would be to have a specific JPA entity graph to handle this
 * case.
 */
@RestController
@RequestMapping("/api/kaylene/users")
public class UserResource {

	private final Logger log = LoggerFactory.getLogger(UserResource.class);

	private final UserRepository userRepository;

	private final UserService userService;

	private final MailService mailService;

	@Autowired
	PasswordEncoder passwordEncoder;

	@Autowired
	CompanyService companyService;

	@Autowired
	AccountResource accountResource;

	@Autowired
	UserMapper userMapper;

	@Autowired
	TokenProvider tokenProvider;

	@Autowired
	private AuthenticationManager authenticationManager;

	public UserResource(UserRepository userRepository, UserService userService, MailService mailService,
			PasswordEncoder passwordEncoder) {

		this.userRepository = userRepository;
		this.userService = userService;
		this.mailService = mailService;
		this.passwordEncoder = passwordEncoder;
	}

	/**
	 * POST  : Creates a new user.
	 * <p>
	 * Creates a new user if the login and email are not already used, and sends an
	 * mail with an activation link. The user needs to be activated on creation.
	 *
	 * @param managedUserVM
	 *            the user to create
	 * @return the ResponseEntity with status 201 (Created) and with body the new
	 *         user, or with status 400 (Bad Request) if the login or email is
	 *         already in use
	 * @throws URISyntaxException
	 *             if the Location URI syntax is incorrect
	 * @throws BadRequestAlertException
	 *             400 (Bad Request) if the login or email is already in use
	 */
	@PostMapping
	@Timed
	@Secured(AuthoritiesConstants.ADMIN)
	public ResponseEntity<UserDTO> createUser(@Valid @RequestBody ManagedUserVM managedUserVM)
			throws URISyntaxException {
		log.debug("REST request to save User : {}", managedUserVM);
		if (managedUserVM.getId() != null) {
			throw new BadRequestAlertException("A new user cannot already have an ID", "userManagement", "idexists");
		} else if (userRepository.findOneByLogin(managedUserVM.getLogin().toLowerCase()).isPresent()) {
			throw new LoginAlreadyUsedException();
		} else if (userRepository.findOneByEmailIgnoreCase(managedUserVM.getEmail()).isPresent()) {
			throw new EmailAlreadyUsedException();
		} else {
			if (managedUserVM.getCompanyId() == null) {
				throw new BadRequestAlertException("Veuillez sélectionner une entreprise" + " pour cet utilisateur.",
						"USER", "nocompanyidprovided");
			}
			if (userService.isEmailInvalid(managedUserVM.getLogin())) {
				throw new BadRequestAlertException("Le login saisi est invalide.", "USER", "logininvalid");
			}
			if (userService.isEmailInvalid(managedUserVM.getEmail())) {
				throw new BadRequestAlertException(INVALID_EMAIL_ERROR_MSG, "USER", "emailinvalid");
			}
			managedUserVM.setPhoneNumber(managedUserVM.getPhoneNumber().replaceAll(" ", ""));
			if (userService.isPhoneNumberAlreadyInUse(managedUserVM.getPhoneNumber())) {
				throw new BadRequestAlertException(PHONE_NUMBER_ALREADY_USED_ERROR_MSG, "USER",
						"phoneNumberAlreadyInUse");
			}
			if (companyService.isPhoneNumberInvalid(managedUserVM.getPhoneNumber())) {
				throw new BadRequestAlertException("Le numero de téléphone ne doit " + "contenir que des chiffres.",
						"USER", "phonenumberinvalid");
			}
			if (companyService.isPhoneNumberLengthInvalid(managedUserVM.getPhoneNumber())) {
				throw new BadRequestAlertException(
						"Le numéro de téléphone doit être composé de " + PHONE_NUMBER_LENGTH + " chiffres.", "USER",
						"phonenumberlengthinvalid");
			}
			managedUserVM.setAccesFacture(managedUserVM.getAccesFacture());
			User newUser = userService.createUser(managedUserVM);
			UserDTO newUserDTO = userMapper.userToUserDTO(newUser);
			mailService.customUserCreationEmail(managedUserVM);
			return ResponseEntity
					.created(new URI("/api/" + newUser.getLogin())).headers(HeaderUtil
							.createAlert("A user is created with identifier " + newUser.getLogin(), newUser.getLogin()))
					.body(newUserDTO);
		}
	}


	@PutMapping
	public ResponseEntity<UserDTO> updateUser(@RequestBody ManagedUserVM managedUserVM) {
		log.debug("REST request to update User : {}", managedUserVM);
		managedUserVM.setAccesFacture(managedUserVM.getAccesFacture());
		userService.updateUser(managedUserVM);

		Optional<UserDTO> updatedUser = userService.updateUser(managedUserVM);
		return ResponseUtil.wrapOrNotFound(updatedUser, HeaderUtil.createAlert(
				"A user is updated with identifier " + managedUserVM.getLogin(), managedUserVM.getLogin()));
	}

	/**
	 * GET  : get all users.
	 *
	 * @param pageable
	 *            the pagination information
	 * @return the ResponseEntity with status 200 (OK) and with body all users
	 */
	@GetMapping
	@Timed
	public ResponseEntity<List<UserDTO>> getAllUsers(@ApiParam Pageable pageable) {
		final Page<UserDTO> page = userService.getAllManagedUsers(pageable);
		HttpHeaders headers = PaginationUtil.generatePaginationHttpHeaders(page, "/api");
		return new ResponseEntity<>(page.getContent(), headers, HttpStatus.OK);
	}

	/**
	 * @return a string list of the all of the roles
	 */
	@GetMapping("/authorities")
	@Timed
	@Secured(AuthoritiesConstants.ADMIN)
	public List<String> getAuthorities() {
		return userService.getAuthorities();
	}

	/**
	 * GET /:login : get the "login" user.
	 *
	 * @param login
	 *            the login of the user to find
	 * @return the ResponseEntity with status 200 (OK) and with body the "login"
	 *         user, or with status 404 (Not Found)
	 */
	@GetMapping("/{login:" + Constants.LOGIN_REGEX + "}")
	@Timed
	public ResponseEntity<UserDTO> getUser(@PathVariable String login) {
		log.debug("REST request to get User : {}", login);
		return ResponseUtil
				.wrapOrNotFound(userService.getUserWithAuthoritiesByLogin(login).map(userMapper::userToUserDTO));
	}

	/**
	 * GET /by-id/:id : get the "id" user.
	 *
	 * @param id
	 *            the id of the user to find
	 * @return the ResponseEntity with status 200 (OK) and with body the "id" user,
	 *         or with status 400 (Bad Request)
	 */
	@GetMapping("/by-id/{id}")
	@Timed
	public ResponseEntity<UserDTO> getUserById(@PathVariable Long id) {
		log.debug("REST request to get User : {}", id);
		User user = userRepository.findOne(id);
		if (user == null) {
			throw new BadRequestAlertException("Aucun utilisateur trouvé", "USER", "nouserfoundwithid");
		}
		UserDTO userDTO = userMapper.userToUserDTO(user);
		return ResponseUtil.wrapOrNotFound(Optional.ofNullable(userDTO));
	}

	@DeleteMapping("/{login:" + Constants.LOGIN_REGEX + "}")
	@Timed
	@Secured(AuthoritiesConstants.ADMIN)
	public ResponseEntity<Void> deleteUser(@PathVariable String login) {
		log.info("REST request to delete User: {}", login);
		userService.deleteUser(login);
		return ResponseEntity.ok().headers(HeaderUtil.createAlert("userManagement.deleted", login)).build();
	}


	@PutMapping("/{login:" + Constants.LOGIN_REGEX + "}/unDelete")
	@Timed
	@Secured(AuthoritiesConstants.ADMIN)
	public ResponseEntity<Void> unDeleteUser(@PathVariable String login) {
		log.info("REST request to delete User: {}", login);
		userService.unDeleteUser(login);
		return ResponseEntity.ok().headers(HeaderUtil.createAlert("userManagement.unDeleted", login)).build();
	}




	/**
	 * PUT /change-password Change password of an existing User.
	 *
	 * @param login
	 *            user's login
	 * @param password
	 *            new password
	 * @return the ResponseEntity with status 200 (OK) and with body the updated
	 *         user
	 * @throws BadRequestException
	 */
	@PutMapping("/change-password")
	@Timed
	@Secured(AuthoritiesConstants.ADMIN)
	public ResponseEntity<UserDTO> changePassword(
			@RequestBody LoginCurrentPasswordNewPassword loginCurrentPasswordNewPassword) {
		String currentpassword = loginCurrentPasswordNewPassword.getCurrentPassword();
		String newpassword = loginCurrentPasswordNewPassword.getNewPassword();
		String login = loginCurrentPasswordNewPassword.getLogin();

		User user = userRepository.findByLogin(login);
		if (user == null) {
			throw new BadRequestAlertException("Aucun utilisateur trouvé avec le login " + login, "USER",
					"noUserFoundWithGivenLogin");
		}

		UsernamePasswordAuthenticationToken authenticationToken = new UsernamePasswordAuthenticationToken(login,
				currentpassword);
		authenticationManager.authenticate(authenticationToken);
		userService.changePassword(newpassword);

		UserDTO userDTO = userMapper.userToUserDTO(user);
		return ResponseEntity.ok().headers(HeaderUtil.createEntityUpdateAlert("USER", user.getLogin())).body(userDTO);
	}

	@GetMapping("/AllUsersByCompanyId/{id}")
	@Timed
	public List<User> getAllUsersByCompanyId(@PathVariable Long id) {
		List<User> users = userRepository.findByCompanyId(id);
		return users;
	}

}
