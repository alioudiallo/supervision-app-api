package com.kaylene.supervision.web.rest;

import com.codahale.metrics.annotation.Timed;
import com.kaylene.supervision.domain.Contract;
import com.kaylene.supervision.domain.enumeration.ContractStatus;
import com.kaylene.supervision.repository.CompanyRepository;
import com.kaylene.supervision.repository.ContractRepository;
import com.kaylene.supervision.service.ContractService;
import com.kaylene.supervision.service.dto.ContractDTO;
import com.kaylene.supervision.service.mapper.ContractMapper;
import com.kaylene.supervision.web.rest.errors.BadRequestAlertException;
import com.kaylene.supervision.web.rest.util.HeaderUtil;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import java.net.URI;
import java.net.URISyntaxException;
import java.time.Instant;
import java.util.ArrayList;
import java.util.List;

import static com.kaylene.supervision.constants.Messages.ONGOING_CONTRACT_EXISTS_ERROR_MSG;

/**
 * REST controller for managing Contract.
 */
@RestController
@RequestMapping("/api/kaylene")
public class ContractResource {

	private final Logger log = LoggerFactory.getLogger(ContractResource.class);

	private static final String ENTITY_NAME = "contract";

	private final ContractRepository contractRepository;

	private final ContractMapper contractMapper;
	@Autowired
	CompanyRepository companyRepository;

	@Autowired
	ContractService contractService;

	public ContractResource(ContractRepository contractRepository, ContractMapper contractMapper) {
		this.contractRepository = contractRepository;
		this.contractMapper = contractMapper;
	}

	/**
	 * POST /contracts : Create a new contract.
	 *
	 * @param contractDTO
	 *            the contractDTO to create
	 * @return the ResponseEntity with status 201 (Created) and with body the new
	 *         contractDTO, or with status 400 (Bad Request) if the contract has
	 *         already an ID
	 * @throws URISyntaxException
	 *             if the Location URI syntax is incorrect
	 */
	@PostMapping("/contracts")
	@Timed
	public ResponseEntity<ContractDTO> createContract(@RequestBody ContractDTO contractDTO) throws URISyntaxException {
		log.debug("REST request to save Contract : {}", contractDTO);
		if (contractDTO.getId() != null) {
			throw new BadRequestAlertException("A new contract cannot already have an ID", ENTITY_NAME, "idexists");
		}
		if (contractService.thereIsOnGoingContractForCompany(contractDTO.getCompanyId())) {
			throw new BadRequestAlertException(ONGOING_CONTRACT_EXISTS_ERROR_MSG, ENTITY_NAME, "ongoingcontractexists");
		}
		Contract contract = contractMapper.toEntity(contractDTO);
		contract.setCreateDate(Instant.now());
		contract.setCurrentStatus(ContractStatus.ONGOING);
		contract = contractRepository.save(contract);
		ContractDTO result = contractMapper.toDto(contract);
		return ResponseEntity.created(new URI("/api/contracts/" + result.getId()))
				.headers(HeaderUtil.createEntityCreationAlert(ENTITY_NAME, result.getId().toString())).body(result);
	}

	@PutMapping("/contracts")
	@Timed
	public ResponseEntity<ContractDTO> updateContract(@RequestBody ContractDTO contractDTO) throws URISyntaxException {
		log.debug("REST request to update Contract : {}", contractDTO);
		if (contractDTO.getId() == null) {
			return createContract(contractDTO);
		}	
		ContractDTO result = contractService.update(contractDTO);
		return ResponseEntity.ok()
				.headers(HeaderUtil.createEntityUpdateAlert(ENTITY_NAME, contractDTO.getId().toString())).body(result);
	}

	@GetMapping("/contracts")
	@Timed
	public List<Contract> getAllContracts() {
		log.debug("REST request to get a page of Companies");
		List<Contract> listeContract = contractRepository.findAll();
		return listeContract;

	}

	/**
	 * GET /contracts/:id : get the "id" contract.
	 *
	 * @param id
	 *            the id of the contractDTO to retrieve
	 * @return the ResponseEntity with status 200 (OK) and with body the
	 *         contractDTO, or with status 404 (Not Found)
	 */
	@GetMapping("/contracts/{id}")
	@Timed
	public List<Contract> getContract(@PathVariable Long id) {
		log.debug("REST request to get Contrat : {}", id);
		Contract contract = contractRepository.findOne(id);		
		List<Contract> contracts = new ArrayList<Contract>();
		contracts.add(contract);
		return contracts;
	}

	/**
	 * DELETE /contracts/:id : delete the "id" contract.
	 *
	 * @param id
	 *            the id of the contractDTO to delete
	 * @return the ResponseEntity with status 200 (OK)
	 */
	@DeleteMapping("/contracts/{id}")
	@Timed
	public ResponseEntity<Void> deleteContract(@PathVariable Long id) {
		log.debug("REST request to delete Contract : {}", id);
		contractRepository.delete(id);
		return ResponseEntity.ok().headers(HeaderUtil.createEntityDeletionAlert(ENTITY_NAME, id.toString())).build();
	}
}
