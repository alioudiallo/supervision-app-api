#!/bin/bash

sudo ./mvnw package -Pprod dockerfile:build
docker tag supervisionapi:latest snecommerce/supervision-api
docker push snecommerce/supervision-api